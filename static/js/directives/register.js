(function(ng, $, app) {

	'use strict';

	app.directive('register', [function() {
		return {
			restrict: 'A',
			scope: true,
			link: function(scope, element) {
				var $form = $(element).find('form');

				function register() {
					loader.show();
					var data = {
						email: $form.find('.email').val(),
						password: $form.find('.password').val(),
						confirm: $form.find('.password-confirmation').val()
					};

					$.post('/register', data, function(response) {
						loader.hide();
						if (response.success) {
							window.location = '/';
						} else {
							alert(response.data);
						}
					});
				}

				$form.on('submit', function(e) {
					e.preventDefault();
					register();
				});
			}
		};
	}]);

}) (angular, jQuery, Word);