(function(ng, $, app) {

	'use strict';

	app.directive('create', ['utility', function(utility) {
		return {
			restrict: 'A',
			scope: true,
			link: function(scope, element) {
				var $form = $(element).find('form');
				scope.suggestions = [];

				function upload(file) {
					var data = {
						word: $('#word').val(),
						file: file,
						filter: $('#filter').val()
					};
					$.post('/create', data, function(response) {
						loader.hide();
						if (response.success) {
							scope.$parent.emitMessage('word-created');
							setTimeout(function() {
								window.location = '/';
							}, 200);
							return;
						}

						if (ng.isArray(response.data)) {
							scope.suggestions = response.data;
							utility.safeApply(scope);
							return;
						}

						alert(response.data);
					});
				}

				function create() {
					loader.show();
					var files = $('#asset')[0].files;

					if (! files.length) {
						return alert('Please upload an image.');
					}

					var reader = new FileReader();

					reader.onload = function(e) {
						upload(e.target.result);
					};

					reader.readAsDataURL(files[0]);
				}

				$form.on('submit', function(e) {
					e.preventDefault();
					create();
				});

				scope.shim = function(event, suggestion) {
					event.preventDefault();
					$('#word').val(suggestion);
					create();
				};
			}
		};
	}]);

}) (angular, jQuery, Word);