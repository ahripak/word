(function(ng, $, app) {

	'use strict';

	app.directive('feed', ['$timeout', 'utility', function(timeout, utility) {
		return {
			restrict: 'A',
			scope: true,
			link: function(scope, element, attr) {
				scope.posts   = [];
				scope.loaded  = false;
				scope.user_id = $(element).attr('user-id');
				scope.count   = 0;

				scope.$parent.onMessage = function(data) {
					if (window.location.pathname !== '/') {
						return;
					}
					data = JSON.parse(data);
					scope.count += parseInt(data.new);
					utility.safeApply(scope);
					window.document.title = '(' + scope.count + ') Feed | Word';

					if (typeof Audio !== undefined) {
						var audio = new Audio('/img/music_marimba_chord.wav');
						audio.play();
					}
				};

				scope.refresh = function() {
					loader.show();
					$.get('/feed', function(response) {
						if (response.success) {
							scope.posts = response.data;
						}
						scope.loaded = true;
						utility.safeApply(scope);
						loader.hide();
					});
				};

				scope.deletePost = function(id) {
					var confirmation = confirm('Are you sure?');
					if (! confirmation) {
						return;
					}
					loader.show();
					$.get('/delete/' + id, function(response) {
						if (response.success) {
							scope.refresh();
						}
						utility.safeApply(scope);
						loader.hide();
					});
				};

				scope.play = function(event, file) {
					event.preventDefault();
					if (typeof Audio === undefined) {
						return alert('This browser does not support audio.');
					}

					if (scope.mute) {
						return;
					}

					var audio = new Audio(file);
					audio.play();
				};

				timeout(scope.refresh, 10);
			}
		};
	}]);

}) (angular, jQuery, Word);