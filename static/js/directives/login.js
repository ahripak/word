(function(ng, $, app) {

	'use strict';

	app.directive('login', [function() {
		return {
			restrict: 'A',
			scope: true,
			link: function(scope, element) {
				var $form = $(element).find('form');

				function login() {
					loader.show();
					var data = {
						email: $form.find('.email').val(),
						password: $form.find('.password').val()
					};

					$.post('/login', data, function(response) {
						loader.hide();
						if (response.success) {
							window.location = '/';
						} else {
							alert(response.data);
						}
					});
				}

				$form.on('submit', function(e) {
					e.preventDefault();
					login();
				});
			}
		};
	}]);

}) (angular, jQuery, Word);