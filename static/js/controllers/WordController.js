(function(ng, app) {

	'use strict';

	app.controller('WordController', ['$scope', '$http', function(scope, http) {

		if (typeof WebSocket !== 'undefined') {
			var connection;

			var connect = function() {
				connection = new WebSocket($('meta[name="socket"]').attr('content'));

				connection.onmessage = function(e) {
					if (typeof scope.onMessage === 'function') {
						scope.onMessage(e.data);
					}
				};

				connection.onclose = function() {
					setTimeout(connect, 200);
				};
			};

			connect();
		}

		scope.emitMessage = function(data) {
			if (connection === undefined) {
				return;
			}
			connection.send(JSON.stringify(data));
		};

	}]);

})(angular, Word);
