(function(ng, app) {

	'use strict';

	app.service('utility', function() {

		this.safeApply = function(scope) {
			var phase = scope.$root.$$phase;

			if (phase !== '$apply' && phase !== '$digest') {
				scope.$apply();
			}
		};

	});

})(angular, Word);
