module.exports = function(grunt) {

	/**
	 * Load all grunt-related dependencies
	 */
	require('load-grunt-tasks')(grunt);

	var javascriptFiles = {
		"build/js/app.js": [
			"app.js"
		],
		"build/js/word.js": [
			".compiled/word.js"
		],
		"build/js/lib/jquery.js": [
			"bower_components/jquery/dist/jquery.js"
		],
		"build/js/lib/angular.js": [
			"bower_components/angular/angular.js"
		]
	};

	/**
	 * Project Config!
	 */
	grunt.initConfig( {
		pkg: grunt.file.readJSON("package.json"),

		/**
		 * Compile our files - with some annotations
		 */
		concat: {
			js: {
				options: {
					process: function(src, filepath) {
						var blob = "/*!\n";
						blob += " * ------/ Source: " + filepath.replace(/^\.compiled\/js\//, "") + "\n";
						blob += " */\n" + src + "\n//! END";
						return blob;
					}
				},
				files: {
					".compiled/word.js" : [
						"js/directives/*.js",
						"js/controllers/*.js",
						"js/services/*.js"
					]
				}
			}
		},

		/**
		 * Minify our code depending on the environment we're in
		 */
		uglify: {
			prod: {
				options: {
					preserveComments: "some",
					compress: true
				},
				files: javascriptFiles
			},
			dev: {
				options: {
					preserveComments: "all",
					compress: false,
					beautify: true
				},
				files: javascriptFiles
			}
		},

		/**
		 * Compass plugin to compile out sass
		 */
		compass: {
			prod: {
				options: {
					config: "config.rb",
					force: true,
					outputStyle: "compressed",
					environment: "production"
				}
			},
			dev: {
				options: {
					config: "config.rb",
					force: false,
					outputStyle: "expanded",
					environment: "development"
				}
			}
		},

		/**
		 * Watch plugin to run tasks as we change files locally
		 */
		watch: {
			sass: {
				files: ["scss/**/*.scss"],
				tasks: ["compass:dev"]
			},
			scripts: {
				files: ["js/**/*.js"],
				tasks: ["eslint", "concat:js", "uglify:dev", "clean:scripts"]
			}
		},

		/**
		 * Clean out the compiled cruft.
		 */
		clean: {
			scripts: [".compiled"],
			buildFiles: ["build/js/*.js", "build/js/lib/*.js"]
		},

		/**
		 * Keep that code clean.
		 */
		eslint: {
			src: ['js/**/*.js'],
			options: {
				silent: true
			}
		},

		build: {
			/**
			 * Development Task
			 *
			 * This spawns a watch process that oversees changes
			 * applied to both .scss and .js files. When it catches
			 * one, it runs through the tasks almost as if this
			 * were productions.
			 */
			dev: ["compass:dev", "eslint", "concat:js", "uglify:dev", "clean:scripts", "watch"],
			/**
			 * Production Task
			 *
			 * This prepares the Angular code for minification,
			 * minifies it, and shoves it into one file for the client.
			 */
			prod: ["clean:buildFiles", "compass:prod", "eslint", "concat:css", "concat:js", "uglify:prod", "clean:scripts"]
		}

	} );

	/**
	 * Default Grunt Task
	 *
	 * We will block people from running the default grunt task
	 * because this would allow them to run all tasks that are
	 * of the multiTask type.
	 */
	grunt.registerTask("default", function() {
		return grunt.util.error("Please specify an environment. (i.e. `grunt build:dev`)");
	});

	grunt.registerMultiTask("build", function() {
		grunt.task.run(this.data);
	});

};