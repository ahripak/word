window.Word = angular.module("Word", [], [ "$interpolateProvider", function(a) {
    a.startSymbol("[[");
    a.endSymbol("]]");
} ]);

$(document).ready(function() {
    angular.bootstrap(this, [ "Word" ]);
});

window.loader = {
    show: function() {
        $(".loading").show();
    },
    hide: function() {
        $(".loading").hide();
    }
};