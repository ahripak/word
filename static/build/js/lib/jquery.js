/*!
 * jQuery JavaScript Library v1.11.1
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-05-01T17:42Z
 */
(function(a, b) {
    if (typeof module === "object" && typeof module.exports === "object") {
        // For CommonJS and CommonJS-like environments where a proper window is present,
        // execute the factory and get jQuery
        // For environments that do not inherently posses a window with a document
        // (such as Node.js), expose a jQuery-making factory as module.exports
        // This accentuates the need for the creation of a real window
        // e.g. var jQuery = require("jquery")(window);
        // See ticket #14549 for more info
        module.exports = a.document ? b(a, true) : function(a) {
            if (!a.document) {
                throw new Error("jQuery requires a window with a document");
            }
            return b(a);
        };
    } else {
        b(a);
    }
})(typeof window !== "undefined" ? window : this, function(a, b) {
    // Can't do this because several apps including ASP.NET trace
    // the stack via arguments.caller.callee and Firefox dies if
    // you try to trace through "use strict" call chains. (#13335)
    // Support: Firefox 18+
    //
    var c = [];
    var d = c.slice;
    var e = c.concat;
    var f = c.push;
    var g = c.indexOf;
    var h = {};
    var i = h.toString;
    var j = h.hasOwnProperty;
    var k = {};
    var l = "1.11.1", // Define a local copy of jQuery
    m = function(a, b) {
        // The jQuery object is actually just the init constructor 'enhanced'
        // Need init if jQuery is called (just allow error to be thrown if not included)
        return new m.fn.init(a, b);
    }, // Support: Android<4.1, IE<9
    // Make sure we trim BOM and NBSP
    n = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, // Matches dashed string for camelizing
    o = /^-ms-/, p = /-([\da-z])/gi, // Used by jQuery.camelCase as callback to replace()
    q = function(a, b) {
        return b.toUpperCase();
    };
    m.fn = m.prototype = {
        // The current version of jQuery being used
        jquery: l,
        constructor: m,
        // Start with an empty selector
        selector: "",
        // The default length of a jQuery object is 0
        length: 0,
        toArray: function() {
            return d.call(this);
        },
        // Get the Nth element in the matched element set OR
        // Get the whole matched element set as a clean array
        get: function(a) {
            // Return just the one element from the set
            // Return all the elements in a clean array
            return a != null ? a < 0 ? this[a + this.length] : this[a] : d.call(this);
        },
        // Take an array of elements and push it onto the stack
        // (returning the new matched element set)
        pushStack: function(a) {
            // Build a new jQuery matched element set
            var b = m.merge(this.constructor(), a);
            // Add the old object onto the stack (as a reference)
            b.prevObject = this;
            b.context = this.context;
            // Return the newly-formed element set
            return b;
        },
        // Execute a callback for every element in the matched set.
        // (You can seed the arguments with an array of args, but this is
        // only used internally.)
        each: function(a, b) {
            return m.each(this, a, b);
        },
        map: function(a) {
            return this.pushStack(m.map(this, function(b, c) {
                return a.call(b, c, b);
            }));
        },
        slice: function() {
            return this.pushStack(d.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(a) {
            var b = this.length, c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [ this[c] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor(null);
        },
        // For internal use only.
        // Behaves like an Array's method, not like a jQuery method.
        push: f,
        sort: c.sort,
        splice: c.splice
    };
    m.extend = m.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = false;
        // Handle a deep copy situation
        if (typeof g === "boolean") {
            j = g;
            // skip the boolean and the target
            g = arguments[h] || {};
            h++;
        }
        // Handle case when target is a string or something (possible in deep copy)
        if (typeof g !== "object" && !m.isFunction(g)) {
            g = {};
        }
        // extend jQuery itself if only one argument is passed
        if (h === i) {
            g = this;
            h--;
        }
        for (;h < i; h++) {
            // Only deal with non-null/undefined values
            if ((e = arguments[h]) != null) {
                // Extend the base object
                for (d in e) {
                    a = g[d];
                    c = e[d];
                    // Prevent never-ending loop
                    if (g === c) {
                        continue;
                    }
                    // Recurse if we're merging plain objects or arrays
                    if (j && c && (m.isPlainObject(c) || (b = m.isArray(c)))) {
                        if (b) {
                            b = false;
                            f = a && m.isArray(a) ? a : [];
                        } else {
                            f = a && m.isPlainObject(a) ? a : {};
                        }
                        // Never move original objects, clone them
                        g[d] = m.extend(j, f, c);
                    } else if (c !== undefined) {
                        g[d] = c;
                    }
                }
            }
        }
        // Return the modified object
        return g;
    };
    m.extend({
        // Unique for each copy of jQuery on the page
        expando: "jQuery" + (l + Math.random()).replace(/\D/g, ""),
        // Assume jQuery is ready without the ready module
        isReady: true,
        error: function(a) {
            throw new Error(a);
        },
        noop: function() {},
        // See test/unit/core.js for details concerning isFunction.
        // Since version 1.3, DOM methods and functions like alert
        // aren't supported. They return false on IE (#2968).
        isFunction: function(a) {
            return m.type(a) === "function";
        },
        isArray: Array.isArray || function(a) {
            return m.type(a) === "array";
        },
        isWindow: function(a) {
            /* jshint eqeqeq: false */
            return a != null && a == a.window;
        },
        isNumeric: function(a) {
            // parseFloat NaNs numeric-cast false positives (null|true|false|"")
            // ...but misinterprets leading-number strings, particularly hex literals ("0x...")
            // subtraction forces infinities to NaN
            return !m.isArray(a) && a - parseFloat(a) >= 0;
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) {
                return false;
            }
            return true;
        },
        isPlainObject: function(a) {
            var b;
            // Must be an Object.
            // Because of IE, we also have to check the presence of the constructor property.
            // Make sure that DOM nodes and window objects don't pass through, as well
            if (!a || m.type(a) !== "object" || a.nodeType || m.isWindow(a)) {
                return false;
            }
            try {
                // Not own constructor property must be Object
                if (a.constructor && !j.call(a, "constructor") && !j.call(a.constructor.prototype, "isPrototypeOf")) {
                    return false;
                }
            } catch (c) {
                // IE8,9 Will throw exceptions on certain host objects #9897
                return false;
            }
            // Support: IE<9
            // Handle iteration over inherited properties before own properties.
            if (k.ownLast) {
                for (b in a) {
                    return j.call(a, b);
                }
            }
            // Own properties are enumerated firstly, so to speed up,
            // if last one is own, then all properties are own.
            for (b in a) {}
            return b === undefined || j.call(a, b);
        },
        type: function(a) {
            if (a == null) {
                return a + "";
            }
            return typeof a === "object" || typeof a === "function" ? h[i.call(a)] || "object" : typeof a;
        },
        // Evaluates a script in a global context
        // Workarounds based on findings by Jim Driscoll
        // http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
        globalEval: function(b) {
            if (b && m.trim(b)) {
                // We use execScript on Internet Explorer
                // We use an anonymous function so that context is window
                // rather than jQuery in Firefox
                (a.execScript || function(b) {
                    a["eval"].call(a, b);
                })(b);
            }
        },
        // Convert dashed to camelCase; used by the css and data modules
        // Microsoft forgot to hump their vendor prefix (#9572)
        camelCase: function(a) {
            return a.replace(o, "ms-").replace(p, q);
        },
        nodeName: function(a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
        },
        // args is for internal usage only
        each: function(a, b, c) {
            var d, e = 0, f = a.length, g = r(a);
            if (c) {
                if (g) {
                    for (;e < f; e++) {
                        d = b.apply(a[e], c);
                        if (d === false) {
                            break;
                        }
                    }
                } else {
                    for (e in a) {
                        d = b.apply(a[e], c);
                        if (d === false) {
                            break;
                        }
                    }
                }
            } else {
                if (g) {
                    for (;e < f; e++) {
                        d = b.call(a[e], e, a[e]);
                        if (d === false) {
                            break;
                        }
                    }
                } else {
                    for (e in a) {
                        d = b.call(a[e], e, a[e]);
                        if (d === false) {
                            break;
                        }
                    }
                }
            }
            return a;
        },
        // Support: Android<4.1, IE<9
        trim: function(a) {
            return a == null ? "" : (a + "").replace(n, "");
        },
        // results is for internal usage only
        makeArray: function(a, b) {
            var c = b || [];
            if (a != null) {
                if (r(Object(a))) {
                    m.merge(c, typeof a === "string" ? [ a ] : a);
                } else {
                    f.call(c, a);
                }
            }
            return c;
        },
        inArray: function(a, b, c) {
            var d;
            if (b) {
                if (g) {
                    return g.call(b, a, c);
                }
                d = b.length;
                c = c ? c < 0 ? Math.max(0, d + c) : c : 0;
                for (;c < d; c++) {
                    // Skip accessing in sparse arrays
                    if (c in b && b[c] === a) {
                        return c;
                    }
                }
            }
            return -1;
        },
        merge: function(a, b) {
            var c = +b.length, d = 0, e = a.length;
            while (d < c) {
                a[e++] = b[d++];
            }
            // Support: IE<9
            // Workaround casting of .length to NaN on otherwise arraylike objects (e.g., NodeLists)
            if (c !== c) {
                while (b[d] !== undefined) {
                    a[e++] = b[d++];
                }
            }
            a.length = e;
            return a;
        },
        grep: function(a, b, c) {
            var d, e = [], f = 0, g = a.length, h = !c;
            // Go through the array, only saving the items
            // that pass the validator function
            for (;f < g; f++) {
                d = !b(a[f], f);
                if (d !== h) {
                    e.push(a[f]);
                }
            }
            return e;
        },
        // arg is for internal usage only
        map: function(a, b, c) {
            var d, f = 0, g = a.length, h = r(a), i = [];
            // Go through the array, translating each of the items to their new values
            if (h) {
                for (;f < g; f++) {
                    d = b(a[f], f, c);
                    if (d != null) {
                        i.push(d);
                    }
                }
            } else {
                for (f in a) {
                    d = b(a[f], f, c);
                    if (d != null) {
                        i.push(d);
                    }
                }
            }
            // Flatten any nested arrays
            return e.apply([], i);
        },
        // A global GUID counter for objects
        guid: 1,
        // Bind a function to a context, optionally partially applying any
        // arguments.
        proxy: function(a, b) {
            var c, e, f;
            if (typeof b === "string") {
                f = a[b];
                b = a;
                a = f;
            }
            // Quick check to determine if target is callable, in the spec
            // this throws a TypeError, but we will just return undefined.
            if (!m.isFunction(a)) {
                return undefined;
            }
            // Simulated bind
            c = d.call(arguments, 2);
            e = function() {
                return a.apply(b || this, c.concat(d.call(arguments)));
            };
            // Set the guid of unique handler to the same of original handler, so it can be removed
            e.guid = a.guid = a.guid || m.guid++;
            return e;
        },
        now: function() {
            return +new Date();
        },
        // jQuery.support is not used in Core but other projects attach their
        // properties to it so it needs to exist.
        support: k
    });
    // Populate the class2type map
    m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(a, b) {
        h["[object " + b + "]"] = b.toLowerCase();
    });
    function r(a) {
        var b = a.length, c = m.type(a);
        if (c === "function" || m.isWindow(a)) {
            return false;
        }
        if (a.nodeType === 1 && b) {
            return true;
        }
        return c === "array" || b === 0 || typeof b === "number" && b > 0 && b - 1 in a;
    }
    var s = /*!
 * Sizzle CSS Selector Engine v1.10.19
 * http://sizzlejs.com/
 *
 * Copyright 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-04-18
 */
    function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, // Local document vars
        m, n, o, p, q, r, s, t, // Instance-specific data
        u = "sizzle" + -new Date(), v = a.document, w = 0, x = 0, y = gb(), z = gb(), A = gb(), B = function(a, b) {
            if (a === b) {
                l = true;
            }
            return 0;
        }, // General-purpose constants
        C = typeof undefined, D = 1 << 31, // Instance methods
        E = {}.hasOwnProperty, F = [], G = F.pop, H = F.push, I = F.push, J = F.slice, // Use a stripped-down indexOf if we can't use a native one
        K = F.indexOf || function(a) {
            var b = 0, c = this.length;
            for (;b < c; b++) {
                if (this[b] === a) {
                    return b;
                }
            }
            return -1;
        }, L = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", // Regular expressions
        // Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
        M = "[\\x20\\t\\r\\n\\f]", // http://www.w3.org/TR/css3-syntax/#characters
        N = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", // Loosely modeled on CSS identifier characters
        // An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
        // Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
        O = N.replace("w", "w#"), // Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
        P = "\\[" + M + "*(" + N + ")(?:" + M + // Operator (capture 2)
        "*([*^$|!~]?=)" + M + // "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
        "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + O + "))|)" + M + "*\\]", Q = ":(" + N + ")(?:\\((" + // To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
        // 1. quoted (capture 3; capture 4 or capture 5)
        "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + // 2. simple (capture 6)
        "((?:\\\\.|[^\\\\()[\\]]|" + P + ")*)|" + // 3. anything else (capture 2)
        ".*" + ")\\)|)", // Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
        R = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"), S = new RegExp("^" + M + "*," + M + "*"), T = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"), U = new RegExp("=" + M + "*([^\\]'\"]*?)" + M + "*\\]", "g"), V = new RegExp(Q), W = new RegExp("^" + O + "$"), X = {
            ID: new RegExp("^#(" + N + ")"),
            CLASS: new RegExp("^\\.(" + N + ")"),
            TAG: new RegExp("^(" + N.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + P),
            PSEUDO: new RegExp("^" + Q),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + L + ")$", "i"),
            // For use in libraries implementing .is()
            // We use this for POS matching in `select`
            needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
        }, Y = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, $ = /^[^{]+\{\s*\[native \w/, // Easily-parseable/retrievable ID or TAG or CLASS selectors
        _ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ab = /[+~]/, bb = /'|\\/g, // CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
        cb = new RegExp("\\\\([\\da-f]{1,6}" + M + "?|(" + M + ")|.)", "ig"), db = function(a, b, c) {
            var d = "0x" + b - 65536;
            // NaN means non-codepoint
            // Support: Firefox<24
            // Workaround erroneous numeric interpretation of +"0x"
            // BMP codepoint
            // Supplemental Plane codepoint (surrogate pair)
            return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, d & 1023 | 56320);
        };
        // Optimize for push.apply( _, NodeList )
        try {
            I.apply(F = J.call(v.childNodes), v.childNodes);
            // Support: Android<4.0
            // Detect silently failing push.apply
            F[v.childNodes.length].nodeType;
        } catch (eb) {
            I = {
                apply: F.length ? // Leverage slice if possible
                function(a, b) {
                    H.apply(a, J.call(b));
                } : // Support: IE<9
                // Otherwise append directly
                function(a, b) {
                    var c = a.length, d = 0;
                    // Can't trust NodeList.length
                    while (a[c++] = b[d++]) {}
                    a.length = c - 1;
                }
            };
        }
        function fb(a, b, d, e) {
            var f, h, j, k, // QSA vars
            l, o, r, s, w, x;
            if ((b ? b.ownerDocument || b : v) !== n) {
                m(b);
            }
            b = b || n;
            d = d || [];
            if (!a || typeof a !== "string") {
                return d;
            }
            if ((k = b.nodeType) !== 1 && k !== 9) {
                return [];
            }
            if (p && !e) {
                // Shortcuts
                if (f = _.exec(a)) {
                    // Speed-up: Sizzle("#ID")
                    if (j = f[1]) {
                        if (k === 9) {
                            h = b.getElementById(j);
                            // Check parentNode to catch when Blackberry 4.6 returns
                            // nodes that are no longer in the document (jQuery #6963)
                            if (h && h.parentNode) {
                                // Handle the case where IE, Opera, and Webkit return items
                                // by name instead of ID
                                if (h.id === j) {
                                    d.push(h);
                                    return d;
                                }
                            } else {
                                return d;
                            }
                        } else {
                            // Context is not a document
                            if (b.ownerDocument && (h = b.ownerDocument.getElementById(j)) && t(b, h) && h.id === j) {
                                d.push(h);
                                return d;
                            }
                        }
                    } else if (f[2]) {
                        I.apply(d, b.getElementsByTagName(a));
                        return d;
                    } else if ((j = f[3]) && c.getElementsByClassName && b.getElementsByClassName) {
                        I.apply(d, b.getElementsByClassName(j));
                        return d;
                    }
                }
                // QSA path
                if (c.qsa && (!q || !q.test(a))) {
                    s = r = u;
                    w = b;
                    x = k === 9 && a;
                    // qSA works strangely on Element-rooted queries
                    // We can work around this by specifying an extra ID on the root
                    // and working up from there (Thanks to Andrew Dupont for the technique)
                    // IE 8 doesn't work on object elements
                    if (k === 1 && b.nodeName.toLowerCase() !== "object") {
                        o = g(a);
                        if (r = b.getAttribute("id")) {
                            s = r.replace(bb, "\\$&");
                        } else {
                            b.setAttribute("id", s);
                        }
                        s = "[id='" + s + "'] ";
                        l = o.length;
                        while (l--) {
                            o[l] = s + qb(o[l]);
                        }
                        w = ab.test(a) && ob(b.parentNode) || b;
                        x = o.join(",");
                    }
                    if (x) {
                        try {
                            I.apply(d, w.querySelectorAll(x));
                            return d;
                        } catch (y) {} finally {
                            if (!r) {
                                b.removeAttribute("id");
                            }
                        }
                    }
                }
            }
            // All others
            return i(a.replace(R, "$1"), b, d, e);
        }
        /**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
        function gb() {
            var a = [];
            function b(c, e) {
                // Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
                if (a.push(c + " ") > d.cacheLength) {
                    // Only keep the most recent entries
                    delete b[a.shift()];
                }
                return b[c + " "] = e;
            }
            return b;
        }
        /**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
        function hb(a) {
            a[u] = true;
            return a;
        }
        /**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
        function ib(a) {
            var b = n.createElement("div");
            try {
                return !!a(b);
            } catch (c) {
                return false;
            } finally {
                // Remove from its parent by default
                if (b.parentNode) {
                    b.parentNode.removeChild(b);
                }
                // release memory in IE
                b = null;
            }
        }
        /**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
        function jb(a, b) {
            var c = a.split("|"), e = a.length;
            while (e--) {
                d.attrHandle[c[e]] = b;
            }
        }
        /**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
        function kb(a, b) {
            var c = b && a, d = c && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || D) - (~a.sourceIndex || D);
            // Use IE sourceIndex if available on both nodes
            if (d) {
                return d;
            }
            // Check if b follows a
            if (c) {
                while (c = c.nextSibling) {
                    if (c === b) {
                        return -1;
                    }
                }
            }
            return a ? 1 : -1;
        }
        /**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
        function lb(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return c === "input" && b.type === a;
            };
        }
        /**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
        function mb(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return (c === "input" || c === "button") && b.type === a;
            };
        }
        /**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
        function nb(a) {
            return hb(function(b) {
                b = +b;
                return hb(function(c, d) {
                    var e, f = a([], c.length, b), g = f.length;
                    // Match elements found at the specified indexes
                    while (g--) {
                        if (c[e = f[g]]) {
                            c[e] = !(d[e] = c[e]);
                        }
                    }
                });
            });
        }
        /**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
        function ob(a) {
            return a && typeof a.getElementsByTagName !== C && a;
        }
        // Expose support vars for convenience
        c = fb.support = {};
        /**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
        f = fb.isXML = function(a) {
            // documentElement is verified for cases where it doesn't yet exist
            // (such as loading iframes in IE - #4833)
            var b = a && (a.ownerDocument || a).documentElement;
            return b ? b.nodeName !== "HTML" : false;
        };
        /**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
        m = fb.setDocument = function(a) {
            var b, e = a ? a.ownerDocument || a : v, g = e.defaultView;
            // If no document and documentElement is available, return
            if (e === n || e.nodeType !== 9 || !e.documentElement) {
                return n;
            }
            // Set our document
            n = e;
            o = e.documentElement;
            // Support tests
            p = !f(e);
            // Support: IE>8
            // If iframe document is assigned to "document" variable and if iframe has been reloaded,
            // IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
            // IE6-8 do not support the defaultView property so parent will be undefined
            if (g && g !== g.top) {
                // IE11 does not have attachEvent, so all must suffer
                if (g.addEventListener) {
                    g.addEventListener("unload", function() {
                        m();
                    }, false);
                } else if (g.attachEvent) {
                    g.attachEvent("onunload", function() {
                        m();
                    });
                }
            }
            /* Attributes
	---------------------------------------------------------------------- */
            // Support: IE<8
            // Verify that getAttribute really returns attributes and not properties (excepting IE8 booleans)
            c.attributes = ib(function(a) {
                a.className = "i";
                return !a.getAttribute("className");
            });
            /* getElement(s)By*
	---------------------------------------------------------------------- */
            // Check if getElementsByTagName("*") returns only elements
            c.getElementsByTagName = ib(function(a) {
                a.appendChild(e.createComment(""));
                return !a.getElementsByTagName("*").length;
            });
            // Check if getElementsByClassName can be trusted
            c.getElementsByClassName = $.test(e.getElementsByClassName) && ib(function(a) {
                a.innerHTML = "<div class='a'></div><div class='a i'></div>";
                // Support: Safari<4
                // Catch class over-caching
                a.firstChild.className = "i";
                // Support: Opera<10
                // Catch gEBCN failure to find non-leading classes
                return a.getElementsByClassName("i").length === 2;
            });
            // Support: IE<10
            // Check if getElementById returns elements by name
            // The broken getElementById methods don't pick up programatically-set names,
            // so use a roundabout getElementsByName test
            c.getById = ib(function(a) {
                o.appendChild(a).id = u;
                return !e.getElementsByName || !e.getElementsByName(u).length;
            });
            // ID find and filter
            if (c.getById) {
                d.find["ID"] = function(a, b) {
                    if (typeof b.getElementById !== C && p) {
                        var c = b.getElementById(a);
                        // Check parentNode to catch when Blackberry 4.6 returns
                        // nodes that are no longer in the document #6963
                        return c && c.parentNode ? [ c ] : [];
                    }
                };
                d.filter["ID"] = function(a) {
                    var b = a.replace(cb, db);
                    return function(a) {
                        return a.getAttribute("id") === b;
                    };
                };
            } else {
                // Support: IE6/7
                // getElementById is not reliable as a find shortcut
                delete d.find["ID"];
                d.filter["ID"] = function(a) {
                    var b = a.replace(cb, db);
                    return function(a) {
                        var c = typeof a.getAttributeNode !== C && a.getAttributeNode("id");
                        return c && c.value === b;
                    };
                };
            }
            // Tag
            d.find["TAG"] = c.getElementsByTagName ? function(a, b) {
                if (typeof b.getElementsByTagName !== C) {
                    return b.getElementsByTagName(a);
                }
            } : function(a, b) {
                var c, d = [], e = 0, f = b.getElementsByTagName(a);
                // Filter out possible comments
                if (a === "*") {
                    while (c = f[e++]) {
                        if (c.nodeType === 1) {
                            d.push(c);
                        }
                    }
                    return d;
                }
                return f;
            };
            // Class
            d.find["CLASS"] = c.getElementsByClassName && function(a, b) {
                if (typeof b.getElementsByClassName !== C && p) {
                    return b.getElementsByClassName(a);
                }
            };
            /* QSA/matchesSelector
	---------------------------------------------------------------------- */
            // QSA and matchesSelector support
            // matchesSelector(:active) reports false when true (IE9/Opera 11.5)
            r = [];
            // qSa(:focus) reports false when true (Chrome 21)
            // We allow this because of a bug in IE8/9 that throws an error
            // whenever `document.activeElement` is accessed on an iframe
            // So, we allow :focus to pass through QSA all the time to avoid the IE error
            // See http://bugs.jquery.com/ticket/13378
            q = [];
            if (c.qsa = $.test(e.querySelectorAll)) {
                // Build QSA regex
                // Regex strategy adopted from Diego Perini
                ib(function(a) {
                    // Select is set to empty string on purpose
                    // This is to test IE's treatment of not explicitly
                    // setting a boolean content attribute,
                    // since its presence should be enough
                    // http://bugs.jquery.com/ticket/12359
                    a.innerHTML = "<select msallowclip=''><option selected=''></option></select>";
                    // Support: IE8, Opera 11-12.16
                    // Nothing should be selected when empty strings follow ^= or $= or *=
                    // The test attribute must be unknown in Opera but "safe" for WinRT
                    // http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
                    if (a.querySelectorAll("[msallowclip^='']").length) {
                        q.push("[*^$]=" + M + "*(?:''|\"\")");
                    }
                    // Support: IE8
                    // Boolean attributes and "value" are not treated correctly
                    if (!a.querySelectorAll("[selected]").length) {
                        q.push("\\[" + M + "*(?:value|" + L + ")");
                    }
                    // Webkit/Opera - :checked should return selected option elements
                    // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
                    // IE8 throws error here and will not see later tests
                    if (!a.querySelectorAll(":checked").length) {
                        q.push(":checked");
                    }
                });
                ib(function(a) {
                    // Support: Windows 8 Native Apps
                    // The type and name attributes are restricted during .innerHTML assignment
                    var b = e.createElement("input");
                    b.setAttribute("type", "hidden");
                    a.appendChild(b).setAttribute("name", "D");
                    // Support: IE8
                    // Enforce case-sensitivity of name attribute
                    if (a.querySelectorAll("[name=d]").length) {
                        q.push("name" + M + "*[*^$|!~]?=");
                    }
                    // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
                    // IE8 throws error here and will not see later tests
                    if (!a.querySelectorAll(":enabled").length) {
                        q.push(":enabled", ":disabled");
                    }
                    // Opera 10-11 does not throw on post-comma invalid pseudos
                    a.querySelectorAll("*,:x");
                    q.push(",.*:");
                });
            }
            if (c.matchesSelector = $.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) {
                ib(function(a) {
                    // Check to see if it's possible to do matchesSelector
                    // on a disconnected node (IE 9)
                    c.disconnectedMatch = s.call(a, "div");
                    // This should fail with an exception
                    // Gecko does not error, returns false instead
                    s.call(a, "[s!='']:x");
                    r.push("!=", Q);
                });
            }
            q = q.length && new RegExp(q.join("|"));
            r = r.length && new RegExp(r.join("|"));
            /* Contains
	---------------------------------------------------------------------- */
            b = $.test(o.compareDocumentPosition);
            // Element contains another
            // Purposefully does not implement inclusive descendent
            // As in, an element does not contain itself
            t = b || $.test(o.contains) ? function(a, b) {
                var c = a.nodeType === 9 ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !!(d && d.nodeType === 1 && (c.contains ? c.contains(d) : a.compareDocumentPosition && a.compareDocumentPosition(d) & 16));
            } : function(a, b) {
                if (b) {
                    while (b = b.parentNode) {
                        if (b === a) {
                            return true;
                        }
                    }
                }
                return false;
            };
            /* Sorting
	---------------------------------------------------------------------- */
            // Document order sorting
            B = b ? function(a, b) {
                // Flag for duplicate removal
                if (a === b) {
                    l = true;
                    return 0;
                }
                // Sort on method existence if only one input has compareDocumentPosition
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                if (d) {
                    return d;
                }
                // Calculate position if both inputs belong to the same document
                d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : // Otherwise we know they are disconnected
                1;
                // Disconnected nodes
                if (d & 1 || !c.sortDetached && b.compareDocumentPosition(a) === d) {
                    // Choose the first element that is related to our preferred document
                    if (a === e || a.ownerDocument === v && t(v, a)) {
                        return -1;
                    }
                    if (b === e || b.ownerDocument === v && t(v, b)) {
                        return 1;
                    }
                    // Maintain original order
                    return k ? K.call(k, a) - K.call(k, b) : 0;
                }
                return d & 4 ? -1 : 1;
            } : function(a, b) {
                // Exit early if the nodes are identical
                if (a === b) {
                    l = true;
                    return 0;
                }
                var c, d = 0, f = a.parentNode, g = b.parentNode, h = [ a ], i = [ b ];
                // Parentless nodes are either documents or disconnected
                if (!f || !g) {
                    return a === e ? -1 : b === e ? 1 : f ? -1 : g ? 1 : k ? K.call(k, a) - K.call(k, b) : 0;
                } else if (f === g) {
                    return kb(a, b);
                }
                // Otherwise we need full lists of their ancestors for comparison
                c = a;
                while (c = c.parentNode) {
                    h.unshift(c);
                }
                c = b;
                while (c = c.parentNode) {
                    i.unshift(c);
                }
                // Walk down the tree looking for a discrepancy
                while (h[d] === i[d]) {
                    d++;
                }
                // Do a sibling check if the nodes have a common ancestor
                // Otherwise nodes in our document sort first
                return d ? kb(h[d], i[d]) : h[d] === v ? -1 : i[d] === v ? 1 : 0;
            };
            return e;
        };
        fb.matches = function(a, b) {
            return fb(a, null, null, b);
        };
        fb.matchesSelector = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            // Make sure that attribute selectors are quoted
            b = b.replace(U, "='$1']");
            if (c.matchesSelector && p && (!r || !r.test(b)) && (!q || !q.test(b))) {
                try {
                    var d = s.call(a, b);
                    // IE 9's matchesSelector returns false on disconnected nodes
                    if (d || c.disconnectedMatch || // As well, disconnected nodes are said to be in a document
                    // fragment in IE 9
                    a.document && a.document.nodeType !== 11) {
                        return d;
                    }
                } catch (e) {}
            }
            return fb(b, n, null, [ a ]).length > 0;
        };
        fb.contains = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            return t(a, b);
        };
        fb.attr = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            var e = d.attrHandle[b.toLowerCase()], // Don't get fooled by Object.prototype properties (jQuery #13807)
            f = e && E.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : undefined;
            return f !== undefined ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
        };
        fb.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a);
        };
        /**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
        fb.uniqueSort = function(a) {
            var b, d = [], e = 0, f = 0;
            // Unless we *know* we can detect duplicates, assume their presence
            l = !c.detectDuplicates;
            k = !c.sortStable && a.slice(0);
            a.sort(B);
            if (l) {
                while (b = a[f++]) {
                    if (b === a[f]) {
                        e = d.push(f);
                    }
                }
                while (e--) {
                    a.splice(d[e], 1);
                }
            }
            // Clear input after sorting to release objects
            // See https://github.com/jquery/sizzle/pull/225
            k = null;
            return a;
        };
        /**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
        e = fb.getText = function(a) {
            var b, c = "", d = 0, f = a.nodeType;
            if (!f) {
                // If no nodeType, this is expected to be an array
                while (b = a[d++]) {
                    // Do not traverse comment nodes
                    c += e(b);
                }
            } else if (f === 1 || f === 9 || f === 11) {
                // Use textContent for elements
                // innerText usage removed for consistency of new lines (jQuery #11153)
                if (typeof a.textContent === "string") {
                    return a.textContent;
                } else {
                    // Traverse its children
                    for (a = a.firstChild; a; a = a.nextSibling) {
                        c += e(a);
                    }
                }
            } else if (f === 3 || f === 4) {
                return a.nodeValue;
            }
            // Do not include comment or processing instruction nodes
            return c;
        };
        d = fb.selectors = {
            // Can be adjusted by the user
            cacheLength: 50,
            createPseudo: hb,
            match: X,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: true
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: true
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    a[1] = a[1].replace(cb, db);
                    // Move the given value to match[3] whether quoted or unquoted
                    a[3] = (a[3] || a[4] || a[5] || "").replace(cb, db);
                    if (a[2] === "~=") {
                        a[3] = " " + a[3] + " ";
                    }
                    return a.slice(0, 4);
                },
                CHILD: function(a) {
                    /* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
                    a[1] = a[1].toLowerCase();
                    if (a[1].slice(0, 3) === "nth") {
                        // nth-* requires argument
                        if (!a[3]) {
                            fb.error(a[0]);
                        }
                        // numeric x and y parameters for Expr.filter.CHILD
                        // remember that false/true cast respectively to 0/1
                        a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * (a[3] === "even" || a[3] === "odd"));
                        a[5] = +(a[7] + a[8] || a[3] === "odd");
                    } else if (a[3]) {
                        fb.error(a[0]);
                    }
                    return a;
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    if (X["CHILD"].test(a[0])) {
                        return null;
                    }
                    // Accept quoted arguments as-is
                    if (a[3]) {
                        a[2] = a[4] || a[5] || "";
                    } else if (c && V.test(c) && (// Get excess from tokenize (recursively)
                    b = g(c, true)) && (// advance to the next closing parenthesis
                    b = c.indexOf(")", c.length - b) - c.length)) {
                        // excess is a negative index
                        a[0] = a[0].slice(0, b);
                        a[2] = c.slice(0, b);
                    }
                    // Return only captures needed by the pseudo filter method (type and argument)
                    return a.slice(0, 3);
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(cb, db).toLowerCase();
                    return a === "*" ? function() {
                        return true;
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b;
                    };
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + M + ")" + a + "(" + M + "|$)")) && y(a, function(a) {
                        return b.test(typeof a.className === "string" && a.className || typeof a.getAttribute !== C && a.getAttribute("class") || "");
                    });
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = fb.attr(d, a);
                        if (e == null) {
                            return b === "!=";
                        }
                        if (!b) {
                            return true;
                        }
                        e += "";
                        return b === "=" ? e === c : b === "!=" ? e !== c : b === "^=" ? c && e.indexOf(c) === 0 : b === "*=" ? c && e.indexOf(c) > -1 : b === "$=" ? c && e.slice(-c.length) === c : b === "~=" ? (" " + e + " ").indexOf(c) > -1 : b === "|=" ? e === c || e.slice(0, c.length + 1) === c + "-" : false;
                    };
                },
                CHILD: function(a, b, c, d, e) {
                    var f = a.slice(0, 3) !== "nth", g = a.slice(-4) !== "last", h = b === "of-type";
                    // Shortcut for :nth-*(n)
                    return d === 1 && e === 0 ? function(a) {
                        return !!a.parentNode;
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
                        if (q) {
                            // :(first|last|only)-(child|of-type)
                            if (f) {
                                while (p) {
                                    l = b;
                                    while (l = l[p]) {
                                        if (h ? l.nodeName.toLowerCase() === r : l.nodeType === 1) {
                                            return false;
                                        }
                                    }
                                    // Reverse direction for :only-* (if we haven't yet done so)
                                    o = p = a === "only" && !o && "nextSibling";
                                }
                                return true;
                            }
                            o = [ g ? q.firstChild : q.lastChild ];
                            // non-xml :nth-child(...) stores cache data on `parent`
                            if (g && s) {
                                // Seek `elem` from a previously-cached index
                                k = q[u] || (q[u] = {});
                                j = k[a] || [];
                                n = j[0] === w && j[1];
                                m = j[0] === w && j[2];
                                l = n && q.childNodes[n];
                                while (l = ++n && l && l[p] || (// Fallback to seeking `elem` from the start
                                m = n = 0) || o.pop()) {
                                    // When found, cache indexes on `parent` and break
                                    if (l.nodeType === 1 && ++m && l === b) {
                                        k[a] = [ w, n, m ];
                                        break;
                                    }
                                }
                            } else if (s && (j = (b[u] || (b[u] = {}))[a]) && j[0] === w) {
                                m = j[1];
                            } else {
                                // Use the same loop as above to seek `elem` from the start
                                while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) {
                                    if ((h ? l.nodeName.toLowerCase() === r : l.nodeType === 1) && ++m) {
                                        // Cache the index of each encountered element
                                        if (s) {
                                            (l[u] || (l[u] = {}))[a] = [ w, m ];
                                        }
                                        if (l === b) {
                                            break;
                                        }
                                    }
                                }
                            }
                            // Incorporate the offset, then check against cycle size
                            m -= e;
                            return m === d || m % d === 0 && m / d >= 0;
                        }
                    };
                },
                PSEUDO: function(a, b) {
                    // pseudo-class names are case-insensitive
                    // http://www.w3.org/TR/selectors/#pseudo-classes
                    // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
                    // Remember that setFilters inherits from pseudos
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || fb.error("unsupported pseudo: " + a);
                    // The user may use createPseudo to indicate that
                    // arguments are needed to create the filter function
                    // just as Sizzle does
                    if (e[u]) {
                        return e(b);
                    }
                    // But maintain support for old signatures
                    if (e.length > 1) {
                        c = [ a, a, "", b ];
                        return d.setFilters.hasOwnProperty(a.toLowerCase()) ? hb(function(a, c) {
                            var d, f = e(a, b), g = f.length;
                            while (g--) {
                                d = K.call(a, f[g]);
                                a[d] = !(c[d] = f[g]);
                            }
                        }) : function(a) {
                            return e(a, 0, c);
                        };
                    }
                    return e;
                }
            },
            pseudos: {
                // Potentially complex pseudos
                not: hb(function(a) {
                    // Trim the selector passed to compile
                    // to avoid treating leading and trailing
                    // spaces as combinators
                    var b = [], c = [], d = h(a.replace(R, "$1"));
                    return d[u] ? hb(function(a, b, c, e) {
                        var f, g = d(a, null, e, []), h = a.length;
                        // Match elements unmatched by `matcher`
                        while (h--) {
                            if (f = g[h]) {
                                a[h] = !(b[h] = f);
                            }
                        }
                    }) : function(a, e, f) {
                        b[0] = a;
                        d(b, null, f, c);
                        return !c.pop();
                    };
                }),
                has: hb(function(a) {
                    return function(b) {
                        return fb(a, b).length > 0;
                    };
                }),
                contains: hb(function(a) {
                    return function(b) {
                        return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
                    };
                }),
                // "Whether an element is represented by a :lang() selector
                // is based solely on the element's language value
                // being equal to the identifier C,
                // or beginning with the identifier C immediately followed by "-".
                // The matching of C against the element's language value is performed case-insensitively.
                // The identifier C does not have to be a valid language name."
                // http://www.w3.org/TR/selectors/#lang-pseudo
                lang: hb(function(a) {
                    // lang value must be a valid identifier
                    if (!W.test(a || "")) {
                        fb.error("unsupported lang: " + a);
                    }
                    a = a.replace(cb, db).toLowerCase();
                    return function(b) {
                        var c;
                        do {
                            if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) {
                                c = c.toLowerCase();
                                return c === a || c.indexOf(a + "-") === 0;
                            }
                        } while ((b = b.parentNode) && b.nodeType === 1);
                        return false;
                    };
                }),
                // Miscellaneous
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id;
                },
                root: function(a) {
                    return a === o;
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
                },
                // Boolean properties
                enabled: function(a) {
                    return a.disabled === false;
                },
                disabled: function(a) {
                    return a.disabled === true;
                },
                checked: function(a) {
                    // In CSS3, :checked should return both checked and selected elements
                    // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
                    var b = a.nodeName.toLowerCase();
                    return b === "input" && !!a.checked || b === "option" && !!a.selected;
                },
                selected: function(a) {
                    // Accessing this property makes selected-by-default
                    // options in Safari work properly
                    if (a.parentNode) {
                        a.parentNode.selectedIndex;
                    }
                    return a.selected === true;
                },
                // Contents
                empty: function(a) {
                    // http://www.w3.org/TR/selectors/#empty-pseudo
                    // :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
                    //   but not by others (comment: 8; processing instruction: 7; etc.)
                    // nodeType < 6 works because attributes (2) do not appear as children
                    for (a = a.firstChild; a; a = a.nextSibling) {
                        if (a.nodeType < 6) {
                            return false;
                        }
                    }
                    return true;
                },
                parent: function(a) {
                    return !d.pseudos["empty"](a);
                },
                // Element/input types
                header: function(a) {
                    return Z.test(a.nodeName);
                },
                input: function(a) {
                    return Y.test(a.nodeName);
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return b === "input" && a.type === "button" || b === "button";
                },
                text: function(a) {
                    var b;
                    // Support: IE<8
                    // New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
                    return a.nodeName.toLowerCase() === "input" && a.type === "text" && ((b = a.getAttribute("type")) == null || b.toLowerCase() === "text");
                },
                // Position-in-collection
                first: nb(function() {
                    return [ 0 ];
                }),
                last: nb(function(a, b) {
                    return [ b - 1 ];
                }),
                eq: nb(function(a, b, c) {
                    return [ c < 0 ? c + b : c ];
                }),
                even: nb(function(a, b) {
                    var c = 0;
                    for (;c < b; c += 2) {
                        a.push(c);
                    }
                    return a;
                }),
                odd: nb(function(a, b) {
                    var c = 1;
                    for (;c < b; c += 2) {
                        a.push(c);
                    }
                    return a;
                }),
                lt: nb(function(a, b, c) {
                    var d = c < 0 ? c + b : c;
                    for (;--d >= 0; ) {
                        a.push(d);
                    }
                    return a;
                }),
                gt: nb(function(a, b, c) {
                    var d = c < 0 ? c + b : c;
                    for (;++d < b; ) {
                        a.push(d);
                    }
                    return a;
                })
            }
        };
        d.pseudos["nth"] = d.pseudos["eq"];
        // Add button/input type pseudos
        for (b in {
            radio: true,
            checkbox: true,
            file: true,
            password: true,
            image: true
        }) {
            d.pseudos[b] = lb(b);
        }
        for (b in {
            submit: true,
            reset: true
        }) {
            d.pseudos[b] = mb(b);
        }
        // Easy API for creating new setFilters
        function pb() {}
        pb.prototype = d.filters = d.pseudos;
        d.setFilters = new pb();
        g = fb.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) {
                return b ? 0 : k.slice(0);
            }
            h = a;
            i = [];
            j = d.preFilter;
            while (h) {
                // Comma and first run
                if (!c || (e = S.exec(h))) {
                    if (e) {
                        // Don't consume trailing commas as valid
                        h = h.slice(e[0].length) || h;
                    }
                    i.push(f = []);
                }
                c = false;
                // Combinators
                if (e = T.exec(h)) {
                    c = e.shift();
                    f.push({
                        value: c,
                        // Cast descendant combinators to space
                        type: e[0].replace(R, " ")
                    });
                    h = h.slice(c.length);
                }
                // Filters
                for (g in d.filter) {
                    if ((e = X[g].exec(h)) && (!j[g] || (e = j[g](e)))) {
                        c = e.shift();
                        f.push({
                            value: c,
                            type: g,
                            matches: e
                        });
                        h = h.slice(c.length);
                    }
                }
                if (!c) {
                    break;
                }
            }
            // Return the length of the invalid excess
            // if we're just parsing
            // Otherwise, throw an error or return tokens
            // Cache the tokens
            return b ? h.length : h ? fb.error(a) : z(a, i).slice(0);
        };
        function qb(a) {
            var b = 0, c = a.length, d = "";
            for (;b < c; b++) {
                d += a[b].value;
            }
            return d;
        }
        function rb(a, b, c) {
            var d = b.dir, e = c && d === "parentNode", f = x++;
            // Check against closest ancestor/preceding element
            // Check against all ancestor/preceding elements
            return b.first ? function(b, c, f) {
                while (b = b[d]) {
                    if (b.nodeType === 1 || e) {
                        return a(b, c, f);
                    }
                }
            } : function(b, c, g) {
                var h, i, j = [ w, f ];
                // We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
                if (g) {
                    while (b = b[d]) {
                        if (b.nodeType === 1 || e) {
                            if (a(b, c, g)) {
                                return true;
                            }
                        }
                    }
                } else {
                    while (b = b[d]) {
                        if (b.nodeType === 1 || e) {
                            i = b[u] || (b[u] = {});
                            if ((h = i[d]) && h[0] === w && h[1] === f) {
                                // Assign to newCache so results back-propagate to previous elements
                                return j[2] = h[2];
                            } else {
                                // Reuse newcache so results back-propagate to previous elements
                                i[d] = j;
                                // A match means we're done; a fail means we have to keep checking
                                if (j[2] = a(b, c, g)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            };
        }
        function sb(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--) {
                    if (!a[e](b, c, d)) {
                        return false;
                    }
                }
                return true;
            } : a[0];
        }
        function tb(a, b, c) {
            var d = 0, e = b.length;
            for (;d < e; d++) {
                fb(a, b[d], c);
            }
            return c;
        }
        function ub(a, b, c, d, e) {
            var f, g = [], h = 0, i = a.length, j = b != null;
            for (;h < i; h++) {
                if (f = a[h]) {
                    if (!c || c(f, d, e)) {
                        g.push(f);
                        if (j) {
                            b.push(h);
                        }
                    }
                }
            }
            return g;
        }
        function vb(a, b, c, d, e, f) {
            if (d && !d[u]) {
                d = vb(d);
            }
            if (e && !e[u]) {
                e = vb(e, f);
            }
            return hb(function(f, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, // Get initial elements from seed or context
                p = f || tb(b || "*", h.nodeType ? [ h ] : h, []), // Prefilter to get matcher input, preserving a map for seed-results synchronization
                q = a && (f || !b) ? ub(p, m, a, h, i) : p, r = c ? // If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
                e || (f ? a : o || d) ? // ...intermediate processing is necessary
                [] : // ...otherwise use results directly
                g : q;
                // Find primary matches
                if (c) {
                    c(q, r, h, i);
                }
                // Apply postFilter
                if (d) {
                    j = ub(r, n);
                    d(j, [], h, i);
                    // Un-match failing elements by moving them back to matcherIn
                    k = j.length;
                    while (k--) {
                        if (l = j[k]) {
                            r[n[k]] = !(q[n[k]] = l);
                        }
                    }
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            // Get the final matcherOut by condensing this intermediate into postFinder contexts
                            j = [];
                            k = r.length;
                            while (k--) {
                                if (l = r[k]) {
                                    // Restore matcherIn since elem is not yet a final match
                                    j.push(q[k] = l);
                                }
                            }
                            e(null, r = [], j, i);
                        }
                        // Move matched elements from seed to results to keep them synchronized
                        k = r.length;
                        while (k--) {
                            if ((l = r[k]) && (j = e ? K.call(f, l) : m[k]) > -1) {
                                f[j] = !(g[j] = l);
                            }
                        }
                    }
                } else {
                    r = ub(r === g ? r.splice(o, r.length) : r);
                    if (e) {
                        e(null, g, r, i);
                    } else {
                        I.apply(g, r);
                    }
                }
            });
        }
        function wb(a) {
            var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, // The foundational matcher ensures that elements are reachable from top-level context(s)
            k = rb(function(a) {
                return a === b;
            }, h, true), l = rb(function(a) {
                return K.call(b, a) > -1;
            }, h, true), m = [ function(a, c, d) {
                return !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
            } ];
            for (;i < f; i++) {
                if (c = d.relative[a[i].type]) {
                    m = [ rb(sb(m), c) ];
                } else {
                    c = d.filter[a[i].type].apply(null, a[i].matches);
                    // Return special upon seeing a positional matcher
                    if (c[u]) {
                        // Find the next relative operator (if any) for proper handling
                        e = ++i;
                        for (;e < f; e++) {
                            if (d.relative[a[e].type]) {
                                break;
                            }
                        }
                        // If the preceding token was a descendant combinator, insert an implicit any-element `*`
                        return vb(i > 1 && sb(m), i > 1 && qb(a.slice(0, i - 1).concat({
                            value: a[i - 2].type === " " ? "*" : ""
                        })).replace(R, "$1"), c, i < e && wb(a.slice(i, e)), e < f && wb(a = a.slice(e)), e < f && qb(a));
                    }
                    m.push(c);
                }
            }
            return sb(m);
        }
        function xb(a, b) {
            var c = b.length > 0, e = a.length > 0, f = function(f, g, h, i, k) {
                var l, m, o, p = 0, q = "0", r = f && [], s = [], t = j, // We must always have either seed elements or outermost context
                u = f || e && d.find["TAG"]("*", k), // Use integer dirruns iff this is the outermost matcher
                v = w += t == null ? 1 : Math.random() || .1, x = u.length;
                if (k) {
                    j = g !== n && g;
                }
                // Add elements passing elementMatchers directly to results
                // Keep `i` a string if there are no elements so `matchedCount` will be "00" below
                // Support: IE<9, Safari
                // Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
                for (;q !== x && (l = u[q]) != null; q++) {
                    if (e && l) {
                        m = 0;
                        while (o = a[m++]) {
                            if (o(l, g, h)) {
                                i.push(l);
                                break;
                            }
                        }
                        if (k) {
                            w = v;
                        }
                    }
                    // Track unmatched elements for set filters
                    if (c) {
                        // They will have gone through all possible matchers
                        if (l = !o && l) {
                            p--;
                        }
                        // Lengthen the array for every element, matched or not
                        if (f) {
                            r.push(l);
                        }
                    }
                }
                // Apply set filters to unmatched elements
                p += q;
                if (c && q !== p) {
                    m = 0;
                    while (o = b[m++]) {
                        o(r, s, g, h);
                    }
                    if (f) {
                        // Reintegrate element matches to eliminate the need for sorting
                        if (p > 0) {
                            while (q--) {
                                if (!(r[q] || s[q])) {
                                    s[q] = G.call(i);
                                }
                            }
                        }
                        // Discard index placeholder values to get only actual matches
                        s = ub(s);
                    }
                    // Add matches to results
                    I.apply(i, s);
                    // Seedless set matches succeeding multiple successful matchers stipulate sorting
                    if (k && !f && s.length > 0 && p + b.length > 1) {
                        fb.uniqueSort(i);
                    }
                }
                // Override manipulation of globals by nested matchers
                if (k) {
                    w = v;
                    j = t;
                }
                return r;
            };
            return c ? hb(f) : f;
        }
        h = fb.compile = function(a, b) {
            var c, d = [], e = [], f = A[a + " "];
            if (!f) {
                // Generate a function of recursive functions that can be used to check each element
                if (!b) {
                    b = g(a);
                }
                c = b.length;
                while (c--) {
                    f = wb(b[c]);
                    if (f[u]) {
                        d.push(f);
                    } else {
                        e.push(f);
                    }
                }
                // Cache the compiled function
                f = A(a, xb(e, d));
                // Save selector and tokenization
                f.selector = a;
            }
            return f;
        };
        /**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
        i = fb.select = function(a, b, e, f) {
            var i, j, k, l, m, n = typeof a === "function" && a, o = !f && g(a = n.selector || a);
            e = e || [];
            // Try to minimize operations if there is no seed and only one group
            if (o.length === 1) {
                // Take a shortcut and set the context if the root selector is an ID
                j = o[0] = o[0].slice(0);
                if (j.length > 2 && (k = j[0]).type === "ID" && c.getById && b.nodeType === 9 && p && d.relative[j[1].type]) {
                    b = (d.find["ID"](k.matches[0].replace(cb, db), b) || [])[0];
                    if (!b) {
                        return e;
                    } else if (n) {
                        b = b.parentNode;
                    }
                    a = a.slice(j.shift().value.length);
                }
                // Fetch a seed set for right-to-left matching
                i = X["needsContext"].test(a) ? 0 : j.length;
                while (i--) {
                    k = j[i];
                    // Abort if we hit a combinator
                    if (d.relative[l = k.type]) {
                        break;
                    }
                    if (m = d.find[l]) {
                        // Search, expanding context for leading sibling combinators
                        if (f = m(k.matches[0].replace(cb, db), ab.test(j[0].type) && ob(b.parentNode) || b)) {
                            // If seed is empty or no tokens remain, we can return early
                            j.splice(i, 1);
                            a = f.length && qb(j);
                            if (!a) {
                                I.apply(e, f);
                                return e;
                            }
                            break;
                        }
                    }
                }
            }
            // Compile and execute a filtering function if one is not provided
            // Provide `match` to avoid retokenization if we modified the selector above
            (n || h(a, o))(f, b, !p, e, ab.test(a) && ob(b.parentNode) || b);
            return e;
        };
        // One-time assignments
        // Sort stability
        c.sortStable = u.split("").sort(B).join("") === u;
        // Support: Chrome<14
        // Always assume duplicates if they aren't passed to the comparison function
        c.detectDuplicates = !!l;
        // Initialize against the default document
        m();
        // Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
        // Detached nodes confoundingly follow *each other*
        c.sortDetached = ib(function(a) {
            // Should return 1, but returns 4 (following)
            return a.compareDocumentPosition(n.createElement("div")) & 1;
        });
        // Support: IE<8
        // Prevent attribute/property "interpolation"
        // http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
        if (!ib(function(a) {
            a.innerHTML = "<a href='#'></a>";
            return a.firstChild.getAttribute("href") === "#";
        })) {
            jb("type|href|height|width", function(a, b, c) {
                if (!c) {
                    return a.getAttribute(b, b.toLowerCase() === "type" ? 1 : 2);
                }
            });
        }
        // Support: IE<9
        // Use defaultValue in place of getAttribute("value")
        if (!c.attributes || !ib(function(a) {
            a.innerHTML = "<input/>";
            a.firstChild.setAttribute("value", "");
            return a.firstChild.getAttribute("value") === "";
        })) {
            jb("value", function(a, b, c) {
                if (!c && a.nodeName.toLowerCase() === "input") {
                    return a.defaultValue;
                }
            });
        }
        // Support: IE<9
        // Use getAttributeNode to fetch booleans when getAttribute lies
        if (!ib(function(a) {
            return a.getAttribute("disabled") == null;
        })) {
            jb(L, function(a, b, c) {
                var d;
                if (!c) {
                    return a[b] === true ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
                }
            });
        }
        return fb;
    }(a);
    m.find = s;
    m.expr = s.selectors;
    m.expr[":"] = m.expr.pseudos;
    m.unique = s.uniqueSort;
    m.text = s.getText;
    m.isXMLDoc = s.isXML;
    m.contains = s.contains;
    var t = m.expr.match.needsContext;
    var u = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
    var v = /^.[^:#\[\.,]*$/;
    // Implement the identical functionality for filter and not
    function w(a, b, c) {
        if (m.isFunction(b)) {
            return m.grep(a, function(a, d) {
                /* jshint -W018 */
                return !!b.call(a, d, a) !== c;
            });
        }
        if (b.nodeType) {
            return m.grep(a, function(a) {
                return a === b !== c;
            });
        }
        if (typeof b === "string") {
            if (v.test(b)) {
                return m.filter(b, a, c);
            }
            b = m.filter(b, a);
        }
        return m.grep(a, function(a) {
            return m.inArray(a, b) >= 0 !== c;
        });
    }
    m.filter = function(a, b, c) {
        var d = b[0];
        if (c) {
            a = ":not(" + a + ")";
        }
        return b.length === 1 && d.nodeType === 1 ? m.find.matchesSelector(d, a) ? [ d ] : [] : m.find.matches(a, m.grep(b, function(a) {
            return a.nodeType === 1;
        }));
    };
    m.fn.extend({
        find: function(a) {
            var b, c = [], d = this, e = d.length;
            if (typeof a !== "string") {
                return this.pushStack(m(a).filter(function() {
                    for (b = 0; b < e; b++) {
                        if (m.contains(d[b], this)) {
                            return true;
                        }
                    }
                }));
            }
            for (b = 0; b < e; b++) {
                m.find(a, d[b], c);
            }
            // Needed because $( selector, context ) becomes $( context ).find( selector )
            c = this.pushStack(e > 1 ? m.unique(c) : c);
            c.selector = this.selector ? this.selector + " " + a : a;
            return c;
        },
        filter: function(a) {
            return this.pushStack(w(this, a || [], false));
        },
        not: function(a) {
            return this.pushStack(w(this, a || [], true));
        },
        is: function(a) {
            // If this is a positional/relative selector, check membership in the returned set
            // so $("p:first").is("p:last") won't return true for a doc with two "p".
            return !!w(this, typeof a === "string" && t.test(a) ? m(a) : a || [], false).length;
        }
    });
    // Initialize a jQuery object
    // A central reference to the root jQuery(document)
    var x, // Use the correct document accordingly with window argument (sandbox)
    y = a.document, // A simple way to check for HTML strings
    // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
    // Strict HTML recognition (#11290: must start with <)
    z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, A = m.fn.init = function(a, b) {
        var c, d;
        // HANDLE: $(""), $(null), $(undefined), $(false)
        if (!a) {
            return this;
        }
        // Handle HTML strings
        if (typeof a === "string") {
            if (a.charAt(0) === "<" && a.charAt(a.length - 1) === ">" && a.length >= 3) {
                // Assume that strings that start and end with <> are HTML and skip the regex check
                c = [ null, a, null ];
            } else {
                c = z.exec(a);
            }
            // Match html or make sure no context is specified for #id
            if (c && (c[1] || !b)) {
                // HANDLE: $(html) -> $(array)
                if (c[1]) {
                    b = b instanceof m ? b[0] : b;
                    // scripts is true for back-compat
                    // Intentionally let the error be thrown if parseHTML is not present
                    m.merge(this, m.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : y, true));
                    // HANDLE: $(html, props)
                    if (u.test(c[1]) && m.isPlainObject(b)) {
                        for (c in b) {
                            // Properties of context are called as methods if possible
                            if (m.isFunction(this[c])) {
                                this[c](b[c]);
                            } else {
                                this.attr(c, b[c]);
                            }
                        }
                    }
                    return this;
                } else {
                    d = y.getElementById(c[2]);
                    // Check parentNode to catch when Blackberry 4.6 returns
                    // nodes that are no longer in the document #6963
                    if (d && d.parentNode) {
                        // Handle the case where IE and Opera return items
                        // by name instead of ID
                        if (d.id !== c[2]) {
                            return x.find(a);
                        }
                        // Otherwise, we inject the element directly into the jQuery object
                        this.length = 1;
                        this[0] = d;
                    }
                    this.context = y;
                    this.selector = a;
                    return this;
                }
            } else if (!b || b.jquery) {
                return (b || x).find(a);
            } else {
                return this.constructor(b).find(a);
            }
        } else if (a.nodeType) {
            this.context = this[0] = a;
            this.length = 1;
            return this;
        } else if (m.isFunction(a)) {
            // Execute immediately if ready is not present
            return typeof x.ready !== "undefined" ? x.ready(a) : a(m);
        }
        if (a.selector !== undefined) {
            this.selector = a.selector;
            this.context = a.context;
        }
        return m.makeArray(a, this);
    };
    // Give the init function the jQuery prototype for later instantiation
    A.prototype = m.fn;
    // Initialize central reference
    x = m(y);
    var B = /^(?:parents|prev(?:Until|All))/, // methods guaranteed to produce a unique set when starting from a unique set
    C = {
        children: true,
        contents: true,
        next: true,
        prev: true
    };
    m.extend({
        dir: function(a, b, c) {
            var d = [], e = a[b];
            while (e && e.nodeType !== 9 && (c === undefined || e.nodeType !== 1 || !m(e).is(c))) {
                if (e.nodeType === 1) {
                    d.push(e);
                }
                e = e[b];
            }
            return d;
        },
        sibling: function(a, b) {
            var c = [];
            for (;a; a = a.nextSibling) {
                if (a.nodeType === 1 && a !== b) {
                    c.push(a);
                }
            }
            return c;
        }
    });
    m.fn.extend({
        has: function(a) {
            var b, c = m(a, this), d = c.length;
            return this.filter(function() {
                for (b = 0; b < d; b++) {
                    if (m.contains(this, c[b])) {
                        return true;
                    }
                }
            });
        },
        closest: function(a, b) {
            var c, d = 0, e = this.length, f = [], g = t.test(a) || typeof a !== "string" ? m(a, b || this.context) : 0;
            for (;d < e; d++) {
                for (c = this[d]; c && c !== b; c = c.parentNode) {
                    // Always skip document fragments
                    if (c.nodeType < 11 && (g ? g.index(c) > -1 : // Don't pass non-elements to Sizzle
                    c.nodeType === 1 && m.find.matchesSelector(c, a))) {
                        f.push(c);
                        break;
                    }
                }
            }
            return this.pushStack(f.length > 1 ? m.unique(f) : f);
        },
        // Determine the position of an element within
        // the matched set of elements
        index: function(a) {
            // No argument, return index in parent
            if (!a) {
                return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
            }
            // index in selector
            if (typeof a === "string") {
                return m.inArray(this[0], m(a));
            }
            // Locate the position of the desired element
            // If it receives a jQuery object, the first element is used
            return m.inArray(a.jquery ? a[0] : a, this);
        },
        add: function(a, b) {
            return this.pushStack(m.unique(m.merge(this.get(), m(a, b))));
        },
        addBack: function(a) {
            return this.add(a == null ? this.prevObject : this.prevObject.filter(a));
        }
    });
    function D(a, b) {
        do {
            a = a[b];
        } while (a && a.nodeType !== 1);
        return a;
    }
    m.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && b.nodeType !== 11 ? b : null;
        },
        parents: function(a) {
            return m.dir(a, "parentNode");
        },
        parentsUntil: function(a, b, c) {
            return m.dir(a, "parentNode", c);
        },
        next: function(a) {
            return D(a, "nextSibling");
        },
        prev: function(a) {
            return D(a, "previousSibling");
        },
        nextAll: function(a) {
            return m.dir(a, "nextSibling");
        },
        prevAll: function(a) {
            return m.dir(a, "previousSibling");
        },
        nextUntil: function(a, b, c) {
            return m.dir(a, "nextSibling", c);
        },
        prevUntil: function(a, b, c) {
            return m.dir(a, "previousSibling", c);
        },
        siblings: function(a) {
            return m.sibling((a.parentNode || {}).firstChild, a);
        },
        children: function(a) {
            return m.sibling(a.firstChild);
        },
        contents: function(a) {
            return m.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : m.merge([], a.childNodes);
        }
    }, function(a, b) {
        m.fn[a] = function(c, d) {
            var e = m.map(this, b, c);
            if (a.slice(-5) !== "Until") {
                d = c;
            }
            if (d && typeof d === "string") {
                e = m.filter(d, e);
            }
            if (this.length > 1) {
                // Remove duplicates
                if (!C[a]) {
                    e = m.unique(e);
                }
                // Reverse order for parents* and prev-derivatives
                if (B.test(a)) {
                    e = e.reverse();
                }
            }
            return this.pushStack(e);
        };
    });
    var E = /\S+/g;
    // String to Object options format cache
    var F = {};
    // Convert String-formatted options into Object-formatted ones and store in cache
    function G(a) {
        var b = F[a] = {};
        m.each(a.match(E) || [], function(a, c) {
            b[c] = true;
        });
        return b;
    }
    /*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
    m.Callbacks = function(a) {
        // Convert options from String-formatted to Object-formatted if needed
        // (we check in cache first)
        a = typeof a === "string" ? F[a] || G(a) : m.extend({}, a);
        var // Flag to know if list is currently firing
        b, // Last fire value (for non-forgettable lists)
        c, // Flag to know if list was already fired
        d, // End of the loop when firing
        e, // Index of currently firing callback (modified by remove if needed)
        f, // First callback to fire (used internally by add and fireWith)
        g, // Actual callback list
        h = [], // Stack of fire calls for repeatable lists
        i = !a.once && [], // Fire callbacks
        j = function(l) {
            c = a.memory && l;
            d = true;
            f = g || 0;
            g = 0;
            e = h.length;
            b = true;
            for (;h && f < e; f++) {
                if (h[f].apply(l[0], l[1]) === false && a.stopOnFalse) {
                    c = false;
                    // To prevent further calls using add
                    break;
                }
            }
            b = false;
            if (h) {
                if (i) {
                    if (i.length) {
                        j(i.shift());
                    }
                } else if (c) {
                    h = [];
                } else {
                    k.disable();
                }
            }
        }, // Actual Callbacks object
        k = {
            // Add a callback or a collection of callbacks to the list
            add: function() {
                if (h) {
                    // First, we save the current length
                    var d = h.length;
                    (function f(b) {
                        m.each(b, function(b, c) {
                            var d = m.type(c);
                            if (d === "function") {
                                if (!a.unique || !k.has(c)) {
                                    h.push(c);
                                }
                            } else if (c && c.length && d !== "string") {
                                // Inspect recursively
                                f(c);
                            }
                        });
                    })(arguments);
                    // Do we need to add the callbacks to the
                    // current firing batch?
                    if (b) {
                        e = h.length;
                    } else if (c) {
                        g = d;
                        j(c);
                    }
                }
                return this;
            },
            // Remove a callback from the list
            remove: function() {
                if (h) {
                    m.each(arguments, function(a, c) {
                        var d;
                        while ((d = m.inArray(c, h, d)) > -1) {
                            h.splice(d, 1);
                            // Handle firing indexes
                            if (b) {
                                if (d <= e) {
                                    e--;
                                }
                                if (d <= f) {
                                    f--;
                                }
                            }
                        }
                    });
                }
                return this;
            },
            // Check if a given callback is in the list.
            // If no argument is given, return whether or not list has callbacks attached.
            has: function(a) {
                return a ? m.inArray(a, h) > -1 : !!(h && h.length);
            },
            // Remove all callbacks from the list
            empty: function() {
                h = [];
                e = 0;
                return this;
            },
            // Have the list do nothing anymore
            disable: function() {
                h = i = c = undefined;
                return this;
            },
            // Is it disabled?
            disabled: function() {
                return !h;
            },
            // Lock the list in its current state
            lock: function() {
                i = undefined;
                if (!c) {
                    k.disable();
                }
                return this;
            },
            // Is it locked?
            locked: function() {
                return !i;
            },
            // Call all callbacks with the given context and arguments
            fireWith: function(a, c) {
                if (h && (!d || i)) {
                    c = c || [];
                    c = [ a, c.slice ? c.slice() : c ];
                    if (b) {
                        i.push(c);
                    } else {
                        j(c);
                    }
                }
                return this;
            },
            // Call all the callbacks with the given arguments
            fire: function() {
                k.fireWith(this, arguments);
                return this;
            },
            // To know if the callbacks have already been called at least once
            fired: function() {
                return !!d;
            }
        };
        return k;
    };
    m.extend({
        Deferred: function(a) {
            var b = [ // action, add listener, listener list, final state
            [ "resolve", "done", m.Callbacks("once memory"), "resolved" ], [ "reject", "fail", m.Callbacks("once memory"), "rejected" ], [ "notify", "progress", m.Callbacks("memory") ] ], c = "pending", d = {
                state: function() {
                    return c;
                },
                always: function() {
                    e.done(arguments).fail(arguments);
                    return this;
                },
                then: function() {
                    var a = arguments;
                    return m.Deferred(function(c) {
                        m.each(b, function(b, f) {
                            var g = m.isFunction(a[b]) && a[b];
                            // deferred[ done | fail | progress ] for forwarding actions to newDefer
                            e[f[1]](function() {
                                var a = g && g.apply(this, arguments);
                                if (a && m.isFunction(a.promise)) {
                                    a.promise().done(c.resolve).fail(c.reject).progress(c.notify);
                                } else {
                                    c[f[0] + "With"](this === d ? c.promise() : this, g ? [ a ] : arguments);
                                }
                            });
                        });
                        a = null;
                    }).promise();
                },
                // Get a promise for this deferred
                // If obj is provided, the promise aspect is added to the object
                promise: function(a) {
                    return a != null ? m.extend(a, d) : d;
                }
            }, e = {};
            // Keep pipe for back-compat
            d.pipe = d.then;
            // Add list-specific methods
            m.each(b, function(a, f) {
                var g = f[2], h = f[3];
                // promise[ done | fail | progress ] = list.add
                d[f[1]] = g.add;
                // Handle state
                if (h) {
                    g.add(function() {
                        // state = [ resolved | rejected ]
                        c = h;
                    }, b[a ^ 1][2].disable, b[2][2].lock);
                }
                // deferred[ resolve | reject | notify ]
                e[f[0]] = function() {
                    e[f[0] + "With"](this === e ? d : this, arguments);
                    return this;
                };
                e[f[0] + "With"] = g.fireWith;
            });
            // Make the deferred a promise
            d.promise(e);
            // Call given func if any
            if (a) {
                a.call(e, e);
            }
            // All done!
            return e;
        },
        // Deferred helper
        when: function(a) {
            var b = 0, c = d.call(arguments), e = c.length, // the count of uncompleted subordinates
            f = e !== 1 || a && m.isFunction(a.promise) ? e : 0, // the master Deferred. If resolveValues consist of only a single Deferred, just use that.
            g = f === 1 ? a : m.Deferred(), // Update function for both resolve and progress values
            h = function(a, b, c) {
                return function(e) {
                    b[a] = this;
                    c[a] = arguments.length > 1 ? d.call(arguments) : e;
                    if (c === i) {
                        g.notifyWith(b, c);
                    } else if (!--f) {
                        g.resolveWith(b, c);
                    }
                };
            }, i, j, k;
            // add listeners to Deferred subordinates; treat others as resolved
            if (e > 1) {
                i = new Array(e);
                j = new Array(e);
                k = new Array(e);
                for (;b < e; b++) {
                    if (c[b] && m.isFunction(c[b].promise)) {
                        c[b].promise().done(h(b, k, c)).fail(g.reject).progress(h(b, j, i));
                    } else {
                        --f;
                    }
                }
            }
            // if we're not waiting on anything, resolve the master
            if (!f) {
                g.resolveWith(k, c);
            }
            return g.promise();
        }
    });
    // The deferred used on DOM ready
    var H;
    m.fn.ready = function(a) {
        // Add the callback
        m.ready.promise().done(a);
        return this;
    };
    m.extend({
        // Is the DOM ready to be used? Set to true once it occurs.
        isReady: false,
        // A counter to track how many items to wait for before
        // the ready event fires. See #6781
        readyWait: 1,
        // Hold (or release) the ready event
        holdReady: function(a) {
            if (a) {
                m.readyWait++;
            } else {
                m.ready(true);
            }
        },
        // Handle when the DOM is ready
        ready: function(a) {
            // Abort if there are pending holds or we're already ready
            if (a === true ? --m.readyWait : m.isReady) {
                return;
            }
            // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
            if (!y.body) {
                return setTimeout(m.ready);
            }
            // Remember that the DOM is ready
            m.isReady = true;
            // If a normal DOM Ready event fired, decrement, and wait if need be
            if (a !== true && --m.readyWait > 0) {
                return;
            }
            // If there are functions bound, to execute
            H.resolveWith(y, [ m ]);
            // Trigger any bound ready events
            if (m.fn.triggerHandler) {
                m(y).triggerHandler("ready");
                m(y).off("ready");
            }
        }
    });
    /**
 * Clean-up method for dom ready events
 */
    function I() {
        if (y.addEventListener) {
            y.removeEventListener("DOMContentLoaded", J, false);
            a.removeEventListener("load", J, false);
        } else {
            y.detachEvent("onreadystatechange", J);
            a.detachEvent("onload", J);
        }
    }
    /**
 * The ready event handler and self cleanup method
 */
    function J() {
        // readyState === "complete" is good enough for us to call the dom ready in oldIE
        if (y.addEventListener || event.type === "load" || y.readyState === "complete") {
            I();
            m.ready();
        }
    }
    m.ready.promise = function(b) {
        if (!H) {
            H = m.Deferred();
            // Catch cases where $(document).ready() is called after the browser event has already occurred.
            // we once tried to use readyState "interactive" here, but it caused issues like the one
            // discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
            if (y.readyState === "complete") {
                // Handle it asynchronously to allow scripts the opportunity to delay ready
                setTimeout(m.ready);
            } else if (y.addEventListener) {
                // Use the handy event callback
                y.addEventListener("DOMContentLoaded", J, false);
                // A fallback to window.onload, that will always work
                a.addEventListener("load", J, false);
            } else {
                // Ensure firing before onload, maybe late but safe also for iframes
                y.attachEvent("onreadystatechange", J);
                // A fallback to window.onload, that will always work
                a.attachEvent("onload", J);
                // If IE and not a frame
                // continually check to see if the document is ready
                var c = false;
                try {
                    c = a.frameElement == null && y.documentElement;
                } catch (d) {}
                if (c && c.doScroll) {
                    (function e() {
                        if (!m.isReady) {
                            try {
                                // Use the trick by Diego Perini
                                // http://javascript.nwbox.com/IEContentLoaded/
                                c.doScroll("left");
                            } catch (a) {
                                return setTimeout(e, 50);
                            }
                            // detach all dom ready events
                            I();
                            // and execute any waiting functions
                            m.ready();
                        }
                    })();
                }
            }
        }
        return H.promise(b);
    };
    var K = typeof undefined;
    // Support: IE<9
    // Iteration over object's inherited properties before its own
    var L;
    for (L in m(k)) {
        break;
    }
    k.ownLast = L !== "0";
    // Note: most support tests are defined in their respective modules.
    // false until the test is run
    k.inlineBlockNeedsLayout = false;
    // Execute ASAP in case we need to set body.style.zoom
    m(function() {
        // Minified: var a,b,c,d
        var a, b, c, d;
        c = y.getElementsByTagName("body")[0];
        if (!c || !c.style) {
            // Return for frameset docs that don't have a body
            return;
        }
        // Setup
        b = y.createElement("div");
        d = y.createElement("div");
        d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px";
        c.appendChild(d).appendChild(b);
        if (typeof b.style.zoom !== K) {
            // Support: IE<8
            // Check if natively block-level elements act like inline-block
            // elements when setting their display to 'inline' and giving
            // them layout
            b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1";
            k.inlineBlockNeedsLayout = a = b.offsetWidth === 3;
            if (a) {
                // Prevent IE 6 from affecting layout for positioned elements #11048
                // Prevent IE from shrinking the body in IE 7 mode #12869
                // Support: IE<8
                c.style.zoom = 1;
            }
        }
        c.removeChild(d);
    });
    (function() {
        var a = y.createElement("div");
        // Execute the test only if not already executed in another module.
        if (k.deleteExpando == null) {
            // Support: IE<9
            k.deleteExpando = true;
            try {
                delete a.test;
            } catch (b) {
                k.deleteExpando = false;
            }
        }
        // Null elements to avoid leaks in IE.
        a = null;
    })();
    /**
 * Determines whether an object can have data
 */
    m.acceptData = function(a) {
        var b = m.noData[(a.nodeName + " ").toLowerCase()], c = +a.nodeType || 1;
        // Do not set data on non-element DOM nodes because it will not be cleared (#8335).
        // Nodes accept data unless otherwise specified; rejection can be conditional
        return c !== 1 && c !== 9 ? false : !b || b !== true && a.getAttribute("classid") === b;
    };
    var M = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, N = /([A-Z])/g;
    function O(a, b, c) {
        // If nothing was found internally, try to fetch any
        // data from the HTML5 data-* attribute
        if (c === undefined && a.nodeType === 1) {
            var d = "data-" + b.replace(N, "-$1").toLowerCase();
            c = a.getAttribute(d);
            if (typeof c === "string") {
                try {
                    c = c === "true" ? true : c === "false" ? false : c === "null" ? null : // Only convert to a number if it doesn't change the string
                    +c + "" === c ? +c : M.test(c) ? m.parseJSON(c) : c;
                } catch (e) {}
                // Make sure we set the data so it isn't changed later
                m.data(a, b, c);
            } else {
                c = undefined;
            }
        }
        return c;
    }
    // checks a cache object for emptiness
    function P(a) {
        var b;
        for (b in a) {
            // if the public data object is empty, the private is still empty
            if (b === "data" && m.isEmptyObject(a[b])) {
                continue;
            }
            if (b !== "toJSON") {
                return false;
            }
        }
        return true;
    }
    function Q(a, b, d, e) {
        if (!m.acceptData(a)) {
            return;
        }
        var f, g, h = m.expando, // We have to handle DOM nodes and JS objects differently because IE6-7
        // can't GC object references properly across the DOM-JS boundary
        i = a.nodeType, // Only DOM nodes need the global jQuery cache; JS object data is
        // attached directly to the object so GC can occur automatically
        j = i ? m.cache : a, // Only defining an ID for JS objects if its cache already exists allows
        // the code to shortcut on the same path as a DOM node with no cache
        k = i ? a[h] : a[h] && h;
        // Avoid doing any more work than we need to when trying to get data on an
        // object that has no data at all
        if ((!k || !j[k] || !e && !j[k].data) && d === undefined && typeof b === "string") {
            return;
        }
        if (!k) {
            // Only DOM nodes need a new unique ID for each element since their data
            // ends up in the global cache
            if (i) {
                k = a[h] = c.pop() || m.guid++;
            } else {
                k = h;
            }
        }
        if (!j[k]) {
            // Avoid exposing jQuery metadata on plain JS objects when the object
            // is serialized using JSON.stringify
            j[k] = i ? {} : {
                toJSON: m.noop
            };
        }
        // An object can be passed to jQuery.data instead of a key/value pair; this gets
        // shallow copied over onto the existing cache
        if (typeof b === "object" || typeof b === "function") {
            if (e) {
                j[k] = m.extend(j[k], b);
            } else {
                j[k].data = m.extend(j[k].data, b);
            }
        }
        g = j[k];
        // jQuery data() is stored in a separate object inside the object's internal data
        // cache in order to avoid key collisions between internal data and user-defined
        // data.
        if (!e) {
            if (!g.data) {
                g.data = {};
            }
            g = g.data;
        }
        if (d !== undefined) {
            g[m.camelCase(b)] = d;
        }
        // Check for both converted-to-camel and non-converted data property names
        // If a data property was specified
        if (typeof b === "string") {
            // First Try to find as-is property data
            f = g[b];
            // Test for null|undefined property data
            if (f == null) {
                // Try to find the camelCased property
                f = g[m.camelCase(b)];
            }
        } else {
            f = g;
        }
        return f;
    }
    function R(a, b, c) {
        if (!m.acceptData(a)) {
            return;
        }
        var d, e, f = a.nodeType, // See jQuery.data for more information
        g = f ? m.cache : a, h = f ? a[m.expando] : m.expando;
        // If there is already no cache entry for this object, there is no
        // purpose in continuing
        if (!g[h]) {
            return;
        }
        if (b) {
            d = c ? g[h] : g[h].data;
            if (d) {
                // Support array or space separated string names for data keys
                if (!m.isArray(b)) {
                    // try the string as a key before any manipulation
                    if (b in d) {
                        b = [ b ];
                    } else {
                        // split the camel cased version by spaces unless a key with the spaces exists
                        b = m.camelCase(b);
                        if (b in d) {
                            b = [ b ];
                        } else {
                            b = b.split(" ");
                        }
                    }
                } else {
                    // If "name" is an array of keys...
                    // When data is initially created, via ("key", "val") signature,
                    // keys will be converted to camelCase.
                    // Since there is no way to tell _how_ a key was added, remove
                    // both plain key and camelCase key. #12786
                    // This will only penalize the array argument path.
                    b = b.concat(m.map(b, m.camelCase));
                }
                e = b.length;
                while (e--) {
                    delete d[b[e]];
                }
                // If there is no data left in the cache, we want to continue
                // and let the cache object itself get destroyed
                if (c ? !P(d) : !m.isEmptyObject(d)) {
                    return;
                }
            }
        }
        // See jQuery.data for more information
        if (!c) {
            delete g[h].data;
            // Don't destroy the parent cache unless the internal data object
            // had been the only thing left in it
            if (!P(g[h])) {
                return;
            }
        }
        // Destroy the cache
        if (f) {
            m.cleanData([ a ], true);
        } else if (k.deleteExpando || g != g.window) {
            /* jshint eqeqeq: true */
            delete g[h];
        } else {
            g[h] = null;
        }
    }
    m.extend({
        cache: {},
        // The following elements (space-suffixed to avoid Object.prototype collisions)
        // throw uncatchable exceptions if you attempt to set expando properties
        noData: {
            "applet ": true,
            "embed ": true,
            // ...but Flash objects (which have this classid) *can* handle expandos
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(a) {
            a = a.nodeType ? m.cache[a[m.expando]] : a[m.expando];
            return !!a && !P(a);
        },
        data: function(a, b, c) {
            return Q(a, b, c);
        },
        removeData: function(a, b) {
            return R(a, b);
        },
        // For internal use only.
        _data: function(a, b, c) {
            return Q(a, b, c, true);
        },
        _removeData: function(a, b) {
            return R(a, b, true);
        }
    });
    m.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            // Special expections of .data basically thwart jQuery.access,
            // so implement the relevant behavior ourselves
            // Gets all values
            if (a === undefined) {
                if (this.length) {
                    e = m.data(f);
                    if (f.nodeType === 1 && !m._data(f, "parsedAttrs")) {
                        c = g.length;
                        while (c--) {
                            // Support: IE11+
                            // The attrs elements can be null (#14894)
                            if (g[c]) {
                                d = g[c].name;
                                if (d.indexOf("data-") === 0) {
                                    d = m.camelCase(d.slice(5));
                                    O(f, d, e[d]);
                                }
                            }
                        }
                        m._data(f, "parsedAttrs", true);
                    }
                }
                return e;
            }
            // Sets multiple values
            if (typeof a === "object") {
                return this.each(function() {
                    m.data(this, a);
                });
            }
            // Sets one value
            // Gets one value
            // Try to fetch any internally stored data first
            return arguments.length > 1 ? this.each(function() {
                m.data(this, a, b);
            }) : f ? O(f, a, m.data(f, a)) : undefined;
        },
        removeData: function(a) {
            return this.each(function() {
                m.removeData(this, a);
            });
        }
    });
    m.extend({
        queue: function(a, b, c) {
            var d;
            if (a) {
                b = (b || "fx") + "queue";
                d = m._data(a, b);
                // Speed up dequeue by getting out quickly if this is just a lookup
                if (c) {
                    if (!d || m.isArray(c)) {
                        d = m._data(a, b, m.makeArray(c));
                    } else {
                        d.push(c);
                    }
                }
                return d || [];
            }
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = m.queue(a, b), d = c.length, e = c.shift(), f = m._queueHooks(a, b), g = function() {
                m.dequeue(a, b);
            };
            // If the fx queue is dequeued, always remove the progress sentinel
            if (e === "inprogress") {
                e = c.shift();
                d--;
            }
            if (e) {
                // Add a progress sentinel to prevent the fx queue from being
                // automatically dequeued
                if (b === "fx") {
                    c.unshift("inprogress");
                }
                // clear up the last queue stop function
                delete f.stop;
                e.call(a, g, f);
            }
            if (!d && f) {
                f.empty.fire();
            }
        },
        // not intended for public consumption - generates a queueHooks object, or returns the current one
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return m._data(a, c) || m._data(a, c, {
                empty: m.Callbacks("once memory").add(function() {
                    m._removeData(a, b + "queue");
                    m._removeData(a, c);
                })
            });
        }
    });
    m.fn.extend({
        queue: function(a, b) {
            var c = 2;
            if (typeof a !== "string") {
                b = a;
                a = "fx";
                c--;
            }
            if (arguments.length < c) {
                return m.queue(this[0], a);
            }
            return b === undefined ? this : this.each(function() {
                var c = m.queue(this, a, b);
                // ensure a hooks for this queue
                m._queueHooks(this, a);
                if (a === "fx" && c[0] !== "inprogress") {
                    m.dequeue(this, a);
                }
            });
        },
        dequeue: function(a) {
            return this.each(function() {
                m.dequeue(this, a);
            });
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", []);
        },
        // Get a promise resolved when queues of a certain type
        // are emptied (fx is the type by default)
        promise: function(a, b) {
            var c, d = 1, e = m.Deferred(), f = this, g = this.length, h = function() {
                if (!--d) {
                    e.resolveWith(f, [ f ]);
                }
            };
            if (typeof a !== "string") {
                b = a;
                a = undefined;
            }
            a = a || "fx";
            while (g--) {
                c = m._data(f[g], a + "queueHooks");
                if (c && c.empty) {
                    d++;
                    c.empty.add(h);
                }
            }
            h();
            return e.promise(b);
        }
    });
    var S = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
    var T = [ "Top", "Right", "Bottom", "Left" ];
    var U = function(a, b) {
        // isHidden might be called from jQuery#filter function;
        // in that case, element will be second argument
        a = b || a;
        return m.css(a, "display") === "none" || !m.contains(a.ownerDocument, a);
    };
    // Multifunctional method to get and set values of a collection
    // The value/s can optionally be executed if it's a function
    var V = m.access = function(a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = c == null;
        // Sets many values
        if (m.type(c) === "object") {
            e = true;
            for (h in c) {
                m.access(a, b, h, c[h], true, f, g);
            }
        } else if (d !== undefined) {
            e = true;
            if (!m.isFunction(d)) {
                g = true;
            }
            if (j) {
                // Bulk operations run against the entire set
                if (g) {
                    b.call(a, d);
                    b = null;
                } else {
                    j = b;
                    b = function(a, b, c) {
                        return j.call(m(a), c);
                    };
                }
            }
            if (b) {
                for (;h < i; h++) {
                    b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
                }
            }
        }
        // Gets
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    };
    var W = /^(?:checkbox|radio)$/i;
    (function() {
        // Minified: var a,b,c
        var a = y.createElement("input"), b = y.createElement("div"), c = y.createDocumentFragment();
        // Setup
        b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
        // IE strips leading whitespace when .innerHTML is used
        k.leadingWhitespace = b.firstChild.nodeType === 3;
        // Make sure that tbody elements aren't automatically inserted
        // IE will insert them into empty tables
        k.tbody = !b.getElementsByTagName("tbody").length;
        // Make sure that link elements get serialized correctly by innerHTML
        // This requires a wrapper element in IE
        k.htmlSerialize = !!b.getElementsByTagName("link").length;
        // Makes sure cloning an html5 element does not cause problems
        // Where outerHTML is undefined, this still works
        k.html5Clone = y.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>";
        // Check if a disconnected checkbox will retain its checked
        // value of true after appended to the DOM (IE6/7)
        a.type = "checkbox";
        a.checked = true;
        c.appendChild(a);
        k.appendChecked = a.checked;
        // Make sure textarea (and checkbox) defaultValue is properly cloned
        // Support: IE6-IE11+
        b.innerHTML = "<textarea>x</textarea>";
        k.noCloneChecked = !!b.cloneNode(true).lastChild.defaultValue;
        // #11217 - WebKit loses check when the name is after the checked attribute
        c.appendChild(b);
        b.innerHTML = "<input type='radio' checked='checked' name='t'/>";
        // Support: Safari 5.1, iOS 5.1, Android 4.x, Android 2.3
        // old WebKit doesn't clone checked state correctly in fragments
        k.checkClone = b.cloneNode(true).cloneNode(true).lastChild.checked;
        // Support: IE<9
        // Opera does not clone events (and typeof div.attachEvent === undefined).
        // IE9-10 clones events bound via attachEvent, but they don't trigger with .click()
        k.noCloneEvent = true;
        if (b.attachEvent) {
            b.attachEvent("onclick", function() {
                k.noCloneEvent = false;
            });
            b.cloneNode(true).click();
        }
        // Execute the test only if not already executed in another module.
        if (k.deleteExpando == null) {
            // Support: IE<9
            k.deleteExpando = true;
            try {
                delete b.test;
            } catch (d) {
                k.deleteExpando = false;
            }
        }
    })();
    (function() {
        var b, c, d = y.createElement("div");
        // Support: IE<9 (lack submit/change bubble), Firefox 23+ (lack focusin event)
        for (b in {
            submit: true,
            change: true,
            focusin: true
        }) {
            c = "on" + b;
            if (!(k[b + "Bubbles"] = c in a)) {
                // Beware of CSP restrictions (https://developer.mozilla.org/en/Security/CSP)
                d.setAttribute(c, "t");
                k[b + "Bubbles"] = d.attributes[c].expando === false;
            }
        }
        // Null elements to avoid leaks in IE.
        d = null;
    })();
    var X = /^(?:input|select|textarea)$/i, Y = /^key/, Z = /^(?:mouse|pointer|contextmenu)|click/, $ = /^(?:focusinfocus|focusoutblur)$/, _ = /^([^.]*)(?:\.(.+)|)$/;
    function ab() {
        return true;
    }
    function bb() {
        return false;
    }
    function cb() {
        try {
            return y.activeElement;
        } catch (a) {}
    }
    /*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
    m.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, n, o, p, q, r = m._data(a);
            // Don't attach events to noData or text/comment nodes (but allow plain objects)
            if (!r) {
                return;
            }
            // Caller can pass in an object of custom data in lieu of the handler
            if (c.handler) {
                i = c;
                c = i.handler;
                e = i.selector;
            }
            // Make sure that the handler has a unique ID, used to find/remove it later
            if (!c.guid) {
                c.guid = m.guid++;
            }
            // Init the element's event structure and main handler, if this is the first
            if (!(g = r.events)) {
                g = r.events = {};
            }
            if (!(k = r.handle)) {
                k = r.handle = function(a) {
                    // Discard the second event of a jQuery.event.trigger() and
                    // when an event is called after a page has unloaded
                    return typeof m !== K && (!a || m.event.triggered !== a.type) ? m.event.dispatch.apply(k.elem, arguments) : undefined;
                };
                // Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
                k.elem = a;
            }
            // Handle multiple events separated by a space
            b = (b || "").match(E) || [ "" ];
            h = b.length;
            while (h--) {
                f = _.exec(b[h]) || [];
                o = q = f[1];
                p = (f[2] || "").split(".").sort();
                // There *must* be a type, no attaching namespace-only handlers
                if (!o) {
                    continue;
                }
                // If event changes its type, use the special event handlers for the changed type
                j = m.event.special[o] || {};
                // If selector defined, determine special event api type, otherwise given type
                o = (e ? j.delegateType : j.bindType) || o;
                // Update special based on newly reset type
                j = m.event.special[o] || {};
                // handleObj is passed to all event handlers
                l = m.extend({
                    type: o,
                    origType: q,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && m.expr.match.needsContext.test(e),
                    namespace: p.join(".")
                }, i);
                // Init the event handler queue if we're the first
                if (!(n = g[o])) {
                    n = g[o] = [];
                    n.delegateCount = 0;
                    // Only use addEventListener/attachEvent if the special events handler returns false
                    if (!j.setup || j.setup.call(a, d, p, k) === false) {
                        // Bind the global event handler to the element
                        if (a.addEventListener) {
                            a.addEventListener(o, k, false);
                        } else if (a.attachEvent) {
                            a.attachEvent("on" + o, k);
                        }
                    }
                }
                if (j.add) {
                    j.add.call(a, l);
                    if (!l.handler.guid) {
                        l.handler.guid = c.guid;
                    }
                }
                // Add to the element's handler list, delegates in front
                if (e) {
                    n.splice(n.delegateCount++, 0, l);
                } else {
                    n.push(l);
                }
                // Keep track of which events have ever been used, for event optimization
                m.event.global[o] = true;
            }
            // Nullify elem to prevent memory leaks in IE
            a = null;
        },
        // Detach an event or set of events from an element
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, n, o, p, q, r = m.hasData(a) && m._data(a);
            if (!r || !(k = r.events)) {
                return;
            }
            // Once for each type.namespace in types; type may be omitted
            b = (b || "").match(E) || [ "" ];
            j = b.length;
            while (j--) {
                h = _.exec(b[j]) || [];
                o = q = h[1];
                p = (h[2] || "").split(".").sort();
                // Unbind all events (on this namespace, if provided) for the element
                if (!o) {
                    for (o in k) {
                        m.event.remove(a, o + b[j], c, d, true);
                    }
                    continue;
                }
                l = m.event.special[o] || {};
                o = (d ? l.delegateType : l.bindType) || o;
                n = k[o] || [];
                h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)");
                // Remove matching events
                i = f = n.length;
                while (f--) {
                    g = n[f];
                    if ((e || q === g.origType) && (!c || c.guid === g.guid) && (!h || h.test(g.namespace)) && (!d || d === g.selector || d === "**" && g.selector)) {
                        n.splice(f, 1);
                        if (g.selector) {
                            n.delegateCount--;
                        }
                        if (l.remove) {
                            l.remove.call(a, g);
                        }
                    }
                }
                // Remove generic event handler if we removed something and no more handlers exist
                // (avoids potential for endless recursion during removal of special event handlers)
                if (i && !n.length) {
                    if (!l.teardown || l.teardown.call(a, p, r.handle) === false) {
                        m.removeEvent(a, o, r.handle);
                    }
                    delete k[o];
                }
            }
            // Remove the expando if it's no longer used
            if (m.isEmptyObject(k)) {
                delete r.handle;
                // removeData also checks for emptiness and clears the expando if empty
                // so use it instead of delete
                m._removeData(a, "events");
            }
        },
        trigger: function(b, c, d, e) {
            var f, g, h, i, k, l, n, o = [ d || y ], p = j.call(b, "type") ? b.type : b, q = j.call(b, "namespace") ? b.namespace.split(".") : [];
            h = l = d = d || y;
            // Don't do events on text and comment nodes
            if (d.nodeType === 3 || d.nodeType === 8) {
                return;
            }
            // focus/blur morphs to focusin/out; ensure we're not firing them right now
            if ($.test(p + m.event.triggered)) {
                return;
            }
            if (p.indexOf(".") >= 0) {
                // Namespaced trigger; create a regexp to match event type in handle()
                q = p.split(".");
                p = q.shift();
                q.sort();
            }
            g = p.indexOf(":") < 0 && "on" + p;
            // Caller can pass in a jQuery.Event object, Object, or just an event type string
            b = b[m.expando] ? b : new m.Event(p, typeof b === "object" && b);
            // Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
            b.isTrigger = e ? 2 : 3;
            b.namespace = q.join(".");
            b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
            // Clean up the event in case it is being reused
            b.result = undefined;
            if (!b.target) {
                b.target = d;
            }
            // Clone any incoming data and prepend the event, creating the handler arg list
            c = c == null ? [ b ] : m.makeArray(c, [ b ]);
            // Allow special events to draw outside the lines
            k = m.event.special[p] || {};
            if (!e && k.trigger && k.trigger.apply(d, c) === false) {
                return;
            }
            // Determine event propagation path in advance, per W3C events spec (#9951)
            // Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
            if (!e && !k.noBubble && !m.isWindow(d)) {
                i = k.delegateType || p;
                if (!$.test(i + p)) {
                    h = h.parentNode;
                }
                for (;h; h = h.parentNode) {
                    o.push(h);
                    l = h;
                }
                // Only add window if we got to document (e.g., not plain obj or detached DOM)
                if (l === (d.ownerDocument || y)) {
                    o.push(l.defaultView || l.parentWindow || a);
                }
            }
            // Fire handlers on the event path
            n = 0;
            while ((h = o[n++]) && !b.isPropagationStopped()) {
                b.type = n > 1 ? i : k.bindType || p;
                // jQuery handler
                f = (m._data(h, "events") || {})[b.type] && m._data(h, "handle");
                if (f) {
                    f.apply(h, c);
                }
                // Native handler
                f = g && h[g];
                if (f && f.apply && m.acceptData(h)) {
                    b.result = f.apply(h, c);
                    if (b.result === false) {
                        b.preventDefault();
                    }
                }
            }
            b.type = p;
            // If nobody prevented the default action, do it now
            if (!e && !b.isDefaultPrevented()) {
                if ((!k._default || k._default.apply(o.pop(), c) === false) && m.acceptData(d)) {
                    // Call a native DOM method on the target with the same name name as the event.
                    // Can't use an .isFunction() check here because IE6/7 fails that test.
                    // Don't do default actions on window, that's where global variables be (#6170)
                    if (g && d[p] && !m.isWindow(d)) {
                        // Don't re-trigger an onFOO event when we call its FOO() method
                        l = d[g];
                        if (l) {
                            d[g] = null;
                        }
                        // Prevent re-triggering of the same event, since we already bubbled it above
                        m.event.triggered = p;
                        try {
                            d[p]();
                        } catch (r) {}
                        m.event.triggered = undefined;
                        if (l) {
                            d[g] = l;
                        }
                    }
                }
            }
            return b.result;
        },
        dispatch: function(a) {
            // Make a writable jQuery.Event from the native event object
            a = m.event.fix(a);
            var b, c, e, f, g, h = [], i = d.call(arguments), j = (m._data(this, "events") || {})[a.type] || [], k = m.event.special[a.type] || {};
            // Use the fix-ed jQuery.Event rather than the (read-only) native event
            i[0] = a;
            a.delegateTarget = this;
            // Call the preDispatch hook for the mapped type, and let it bail if desired
            if (k.preDispatch && k.preDispatch.call(this, a) === false) {
                return;
            }
            // Determine handlers
            h = m.event.handlers.call(this, a, j);
            // Run delegates first; they may want to stop propagation beneath us
            b = 0;
            while ((f = h[b++]) && !a.isPropagationStopped()) {
                a.currentTarget = f.elem;
                g = 0;
                while ((e = f.handlers[g++]) && !a.isImmediatePropagationStopped()) {
                    // Triggered event must either 1) have no namespace, or
                    // 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
                    if (!a.namespace_re || a.namespace_re.test(e.namespace)) {
                        a.handleObj = e;
                        a.data = e.data;
                        c = ((m.event.special[e.origType] || {}).handle || e.handler).apply(f.elem, i);
                        if (c !== undefined) {
                            if ((a.result = c) === false) {
                                a.preventDefault();
                                a.stopPropagation();
                            }
                        }
                    }
                }
            }
            // Call the postDispatch hook for the mapped type
            if (k.postDispatch) {
                k.postDispatch.call(this, a);
            }
            return a.result;
        },
        handlers: function(a, b) {
            var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
            // Find delegate handlers
            // Black-hole SVG <use> instance trees (#13180)
            // Avoid non-left-click bubbling in Firefox (#3861)
            if (h && i.nodeType && (!a.button || a.type !== "click")) {
                /* jshint eqeqeq: false */
                for (;i != this; i = i.parentNode || this) {
                    /* jshint eqeqeq: true */
                    // Don't check non-elements (#13208)
                    // Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
                    if (i.nodeType === 1 && (i.disabled !== true || a.type !== "click")) {
                        e = [];
                        for (f = 0; f < h; f++) {
                            d = b[f];
                            // Don't conflict with Object.prototype properties (#13203)
                            c = d.selector + " ";
                            if (e[c] === undefined) {
                                e[c] = d.needsContext ? m(c, this).index(i) >= 0 : m.find(c, this, null, [ i ]).length;
                            }
                            if (e[c]) {
                                e.push(d);
                            }
                        }
                        if (e.length) {
                            g.push({
                                elem: i,
                                handlers: e
                            });
                        }
                    }
                }
            }
            // Add the remaining (directly-bound) handlers
            if (h < b.length) {
                g.push({
                    elem: this,
                    handlers: b.slice(h)
                });
            }
            return g;
        },
        fix: function(a) {
            if (a[m.expando]) {
                return a;
            }
            // Create a writable copy of the event object and normalize some properties
            var b, c, d, e = a.type, f = a, g = this.fixHooks[e];
            if (!g) {
                this.fixHooks[e] = g = Z.test(e) ? this.mouseHooks : Y.test(e) ? this.keyHooks : {};
            }
            d = g.props ? this.props.concat(g.props) : this.props;
            a = new m.Event(f);
            b = d.length;
            while (b--) {
                c = d[b];
                a[c] = f[c];
            }
            // Support: IE<9
            // Fix target property (#1925)
            if (!a.target) {
                a.target = f.srcElement || y;
            }
            // Support: Chrome 23+, Safari?
            // Target should not be a text node (#504, #13143)
            if (a.target.nodeType === 3) {
                a.target = a.target.parentNode;
            }
            // Support: IE<9
            // For mouse/key events, metaKey==false if it's undefined (#3368, #11328)
            a.metaKey = !!a.metaKey;
            return g.filter ? g.filter(a, f) : a;
        },
        // Includes some event props shared by KeyEvent and MouseEvent
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(a, b) {
                // Add which for key events
                if (a.which == null) {
                    a.which = b.charCode != null ? b.charCode : b.keyCode;
                }
                return a;
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(a, b) {
                var c, d, e, f = b.button, g = b.fromElement;
                // Calculate pageX/Y if missing and clientX/Y available
                if (a.pageX == null && b.clientX != null) {
                    d = a.target.ownerDocument || y;
                    e = d.documentElement;
                    c = d.body;
                    a.pageX = b.clientX + (e && e.scrollLeft || c && c.scrollLeft || 0) - (e && e.clientLeft || c && c.clientLeft || 0);
                    a.pageY = b.clientY + (e && e.scrollTop || c && c.scrollTop || 0) - (e && e.clientTop || c && c.clientTop || 0);
                }
                // Add relatedTarget, if necessary
                if (!a.relatedTarget && g) {
                    a.relatedTarget = g === a.target ? b.toElement : g;
                }
                // Add which for click: 1 === left; 2 === middle; 3 === right
                // Note: button is not normalized, so don't use it
                if (!a.which && f !== undefined) {
                    a.which = f & 1 ? 1 : f & 2 ? 3 : f & 4 ? 2 : 0;
                }
                return a;
            }
        },
        special: {
            load: {
                // Prevent triggered image.load events from bubbling to window.load
                noBubble: true
            },
            focus: {
                // Fire native event if possible so blur/focus sequence is correct
                trigger: function() {
                    if (this !== cb() && this.focus) {
                        try {
                            this.focus();
                            return false;
                        } catch (a) {}
                    }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === cb() && this.blur) {
                        this.blur();
                        return false;
                    }
                },
                delegateType: "focusout"
            },
            click: {
                // For checkbox, fire native event so checked state will be right
                trigger: function() {
                    if (m.nodeName(this, "input") && this.type === "checkbox" && this.click) {
                        this.click();
                        return false;
                    }
                },
                // For cross-browser consistency, don't fire native .click() on links
                _default: function(a) {
                    return m.nodeName(a.target, "a");
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    // Support: Firefox 20+
                    // Firefox doesn't alert if the returnValue field is not set.
                    if (a.result !== undefined && a.originalEvent) {
                        a.originalEvent.returnValue = a.result;
                    }
                }
            }
        },
        simulate: function(a, b, c, d) {
            // Piggyback on a donor event to simulate a different one.
            // Fake originalEvent to avoid donor's stopPropagation, but if the
            // simulated event prevents default then we do the same on the donor.
            var e = m.extend(new m.Event(), c, {
                type: a,
                isSimulated: true,
                originalEvent: {}
            });
            if (d) {
                m.event.trigger(e, null, b);
            } else {
                m.event.dispatch.call(b, e);
            }
            if (e.isDefaultPrevented()) {
                c.preventDefault();
            }
        }
    };
    m.removeEvent = y.removeEventListener ? function(a, b, c) {
        if (a.removeEventListener) {
            a.removeEventListener(b, c, false);
        }
    } : function(a, b, c) {
        var d = "on" + b;
        if (a.detachEvent) {
            // #8545, #7054, preventing memory leaks for custom events in IE6-8
            // detachEvent needed property on element, by name of that event, to properly expose it to GC
            if (typeof a[d] === K) {
                a[d] = null;
            }
            a.detachEvent(d, c);
        }
    };
    m.Event = function(a, b) {
        // Allow instantiation without the 'new' keyword
        if (!(this instanceof m.Event)) {
            return new m.Event(a, b);
        }
        // Event object
        if (a && a.type) {
            this.originalEvent = a;
            this.type = a.type;
            // Events bubbling up the document may have been marked as prevented
            // by a handler lower down the tree; reflect the correct value.
            this.isDefaultPrevented = a.defaultPrevented || a.defaultPrevented === undefined && // Support: IE < 9, Android < 4.0
            a.returnValue === false ? ab : bb;
        } else {
            this.type = a;
        }
        // Put explicitly provided properties onto the event object
        if (b) {
            m.extend(this, b);
        }
        // Create a timestamp if incoming event doesn't have one
        this.timeStamp = a && a.timeStamp || m.now();
        // Mark it as fixed
        this[m.expando] = true;
    };
    // jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
    // http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
    m.Event.prototype = {
        isDefaultPrevented: bb,
        isPropagationStopped: bb,
        isImmediatePropagationStopped: bb,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = ab;
            if (!a) {
                return;
            }
            // If preventDefault exists, run it on the original event
            if (a.preventDefault) {
                a.preventDefault();
            } else {
                a.returnValue = false;
            }
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = ab;
            if (!a) {
                return;
            }
            // If stopPropagation exists, run it on the original event
            if (a.stopPropagation) {
                a.stopPropagation();
            }
            // Support: IE
            // Set the cancelBubble property of the original event to true
            a.cancelBubble = true;
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = ab;
            if (a && a.stopImmediatePropagation) {
                a.stopImmediatePropagation();
            }
            this.stopPropagation();
        }
    };
    // Create mouseenter/leave events using mouseover/out and event-time checks
    m.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        m.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this, e = a.relatedTarget, f = a.handleObj;
                // For mousenter/leave call the handler if related is outside the target.
                // NB: No relatedTarget if the mouse left/entered the browser window
                if (!e || e !== d && !m.contains(d, e)) {
                    a.type = f.origType;
                    c = f.handler.apply(this, arguments);
                    a.type = b;
                }
                return c;
            }
        };
    });
    // IE submit delegation
    if (!k.submitBubbles) {
        m.event.special.submit = {
            setup: function() {
                // Only need this for delegated form submit events
                if (m.nodeName(this, "form")) {
                    return false;
                }
                // Lazy-add a submit handler when a descendant form may potentially be submitted
                m.event.add(this, "click._submit keypress._submit", function(a) {
                    // Node name check avoids a VML-related crash in IE (#9807)
                    var b = a.target, c = m.nodeName(b, "input") || m.nodeName(b, "button") ? b.form : undefined;
                    if (c && !m._data(c, "submitBubbles")) {
                        m.event.add(c, "submit._submit", function(a) {
                            a._submit_bubble = true;
                        });
                        m._data(c, "submitBubbles", true);
                    }
                });
            },
            postDispatch: function(a) {
                // If form was submitted by the user, bubble the event up the tree
                if (a._submit_bubble) {
                    delete a._submit_bubble;
                    if (this.parentNode && !a.isTrigger) {
                        m.event.simulate("submit", this.parentNode, a, true);
                    }
                }
            },
            teardown: function() {
                // Only need this for delegated form submit events
                if (m.nodeName(this, "form")) {
                    return false;
                }
                // Remove delegated handlers; cleanData eventually reaps submit handlers attached above
                m.event.remove(this, "._submit");
            }
        };
    }
    // IE change delegation and checkbox/radio fix
    if (!k.changeBubbles) {
        m.event.special.change = {
            setup: function() {
                if (X.test(this.nodeName)) {
                    // IE doesn't fire change on a check/radio until blur; trigger it on click
                    // after a propertychange. Eat the blur-change in special.change.handle.
                    // This still fires onchange a second time for check/radio after blur.
                    if (this.type === "checkbox" || this.type === "radio") {
                        m.event.add(this, "propertychange._change", function(a) {
                            if (a.originalEvent.propertyName === "checked") {
                                this._just_changed = true;
                            }
                        });
                        m.event.add(this, "click._change", function(a) {
                            if (this._just_changed && !a.isTrigger) {
                                this._just_changed = false;
                            }
                            // Allow triggered, simulated change events (#11500)
                            m.event.simulate("change", this, a, true);
                        });
                    }
                    return false;
                }
                // Delegated event; lazy-add a change handler on descendant inputs
                m.event.add(this, "beforeactivate._change", function(a) {
                    var b = a.target;
                    if (X.test(b.nodeName) && !m._data(b, "changeBubbles")) {
                        m.event.add(b, "change._change", function(a) {
                            if (this.parentNode && !a.isSimulated && !a.isTrigger) {
                                m.event.simulate("change", this.parentNode, a, true);
                            }
                        });
                        m._data(b, "changeBubbles", true);
                    }
                });
            },
            handle: function(a) {
                var b = a.target;
                // Swallow native change events from checkbox/radio, we already triggered them above
                if (this !== b || a.isSimulated || a.isTrigger || b.type !== "radio" && b.type !== "checkbox") {
                    return a.handleObj.handler.apply(this, arguments);
                }
            },
            teardown: function() {
                m.event.remove(this, "._change");
                return !X.test(this.nodeName);
            }
        };
    }
    // Create "bubbling" focus and blur events
    if (!k.focusinBubbles) {
        m.each({
            focus: "focusin",
            blur: "focusout"
        }, function(a, b) {
            // Attach a single capturing handler on the document while someone wants focusin/focusout
            var c = function(a) {
                m.event.simulate(b, a.target, m.event.fix(a), true);
            };
            m.event.special[b] = {
                setup: function() {
                    var d = this.ownerDocument || this, e = m._data(d, b);
                    if (!e) {
                        d.addEventListener(a, c, true);
                    }
                    m._data(d, b, (e || 0) + 1);
                },
                teardown: function() {
                    var d = this.ownerDocument || this, e = m._data(d, b) - 1;
                    if (!e) {
                        d.removeEventListener(a, c, true);
                        m._removeData(d, b);
                    } else {
                        m._data(d, b, e);
                    }
                }
            };
        });
    }
    m.fn.extend({
        on: function(a, b, c, d, /*INTERNAL*/ e) {
            var f, g;
            // Types can be a map of types/handlers
            if (typeof a === "object") {
                // ( types-Object, selector, data )
                if (typeof b !== "string") {
                    // ( types-Object, data )
                    c = c || b;
                    b = undefined;
                }
                for (f in a) {
                    this.on(f, b, c, a[f], e);
                }
                return this;
            }
            if (c == null && d == null) {
                // ( types, fn )
                d = b;
                c = b = undefined;
            } else if (d == null) {
                if (typeof b === "string") {
                    // ( types, selector, fn )
                    d = c;
                    c = undefined;
                } else {
                    // ( types, data, fn )
                    d = c;
                    c = b;
                    b = undefined;
                }
            }
            if (d === false) {
                d = bb;
            } else if (!d) {
                return this;
            }
            if (e === 1) {
                g = d;
                d = function(a) {
                    // Can use an empty set, since event contains the info
                    m().off(a);
                    return g.apply(this, arguments);
                };
                // Use same guid so caller can remove using origFn
                d.guid = g.guid || (g.guid = m.guid++);
            }
            return this.each(function() {
                m.event.add(this, a, d, c, b);
            });
        },
        one: function(a, b, c, d) {
            return this.on(a, b, c, d, 1);
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) {
                // ( event )  dispatched jQuery.Event
                d = a.handleObj;
                m(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler);
                return this;
            }
            if (typeof a === "object") {
                // ( types-object [, selector] )
                for (e in a) {
                    this.off(e, b, a[e]);
                }
                return this;
            }
            if (b === false || typeof b === "function") {
                // ( types [, fn] )
                c = b;
                b = undefined;
            }
            if (c === false) {
                c = bb;
            }
            return this.each(function() {
                m.event.remove(this, a, c, b);
            });
        },
        trigger: function(a, b) {
            return this.each(function() {
                m.event.trigger(a, b, this);
            });
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) {
                return m.event.trigger(a, b, c, true);
            }
        }
    });
    function db(a) {
        var b = eb.split("|"), c = a.createDocumentFragment();
        if (c.createElement) {
            while (b.length) {
                c.createElement(b.pop());
            }
        }
        return c;
    }
    var eb = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" + "header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", fb = / jQuery\d+="(?:null|\d+)"/g, gb = new RegExp("<(?:" + eb + ")[\\s/>]", "i"), hb = /^\s+/, ib = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, jb = /<([\w:]+)/, kb = /<tbody/i, lb = /<|&#?\w+;/, mb = /<(?:script|style|link)/i, // checked="checked" or checked
    nb = /checked\s*(?:[^=]|=\s*.checked.)/i, ob = /^$|\/(?:java|ecma)script/i, pb = /^true\/(.*)/, qb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, // We have to close these tags to support XHTML (#13200)
    rb = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        legend: [ 1, "<fieldset>", "</fieldset>" ],
        area: [ 1, "<map>", "</map>" ],
        param: [ 1, "<object>", "</object>" ],
        thead: [ 1, "<table>", "</table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        // IE6-8 can't serialize link, script, style, or any html5 (NoScope) tags,
        // unless wrapped in a div with non-breaking characters in front of it.
        _default: k.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>" ]
    }, sb = db(y), tb = sb.appendChild(y.createElement("div"));
    rb.optgroup = rb.option;
    rb.tbody = rb.tfoot = rb.colgroup = rb.caption = rb.thead;
    rb.th = rb.td;
    function ub(a, b) {
        var c, d, e = 0, f = typeof a.getElementsByTagName !== K ? a.getElementsByTagName(b || "*") : typeof a.querySelectorAll !== K ? a.querySelectorAll(b || "*") : undefined;
        if (!f) {
            for (f = [], c = a.childNodes || a; (d = c[e]) != null; e++) {
                if (!b || m.nodeName(d, b)) {
                    f.push(d);
                } else {
                    m.merge(f, ub(d, b));
                }
            }
        }
        return b === undefined || b && m.nodeName(a, b) ? m.merge([ a ], f) : f;
    }
    // Used in buildFragment, fixes the defaultChecked property
    function vb(a) {
        if (W.test(a.type)) {
            a.defaultChecked = a.checked;
        }
    }
    // Support: IE<8
    // Manipulating tables requires a tbody
    function wb(a, b) {
        return m.nodeName(a, "table") && m.nodeName(b.nodeType !== 11 ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a;
    }
    // Replace/restore the type attribute of script elements for safe DOM manipulation
    function xb(a) {
        a.type = (m.find.attr(a, "type") !== null) + "/" + a.type;
        return a;
    }
    function yb(a) {
        var b = pb.exec(a.type);
        if (b) {
            a.type = b[1];
        } else {
            a.removeAttribute("type");
        }
        return a;
    }
    // Mark scripts as having already been evaluated
    function zb(a, b) {
        var c, d = 0;
        for (;(c = a[d]) != null; d++) {
            m._data(c, "globalEval", !b || m._data(b[d], "globalEval"));
        }
    }
    function Ab(a, b) {
        if (b.nodeType !== 1 || !m.hasData(a)) {
            return;
        }
        var c, d, e, f = m._data(a), g = m._data(b, f), h = f.events;
        if (h) {
            delete g.handle;
            g.events = {};
            for (c in h) {
                for (d = 0, e = h[c].length; d < e; d++) {
                    m.event.add(b, c, h[c][d]);
                }
            }
        }
        // make the cloned public data object a copy from the original
        if (g.data) {
            g.data = m.extend({}, g.data);
        }
    }
    function Bb(a, b) {
        var c, d, e;
        // We do not need to do anything for non-Elements
        if (b.nodeType !== 1) {
            return;
        }
        c = b.nodeName.toLowerCase();
        // IE6-8 copies events bound via attachEvent when using cloneNode.
        if (!k.noCloneEvent && b[m.expando]) {
            e = m._data(b);
            for (d in e.events) {
                m.removeEvent(b, d, e.handle);
            }
            // Event data gets referenced instead of copied if the expando gets copied too
            b.removeAttribute(m.expando);
        }
        // IE blanks contents when cloning scripts, and tries to evaluate newly-set text
        if (c === "script" && b.text !== a.text) {
            xb(b).text = a.text;
            yb(b);
        } else if (c === "object") {
            if (b.parentNode) {
                b.outerHTML = a.outerHTML;
            }
            // This path appears unavoidable for IE9. When cloning an object
            // element in IE9, the outerHTML strategy above is not sufficient.
            // If the src has innerHTML and the destination does not,
            // copy the src.innerHTML into the dest.innerHTML. #10324
            if (k.html5Clone && (a.innerHTML && !m.trim(b.innerHTML))) {
                b.innerHTML = a.innerHTML;
            }
        } else if (c === "input" && W.test(a.type)) {
            // IE6-8 fails to persist the checked state of a cloned checkbox
            // or radio button. Worse, IE6-7 fail to give the cloned element
            // a checked appearance if the defaultChecked value isn't also set
            b.defaultChecked = b.checked = a.checked;
            // IE6-7 get confused and end up setting the value of a cloned
            // checkbox/radio button to an empty string instead of "on"
            if (b.value !== a.value) {
                b.value = a.value;
            }
        } else if (c === "option") {
            b.defaultSelected = b.selected = a.defaultSelected;
        } else if (c === "input" || c === "textarea") {
            b.defaultValue = a.defaultValue;
        }
    }
    m.extend({
        clone: function(a, b, c) {
            var d, e, f, g, h, i = m.contains(a.ownerDocument, a);
            if (k.html5Clone || m.isXMLDoc(a) || !gb.test("<" + a.nodeName + ">")) {
                f = a.cloneNode(true);
            } else {
                tb.innerHTML = a.outerHTML;
                tb.removeChild(f = tb.firstChild);
            }
            if ((!k.noCloneEvent || !k.noCloneChecked) && (a.nodeType === 1 || a.nodeType === 11) && !m.isXMLDoc(a)) {
                // We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
                d = ub(f);
                h = ub(a);
                // Fix all IE cloning issues
                for (g = 0; (e = h[g]) != null; ++g) {
                    // Ensure that the destination node is not null; Fixes #9587
                    if (d[g]) {
                        Bb(e, d[g]);
                    }
                }
            }
            // Copy the events from the original to the clone
            if (b) {
                if (c) {
                    h = h || ub(a);
                    d = d || ub(f);
                    for (g = 0; (e = h[g]) != null; g++) {
                        Ab(e, d[g]);
                    }
                } else {
                    Ab(a, f);
                }
            }
            // Preserve script evaluation history
            d = ub(f, "script");
            if (d.length > 0) {
                zb(d, !i && ub(a, "script"));
            }
            d = h = e = null;
            // Return the cloned set
            return f;
        },
        buildFragment: function(a, b, c, d) {
            var e, f, g, h, i, j, l, n = a.length, // Ensure a safe fragment
            o = db(b), p = [], q = 0;
            for (;q < n; q++) {
                f = a[q];
                if (f || f === 0) {
                    // Add nodes directly
                    if (m.type(f) === "object") {
                        m.merge(p, f.nodeType ? [ f ] : f);
                    } else if (!lb.test(f)) {
                        p.push(b.createTextNode(f));
                    } else {
                        h = h || o.appendChild(b.createElement("div"));
                        // Deserialize a standard representation
                        i = (jb.exec(f) || [ "", "" ])[1].toLowerCase();
                        l = rb[i] || rb._default;
                        h.innerHTML = l[1] + f.replace(ib, "<$1></$2>") + l[2];
                        // Descend through wrappers to the right content
                        e = l[0];
                        while (e--) {
                            h = h.lastChild;
                        }
                        // Manually add leading whitespace removed by IE
                        if (!k.leadingWhitespace && hb.test(f)) {
                            p.push(b.createTextNode(hb.exec(f)[0]));
                        }
                        // Remove IE's autoinserted <tbody> from table fragments
                        if (!k.tbody) {
                            // String was a <table>, *may* have spurious <tbody>
                            f = i === "table" && !kb.test(f) ? h.firstChild : // String was a bare <thead> or <tfoot>
                            l[1] === "<table>" && !kb.test(f) ? h : 0;
                            e = f && f.childNodes.length;
                            while (e--) {
                                if (m.nodeName(j = f.childNodes[e], "tbody") && !j.childNodes.length) {
                                    f.removeChild(j);
                                }
                            }
                        }
                        m.merge(p, h.childNodes);
                        // Fix #12392 for WebKit and IE > 9
                        h.textContent = "";
                        // Fix #12392 for oldIE
                        while (h.firstChild) {
                            h.removeChild(h.firstChild);
                        }
                        // Remember the top-level container for proper cleanup
                        h = o.lastChild;
                    }
                }
            }
            // Fix #11356: Clear elements from fragment
            if (h) {
                o.removeChild(h);
            }
            // Reset defaultChecked for any radios and checkboxes
            // about to be appended to the DOM in IE 6/7 (#8060)
            if (!k.appendChecked) {
                m.grep(ub(p, "input"), vb);
            }
            q = 0;
            while (f = p[q++]) {
                // #4087 - If origin and destination elements are the same, and this is
                // that element, do not do anything
                if (d && m.inArray(f, d) !== -1) {
                    continue;
                }
                g = m.contains(f.ownerDocument, f);
                // Append to fragment
                h = ub(o.appendChild(f), "script");
                // Preserve script evaluation history
                if (g) {
                    zb(h);
                }
                // Capture executables
                if (c) {
                    e = 0;
                    while (f = h[e++]) {
                        if (ob.test(f.type || "")) {
                            c.push(f);
                        }
                    }
                }
            }
            h = null;
            return o;
        },
        cleanData: function(a, /* internal */ b) {
            var d, e, f, g, h = 0, i = m.expando, j = m.cache, l = k.deleteExpando, n = m.event.special;
            for (;(d = a[h]) != null; h++) {
                if (b || m.acceptData(d)) {
                    f = d[i];
                    g = f && j[f];
                    if (g) {
                        if (g.events) {
                            for (e in g.events) {
                                if (n[e]) {
                                    m.event.remove(d, e);
                                } else {
                                    m.removeEvent(d, e, g.handle);
                                }
                            }
                        }
                        // Remove cache only if it was not already removed by jQuery.event.remove
                        if (j[f]) {
                            delete j[f];
                            // IE does not allow us to delete expando properties from nodes,
                            // nor does it have a removeAttribute function on Document nodes;
                            // we must handle all of these cases
                            if (l) {
                                delete d[i];
                            } else if (typeof d.removeAttribute !== K) {
                                d.removeAttribute(i);
                            } else {
                                d[i] = null;
                            }
                            c.push(f);
                        }
                    }
                }
            }
        }
    });
    m.fn.extend({
        text: function(a) {
            return V(this, function(a) {
                return a === undefined ? m.text(this) : this.empty().append((this[0] && this[0].ownerDocument || y).createTextNode(a));
            }, null, a, arguments.length);
        },
        append: function() {
            return this.domManip(arguments, function(a) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var b = wb(this, a);
                    b.appendChild(a);
                }
            });
        },
        prepend: function() {
            return this.domManip(arguments, function(a) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var b = wb(this, a);
                    b.insertBefore(a, b.firstChild);
                }
            });
        },
        before: function() {
            return this.domManip(arguments, function(a) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(a, this);
                }
            });
        },
        after: function() {
            return this.domManip(arguments, function(a) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(a, this.nextSibling);
                }
            });
        },
        remove: function(a, b) {
            var c, d = a ? m.filter(a, this) : this, e = 0;
            for (;(c = d[e]) != null; e++) {
                if (!b && c.nodeType === 1) {
                    m.cleanData(ub(c));
                }
                if (c.parentNode) {
                    if (b && m.contains(c.ownerDocument, c)) {
                        zb(ub(c, "script"));
                    }
                    c.parentNode.removeChild(c);
                }
            }
            return this;
        },
        empty: function() {
            var a, b = 0;
            for (;(a = this[b]) != null; b++) {
                // Remove element nodes and prevent memory leaks
                if (a.nodeType === 1) {
                    m.cleanData(ub(a, false));
                }
                // Remove any remaining nodes
                while (a.firstChild) {
                    a.removeChild(a.firstChild);
                }
                // If this is a select, ensure that it displays empty (#12336)
                // Support: IE<9
                if (a.options && m.nodeName(a, "select")) {
                    a.options.length = 0;
                }
            }
            return this;
        },
        clone: function(a, b) {
            a = a == null ? false : a;
            b = b == null ? a : b;
            return this.map(function() {
                return m.clone(this, a, b);
            });
        },
        html: function(a) {
            return V(this, function(a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (a === undefined) {
                    return b.nodeType === 1 ? b.innerHTML.replace(fb, "") : undefined;
                }
                // See if we can take a shortcut and just use innerHTML
                if (typeof a === "string" && !mb.test(a) && (k.htmlSerialize || !gb.test(a)) && (k.leadingWhitespace || !hb.test(a)) && !rb[(jb.exec(a) || [ "", "" ])[1].toLowerCase()]) {
                    a = a.replace(ib, "<$1></$2>");
                    try {
                        for (;c < d; c++) {
                            // Remove element nodes and prevent memory leaks
                            b = this[c] || {};
                            if (b.nodeType === 1) {
                                m.cleanData(ub(b, false));
                                b.innerHTML = a;
                            }
                        }
                        b = 0;
                    } catch (e) {}
                }
                if (b) {
                    this.empty().append(a);
                }
            }, null, a, arguments.length);
        },
        replaceWith: function() {
            var a = arguments[0];
            // Make the changes, replacing each context element with the new content
            this.domManip(arguments, function(b) {
                a = this.parentNode;
                m.cleanData(ub(this));
                if (a) {
                    a.replaceChild(b, this);
                }
            });
            // Force removal if there was no new content (e.g., from empty arguments)
            return a && (a.length || a.nodeType) ? this : this.remove();
        },
        detach: function(a) {
            return this.remove(a, true);
        },
        domManip: function(a, b) {
            // Flatten any nested arrays
            a = e.apply([], a);
            var c, d, f, g, h, i, j = 0, l = this.length, n = this, o = l - 1, p = a[0], q = m.isFunction(p);
            // We can't cloneNode fragments that contain checked, in WebKit
            if (q || l > 1 && typeof p === "string" && !k.checkClone && nb.test(p)) {
                return this.each(function(c) {
                    var d = n.eq(c);
                    if (q) {
                        a[0] = p.call(this, c, d.html());
                    }
                    d.domManip(a, b);
                });
            }
            if (l) {
                i = m.buildFragment(a, this[0].ownerDocument, false, this);
                c = i.firstChild;
                if (i.childNodes.length === 1) {
                    i = c;
                }
                if (c) {
                    g = m.map(ub(i, "script"), xb);
                    f = g.length;
                    // Use the original fragment for the last item instead of the first because it can end up
                    // being emptied incorrectly in certain situations (#8070).
                    for (;j < l; j++) {
                        d = i;
                        if (j !== o) {
                            d = m.clone(d, true, true);
                            // Keep references to cloned scripts for later restoration
                            if (f) {
                                m.merge(g, ub(d, "script"));
                            }
                        }
                        b.call(this[j], d, j);
                    }
                    if (f) {
                        h = g[g.length - 1].ownerDocument;
                        // Reenable scripts
                        m.map(g, yb);
                        // Evaluate executable scripts on first document insertion
                        for (j = 0; j < f; j++) {
                            d = g[j];
                            if (ob.test(d.type || "") && !m._data(d, "globalEval") && m.contains(h, d)) {
                                if (d.src) {
                                    // Optional AJAX dependency, but won't run scripts if not present
                                    if (m._evalUrl) {
                                        m._evalUrl(d.src);
                                    }
                                } else {
                                    m.globalEval((d.text || d.textContent || d.innerHTML || "").replace(qb, ""));
                                }
                            }
                        }
                    }
                    // Fix #11809: Avoid leaking memory
                    i = c = null;
                }
            }
            return this;
        }
    });
    m.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        m.fn[a] = function(a) {
            var c, d = 0, e = [], g = m(a), h = g.length - 1;
            for (;d <= h; d++) {
                c = d === h ? this : this.clone(true);
                m(g[d])[b](c);
                // Modern browsers can apply jQuery collections as arrays, but oldIE needs a .get()
                f.apply(e, c.get());
            }
            return this.pushStack(e);
        };
    });
    var Cb, Db = {};
    /**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
    // Called only from within defaultDisplay
    function Eb(b, c) {
        var d, e = m(c.createElement(b)).appendTo(c.body), // getDefaultComputedStyle might be reliably used only on attached element
        f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? // Use of this method is a temporary fix (more like optmization) until something better comes along,
        // since it was removed from specification and supported only in FF
        d.display : m.css(e[0], "display");
        // We don't have any data stored on the element,
        // so use "detach" method as fast way to get rid of the element
        e.detach();
        return f;
    }
    /**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
    function Fb(a) {
        var b = y, c = Db[a];
        if (!c) {
            c = Eb(a, b);
            // If the simple way fails, read from inside an iframe
            if (c === "none" || !c) {
                // Use the already-created iframe if possible
                Cb = (Cb || m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement);
                // Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
                b = (Cb[0].contentWindow || Cb[0].contentDocument).document;
                // Support: IE
                b.write();
                b.close();
                c = Eb(a, b);
                Cb.detach();
            }
            // Store the correct default display
            Db[a] = c;
        }
        return c;
    }
    (function() {
        var a;
        k.shrinkWrapBlocks = function() {
            if (a != null) {
                return a;
            }
            // Will be changed later if needed.
            a = false;
            // Minified: var b,c,d
            var b, c, d;
            c = y.getElementsByTagName("body")[0];
            if (!c || !c.style) {
                // Test fired too early or in an unsupported environment, exit.
                return;
            }
            // Setup
            b = y.createElement("div");
            d = y.createElement("div");
            d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px";
            c.appendChild(d).appendChild(b);
            // Support: IE6
            // Check if elements with layout shrink-wrap their children
            if (typeof b.style.zoom !== K) {
                // Reset CSS: box-sizing; display; margin; border
                b.style.cssText = // Support: Firefox<29, Android 2.3
                // Vendor-prefix box-sizing
                "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" + "box-sizing:content-box;display:block;margin:0;border:0;" + "padding:1px;width:1px;zoom:1";
                b.appendChild(y.createElement("div")).style.width = "5px";
                a = b.offsetWidth !== 3;
            }
            c.removeChild(d);
            return a;
        };
    })();
    var Gb = /^margin/;
    var Hb = new RegExp("^(" + S + ")(?!px)[a-z%]+$", "i");
    var Ib, Jb, Kb = /^(top|right|bottom|left)$/;
    if (a.getComputedStyle) {
        Ib = function(a) {
            return a.ownerDocument.defaultView.getComputedStyle(a, null);
        };
        Jb = function(a, b, c) {
            var d, e, f, g, h = a.style;
            c = c || Ib(a);
            // getPropertyValue is only needed for .css('filter') in IE9, see #12537
            g = c ? c.getPropertyValue(b) || c[b] : undefined;
            if (c) {
                if (g === "" && !m.contains(a.ownerDocument, a)) {
                    g = m.style(a, b);
                }
                // A tribute to the "awesome hack by Dean Edwards"
                // Chrome < 17 and Safari 5.0 uses "computed value" instead of "used value" for margin-right
                // Safari 5.1.7 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
                // this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
                if (Hb.test(g) && Gb.test(b)) {
                    // Remember the original values
                    d = h.width;
                    e = h.minWidth;
                    f = h.maxWidth;
                    // Put in the new values to get a computed value out
                    h.minWidth = h.maxWidth = h.width = g;
                    g = c.width;
                    // Revert the changed values
                    h.width = d;
                    h.minWidth = e;
                    h.maxWidth = f;
                }
            }
            // Support: IE
            // IE returns zIndex value as an integer.
            return g === undefined ? g : g + "";
        };
    } else if (y.documentElement.currentStyle) {
        Ib = function(a) {
            return a.currentStyle;
        };
        Jb = function(a, b, c) {
            var d, e, f, g, h = a.style;
            c = c || Ib(a);
            g = c ? c[b] : undefined;
            // Avoid setting ret to empty string here
            // so we don't default to auto
            if (g == null && h && h[b]) {
                g = h[b];
            }
            // From the awesome hack by Dean Edwards
            // http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291
            // If we're not dealing with a regular pixel number
            // but a number that has a weird ending, we need to convert it to pixels
            // but not position css attributes, as those are proportional to the parent element instead
            // and we can't measure the parent instead because it might trigger a "stacking dolls" problem
            if (Hb.test(g) && !Kb.test(b)) {
                // Remember the original values
                d = h.left;
                e = a.runtimeStyle;
                f = e && e.left;
                // Put in the new values to get a computed value out
                if (f) {
                    e.left = a.currentStyle.left;
                }
                h.left = b === "fontSize" ? "1em" : g;
                g = h.pixelLeft + "px";
                // Revert the changed values
                h.left = d;
                if (f) {
                    e.left = f;
                }
            }
            // Support: IE
            // IE returns zIndex value as an integer.
            return g === undefined ? g : g + "" || "auto";
        };
    }
    function Lb(a, b) {
        // Define the hook, we'll check on the first run if it's really needed.
        return {
            get: function() {
                var c = a();
                if (c == null) {
                    // The test was not ready at this point; screw the hook this time
                    // but check again when needed next time.
                    return;
                }
                if (c) {
                    // Hook not needed (or it's not possible to use it due to missing dependency),
                    // remove it.
                    // Since there are no other hooks for marginRight, remove the whole object.
                    delete this.get;
                    return;
                }
                // Hook needed; redefine it so that the support test is not executed again.
                return (this.get = b).apply(this, arguments);
            }
        };
    }
    (function() {
        // Minified: var b,c,d,e,f,g, h,i
        var b, c, d, e, f, g, h;
        // Setup
        b = y.createElement("div");
        b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
        d = b.getElementsByTagName("a")[0];
        c = d && d.style;
        // Finish early in limited (non-browser) environments
        if (!c) {
            return;
        }
        c.cssText = "float:left;opacity:.5";
        // Support: IE<9
        // Make sure that element opacity exists (as opposed to filter)
        k.opacity = c.opacity === "0.5";
        // Verify style float existence
        // (IE uses styleFloat instead of cssFloat)
        k.cssFloat = !!c.cssFloat;
        b.style.backgroundClip = "content-box";
        b.cloneNode(true).style.backgroundClip = "";
        k.clearCloneStyle = b.style.backgroundClip === "content-box";
        // Support: Firefox<29, Android 2.3
        // Vendor-prefix box-sizing
        k.boxSizing = c.boxSizing === "" || c.MozBoxSizing === "" || c.WebkitBoxSizing === "";
        m.extend(k, {
            reliableHiddenOffsets: function() {
                if (g == null) {
                    i();
                }
                return g;
            },
            boxSizingReliable: function() {
                if (f == null) {
                    i();
                }
                return f;
            },
            pixelPosition: function() {
                if (e == null) {
                    i();
                }
                return e;
            },
            // Support: Android 2.3
            reliableMarginRight: function() {
                if (h == null) {
                    i();
                }
                return h;
            }
        });
        function i() {
            // Minified: var b,c,d,j
            var b, c, d, i;
            c = y.getElementsByTagName("body")[0];
            if (!c || !c.style) {
                // Test fired too early or in an unsupported environment, exit.
                return;
            }
            // Setup
            b = y.createElement("div");
            d = y.createElement("div");
            d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px";
            c.appendChild(d).appendChild(b);
            b.style.cssText = // Support: Firefox<29, Android 2.3
            // Vendor-prefix box-sizing
            "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" + "box-sizing:border-box;display:block;margin-top:1%;top:1%;" + "border:1px;padding:1px;width:4px;position:absolute";
            // Support: IE<9
            // Assume reasonable values in the absence of getComputedStyle
            e = f = false;
            h = true;
            // Check for getComputedStyle so that this code is not run in IE<9.
            if (a.getComputedStyle) {
                e = (a.getComputedStyle(b, null) || {}).top !== "1%";
                f = (a.getComputedStyle(b, null) || {
                    width: "4px"
                }).width === "4px";
                // Support: Android 2.3
                // Div with explicit width and no margin-right incorrectly
                // gets computed margin-right based on width of container (#3333)
                // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
                i = b.appendChild(y.createElement("div"));
                // Reset CSS: box-sizing; display; margin; border; padding
                i.style.cssText = b.style.cssText = // Support: Firefox<29, Android 2.3
                // Vendor-prefix box-sizing
                "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" + "box-sizing:content-box;display:block;margin:0;border:0;padding:0";
                i.style.marginRight = i.style.width = "0";
                b.style.width = "1px";
                h = !parseFloat((a.getComputedStyle(i, null) || {}).marginRight);
            }
            // Support: IE8
            // Check if table cells still have offsetWidth/Height when they are set
            // to display:none and there are still other visible table cells in a
            // table row; if so, offsetWidth/Height are not reliable for use when
            // determining if an element has been hidden directly using
            // display:none (it is still safe to use offsets if a parent element is
            // hidden; don safety goggles and see bug #4512 for more information).
            b.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
            i = b.getElementsByTagName("td");
            i[0].style.cssText = "margin:0;border:0;padding:0;display:none";
            g = i[0].offsetHeight === 0;
            if (g) {
                i[0].style.display = "";
                i[1].style.display = "none";
                g = i[0].offsetHeight === 0;
            }
            c.removeChild(d);
        }
    })();
    // A method for quickly swapping in/out CSS properties to get correct calculations.
    m.swap = function(a, b, c, d) {
        var e, f, g = {};
        // Remember the old values, and insert the new ones
        for (f in b) {
            g[f] = a.style[f];
            a.style[f] = b[f];
        }
        e = c.apply(a, d || []);
        // Revert the old values
        for (f in b) {
            a.style[f] = g[f];
        }
        return e;
    };
    var Mb = /alpha\([^)]*\)/i, Nb = /opacity\s*=\s*([^)]*)/, // swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
    // see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
    Ob = /^(none|table(?!-c[ea]).+)/, Pb = new RegExp("^(" + S + ")(.*)$", "i"), Qb = new RegExp("^([+-])=(" + S + ")", "i"), Rb = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, Sb = {
        letterSpacing: "0",
        fontWeight: "400"
    }, Tb = [ "Webkit", "O", "Moz", "ms" ];
    // return a css property mapped to a potentially vendor prefixed property
    function Ub(a, b) {
        // shortcut for names that are not vendor prefixed
        if (b in a) {
            return b;
        }
        // check for vendor prefixed names
        var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = Tb.length;
        while (e--) {
            b = Tb[e] + c;
            if (b in a) {
                return b;
            }
        }
        return d;
    }
    function Vb(a, b) {
        var c, d, e, f = [], g = 0, h = a.length;
        for (;g < h; g++) {
            d = a[g];
            if (!d.style) {
                continue;
            }
            f[g] = m._data(d, "olddisplay");
            c = d.style.display;
            if (b) {
                // Reset the inline display of this element to learn if it is
                // being hidden by cascaded rules or not
                if (!f[g] && c === "none") {
                    d.style.display = "";
                }
                // Set elements which have been overridden with display: none
                // in a stylesheet to whatever the default browser style is
                // for such an element
                if (d.style.display === "" && U(d)) {
                    f[g] = m._data(d, "olddisplay", Fb(d.nodeName));
                }
            } else {
                e = U(d);
                if (c && c !== "none" || !e) {
                    m._data(d, "olddisplay", e ? c : m.css(d, "display"));
                }
            }
        }
        // Set the display of most of the elements in a second loop
        // to avoid the constant reflow
        for (g = 0; g < h; g++) {
            d = a[g];
            if (!d.style) {
                continue;
            }
            if (!b || d.style.display === "none" || d.style.display === "") {
                d.style.display = b ? f[g] || "" : "none";
            }
        }
        return a;
    }
    function Wb(a, b, c) {
        var d = Pb.exec(b);
        // Guard against undefined "subtract", e.g., when used as in cssHooks
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b;
    }
    function Xb(a, b, c, d, e) {
        var f = c === (d ? "border" : "content") ? // If we already have the right measurement, avoid augmentation
        4 : // Otherwise initialize for horizontal or vertical properties
        b === "width" ? 1 : 0, g = 0;
        for (;f < 4; f += 2) {
            // both box models exclude margin, so add it if we want it
            if (c === "margin") {
                g += m.css(a, c + T[f], true, e);
            }
            if (d) {
                // border-box includes padding, so remove it if we want content
                if (c === "content") {
                    g -= m.css(a, "padding" + T[f], true, e);
                }
                // at this point, extra isn't border nor margin, so remove border
                if (c !== "margin") {
                    g -= m.css(a, "border" + T[f] + "Width", true, e);
                }
            } else {
                // at this point, extra isn't content, so add padding
                g += m.css(a, "padding" + T[f], true, e);
                // at this point, extra isn't content nor padding, so add border
                if (c !== "padding") {
                    g += m.css(a, "border" + T[f] + "Width", true, e);
                }
            }
        }
        return g;
    }
    function Yb(a, b, c) {
        // Start with offset property, which is equivalent to the border-box value
        var d = true, e = b === "width" ? a.offsetWidth : a.offsetHeight, f = Ib(a), g = k.boxSizing && m.css(a, "boxSizing", false, f) === "border-box";
        // some non-html elements return undefined for offsetWidth, so check for null/undefined
        // svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
        // MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
        if (e <= 0 || e == null) {
            // Fall back to computed then uncomputed css if necessary
            e = Jb(a, b, f);
            if (e < 0 || e == null) {
                e = a.style[b];
            }
            // Computed unit is not pixels. Stop here and return.
            if (Hb.test(e)) {
                return e;
            }
            // we need the check for style in case a browser which returns unreliable values
            // for getComputedStyle silently falls back to the reliable elem.style
            d = g && (k.boxSizingReliable() || e === a.style[b]);
            // Normalize "", auto, and prepare for extra
            e = parseFloat(e) || 0;
        }
        // use the active box-sizing model to add/subtract irrelevant styles
        return e + Xb(a, b, c || (g ? "border" : "content"), d, f) + "px";
    }
    m.extend({
        // Add in style property hooks for overriding the default
        // behavior of getting and setting a style property
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        // We should always get a number back from opacity
                        var c = Jb(a, "opacity");
                        return c === "" ? "1" : c;
                    }
                }
            }
        },
        // Don't automatically add "px" to these possibly-unitless properties
        cssNumber: {
            columnCount: true,
            fillOpacity: true,
            flexGrow: true,
            flexShrink: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            order: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        // Add in properties whose names you wish to fix before
        // setting or getting the value
        cssProps: {
            // normalize float css property
            "float": k.cssFloat ? "cssFloat" : "styleFloat"
        },
        // Get and set the style property on a DOM Node
        style: function(a, b, c, d) {
            // Don't set styles on text and comment nodes
            if (!a || a.nodeType === 3 || a.nodeType === 8 || !a.style) {
                return;
            }
            // Make sure that we're working with the right name
            var e, f, g, h = m.camelCase(b), i = a.style;
            b = m.cssProps[h] || (m.cssProps[h] = Ub(i, h));
            // gets hook for the prefixed version
            // followed by the unprefixed version
            g = m.cssHooks[b] || m.cssHooks[h];
            // Check if we're setting a value
            if (c !== undefined) {
                f = typeof c;
                // convert relative number strings (+= or -=) to relative numbers. #7345
                if (f === "string" && (e = Qb.exec(c))) {
                    c = (e[1] + 1) * e[2] + parseFloat(m.css(a, b));
                    // Fixes bug #9237
                    f = "number";
                }
                // Make sure that null and NaN values aren't set. See: #7116
                if (c == null || c !== c) {
                    return;
                }
                // If a number was passed in, add 'px' to the (except for certain CSS properties)
                if (f === "number" && !m.cssNumber[h]) {
                    c += "px";
                }
                // Fixes #8908, it can be done more correctly by specifing setters in cssHooks,
                // but it would mean to define eight (for every problematic property) identical functions
                if (!k.clearCloneStyle && c === "" && b.indexOf("background") === 0) {
                    i[b] = "inherit";
                }
                // If a hook was provided, use that value, otherwise just set the specified value
                if (!g || !("set" in g) || (c = g.set(a, c, d)) !== undefined) {
                    // Support: IE
                    // Swallow errors from 'invalid' CSS values (#5509)
                    try {
                        i[b] = c;
                    } catch (j) {}
                }
            } else {
                // If a hook was provided get the non-computed value from there
                if (g && "get" in g && (e = g.get(a, false, d)) !== undefined) {
                    return e;
                }
                // Otherwise just get the value from the style object
                return i[b];
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = m.camelCase(b);
            // Make sure that we're working with the right name
            b = m.cssProps[h] || (m.cssProps[h] = Ub(a.style, h));
            // gets hook for the prefixed version
            // followed by the unprefixed version
            g = m.cssHooks[b] || m.cssHooks[h];
            // If a hook was provided get the computed value from there
            if (g && "get" in g) {
                f = g.get(a, true, c);
            }
            // Otherwise, if a way to get the computed value exists, use that
            if (f === undefined) {
                f = Jb(a, b, d);
            }
            //convert "normal" to computed value
            if (f === "normal" && b in Sb) {
                f = Sb[b];
            }
            // Return, converting to number if forced or a qualifier was provided and val looks numeric
            if (c === "" || c) {
                e = parseFloat(f);
                return c === true || m.isNumeric(e) ? e || 0 : f;
            }
            return f;
        }
    });
    m.each([ "height", "width" ], function(a, b) {
        m.cssHooks[b] = {
            get: function(a, c, d) {
                if (c) {
                    // certain elements can have dimension info if we invisibly show them
                    // however, it must have a current display style that would benefit from this
                    return Ob.test(m.css(a, "display")) && a.offsetWidth === 0 ? m.swap(a, Rb, function() {
                        return Yb(a, b, d);
                    }) : Yb(a, b, d);
                }
            },
            set: function(a, c, d) {
                var e = d && Ib(a);
                return Wb(a, c, d ? Xb(a, b, d, k.boxSizing && m.css(a, "boxSizing", false, e) === "border-box", e) : 0);
            }
        };
    });
    if (!k.opacity) {
        m.cssHooks.opacity = {
            get: function(a, b) {
                // IE uses filters for opacity
                return Nb.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : "";
            },
            set: function(a, b) {
                var c = a.style, d = a.currentStyle, e = m.isNumeric(b) ? "alpha(opacity=" + b * 100 + ")" : "", f = d && d.filter || c.filter || "";
                // IE has trouble with opacity if it does not have layout
                // Force it by setting the zoom level
                c.zoom = 1;
                // if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
                // if value === "", then remove inline opacity #12685
                if ((b >= 1 || b === "") && m.trim(f.replace(Mb, "")) === "" && c.removeAttribute) {
                    // Setting style.filter to null, "" & " " still leave "filter:" in the cssText
                    // if "filter:" is present at all, clearType is disabled, we want to avoid this
                    // style.removeAttribute is IE Only, but so apparently is this code path...
                    c.removeAttribute("filter");
                    // if there is no filter style applied in a css rule or unset inline opacity, we are done
                    if (b === "" || d && !d.filter) {
                        return;
                    }
                }
                // otherwise, set new filter values
                c.filter = Mb.test(f) ? f.replace(Mb, e) : f + " " + e;
            }
        };
    }
    m.cssHooks.marginRight = Lb(k.reliableMarginRight, function(a, b) {
        if (b) {
            // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
            // Work around by temporarily setting element display to inline-block
            return m.swap(a, {
                display: "inline-block"
            }, Jb, [ a, "marginRight" ]);
        }
    });
    // These hooks are used by animate to expand properties
    m.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        m.cssHooks[a + b] = {
            expand: function(c) {
                var d = 0, e = {}, // assumes a single number if not a string
                f = typeof c === "string" ? c.split(" ") : [ c ];
                for (;d < 4; d++) {
                    e[a + T[d] + b] = f[d] || f[d - 2] || f[0];
                }
                return e;
            }
        };
        if (!Gb.test(a)) {
            m.cssHooks[a + b].set = Wb;
        }
    });
    m.fn.extend({
        css: function(a, b) {
            return V(this, function(a, b, c) {
                var d, e, f = {}, g = 0;
                if (m.isArray(b)) {
                    d = Ib(a);
                    e = b.length;
                    for (;g < e; g++) {
                        f[b[g]] = m.css(a, b[g], false, d);
                    }
                    return f;
                }
                return c !== undefined ? m.style(a, b, c) : m.css(a, b);
            }, a, b, arguments.length > 1);
        },
        show: function() {
            return Vb(this, true);
        },
        hide: function() {
            return Vb(this);
        },
        toggle: function(a) {
            if (typeof a === "boolean") {
                return a ? this.show() : this.hide();
            }
            return this.each(function() {
                if (U(this)) {
                    m(this).show();
                } else {
                    m(this).hide();
                }
            });
        }
    });
    function Zb(a, b, c, d, e) {
        return new Zb.prototype.init(a, b, c, d, e);
    }
    m.Tween = Zb;
    Zb.prototype = {
        constructor: Zb,
        init: function(a, b, c, d, e, f) {
            this.elem = a;
            this.prop = c;
            this.easing = e || "swing";
            this.options = b;
            this.start = this.now = this.cur();
            this.end = d;
            this.unit = f || (m.cssNumber[c] ? "" : "px");
        },
        cur: function() {
            var a = Zb.propHooks[this.prop];
            return a && a.get ? a.get(this) : Zb.propHooks._default.get(this);
        },
        run: function(a) {
            var b, c = Zb.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = b = m.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration);
            } else {
                this.pos = b = a;
            }
            this.now = (this.end - this.start) * b + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this);
            }
            if (c && c.set) {
                c.set(this);
            } else {
                Zb.propHooks._default.set(this);
            }
            return this;
        }
    };
    Zb.prototype.init.prototype = Zb.prototype;
    Zb.propHooks = {
        _default: {
            get: function(a) {
                var b;
                if (a.elem[a.prop] != null && (!a.elem.style || a.elem.style[a.prop] == null)) {
                    return a.elem[a.prop];
                }
                // passing an empty string as a 3rd parameter to .css will automatically
                // attempt a parseFloat and fallback to a string if the parse fails
                // so, simple values such as "10px" are parsed to Float.
                // complex values such as "rotate(1rad)" are returned as is.
                b = m.css(a.elem, a.prop, "");
                // Empty strings, null, undefined and "auto" are converted to 0.
                return !b || b === "auto" ? 0 : b;
            },
            set: function(a) {
                // use step hook for back compat - use cssHook if its there - use .style if its
                // available and use plain properties where available
                if (m.fx.step[a.prop]) {
                    m.fx.step[a.prop](a);
                } else if (a.elem.style && (a.elem.style[m.cssProps[a.prop]] != null || m.cssHooks[a.prop])) {
                    m.style(a.elem, a.prop, a.now + a.unit);
                } else {
                    a.elem[a.prop] = a.now;
                }
            }
        }
    };
    // Support: IE <=9
    // Panic based approach to setting things on disconnected nodes
    Zb.propHooks.scrollTop = Zb.propHooks.scrollLeft = {
        set: function(a) {
            if (a.elem.nodeType && a.elem.parentNode) {
                a.elem[a.prop] = a.now;
            }
        }
    };
    m.easing = {
        linear: function(a) {
            return a;
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2;
        }
    };
    m.fx = Zb.prototype.init;
    // Back Compat <1.8 extension point
    m.fx.step = {};
    var $b, _b, ac = /^(?:toggle|show|hide)$/, bc = new RegExp("^(?:([+-])=|)(" + S + ")([a-z%]*)$", "i"), cc = /queueHooks$/, dc = [ ic ], ec = {
        "*": [ function(a, b) {
            var c = this.createTween(a, b), d = c.cur(), e = bc.exec(b), f = e && e[3] || (m.cssNumber[a] ? "" : "px"), // Starting value computation is required for potential unit mismatches
            g = (m.cssNumber[a] || f !== "px" && +d) && bc.exec(m.css(c.elem, a)), h = 1, i = 20;
            if (g && g[3] !== f) {
                // Trust units reported by jQuery.css
                f = f || g[3];
                // Make sure we update the tween properties later on
                e = e || [];
                // Iteratively approximate from a nonzero starting point
                g = +d || 1;
                do {
                    // If previous iteration zeroed out, double until we get *something*
                    // Use a string for doubling factor so we don't accidentally see scale as unchanged below
                    h = h || ".5";
                    // Adjust and apply
                    g = g / h;
                    m.style(c.elem, a, g + f);
                } while (h !== (h = c.cur() / d) && h !== 1 && --i);
            }
            // Update tween properties
            if (e) {
                g = c.start = +g || +d || 0;
                c.unit = f;
                // If a +=/-= token was provided, we're doing a relative animation
                c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2];
            }
            return c;
        } ]
    };
    // Animations created synchronously will run synchronously
    function fc() {
        setTimeout(function() {
            $b = undefined;
        });
        return $b = m.now();
    }
    // Generate parameters to create a standard animation
    function gc(a, b) {
        var c, d = {
            height: a
        }, e = 0;
        // if we include width, step value is 1 to do all cssExpand values,
        // if we don't include width, step value is 2 to skip over Left and Right
        b = b ? 1 : 0;
        for (;e < 4; e += 2 - b) {
            c = T[e];
            d["margin" + c] = d["padding" + c] = a;
        }
        if (b) {
            d.opacity = d.width = a;
        }
        return d;
    }
    function hc(a, b, c) {
        var d, e = (ec[b] || []).concat(ec["*"]), f = 0, g = e.length;
        for (;f < g; f++) {
            if (d = e[f].call(c, b, a)) {
                // we're done with this property
                return d;
            }
        }
    }
    function ic(a, b, c) {
        /* jshint validthis: true */
        var d, e, f, g, h, i, j, l, n = this, o = {}, p = a.style, q = a.nodeType && U(a), r = m._data(a, "fxshow");
        // handle queue: false promises
        if (!c.queue) {
            h = m._queueHooks(a, "fx");
            if (h.unqueued == null) {
                h.unqueued = 0;
                i = h.empty.fire;
                h.empty.fire = function() {
                    if (!h.unqueued) {
                        i();
                    }
                };
            }
            h.unqueued++;
            n.always(function() {
                // doing this makes sure that the complete handler will be called
                // before this completes
                n.always(function() {
                    h.unqueued--;
                    if (!m.queue(a, "fx").length) {
                        h.empty.fire();
                    }
                });
            });
        }
        // height/width overflow pass
        if (a.nodeType === 1 && ("height" in b || "width" in b)) {
            // Make sure that nothing sneaks out
            // Record all 3 overflow attributes because IE does not
            // change the overflow attribute when overflowX and
            // overflowY are set to the same value
            c.overflow = [ p.overflow, p.overflowX, p.overflowY ];
            // Set display property to inline-block for height/width
            // animations on inline elements that are having width/height animated
            j = m.css(a, "display");
            // Test default display if display is currently "none"
            l = j === "none" ? m._data(a, "olddisplay") || Fb(a.nodeName) : j;
            if (l === "inline" && m.css(a, "float") === "none") {
                // inline-level elements accept inline-block;
                // block-level elements need to be inline with layout
                if (!k.inlineBlockNeedsLayout || Fb(a.nodeName) === "inline") {
                    p.display = "inline-block";
                } else {
                    p.zoom = 1;
                }
            }
        }
        if (c.overflow) {
            p.overflow = "hidden";
            if (!k.shrinkWrapBlocks()) {
                n.always(function() {
                    p.overflow = c.overflow[0];
                    p.overflowX = c.overflow[1];
                    p.overflowY = c.overflow[2];
                });
            }
        }
        // show/hide pass
        for (d in b) {
            e = b[d];
            if (ac.exec(e)) {
                delete b[d];
                f = f || e === "toggle";
                if (e === (q ? "hide" : "show")) {
                    // If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
                    if (e === "show" && r && r[d] !== undefined) {
                        q = true;
                    } else {
                        continue;
                    }
                }
                o[d] = r && r[d] || m.style(a, d);
            } else {
                j = undefined;
            }
        }
        if (!m.isEmptyObject(o)) {
            if (r) {
                if ("hidden" in r) {
                    q = r.hidden;
                }
            } else {
                r = m._data(a, "fxshow", {});
            }
            // store state if its toggle - enables .stop().toggle() to "reverse"
            if (f) {
                r.hidden = !q;
            }
            if (q) {
                m(a).show();
            } else {
                n.done(function() {
                    m(a).hide();
                });
            }
            n.done(function() {
                var b;
                m._removeData(a, "fxshow");
                for (b in o) {
                    m.style(a, b, o[b]);
                }
            });
            for (d in o) {
                g = hc(q ? r[d] : 0, d, n);
                if (!(d in r)) {
                    r[d] = g.start;
                    if (q) {
                        g.end = g.start;
                        g.start = d === "width" || d === "height" ? 1 : 0;
                    }
                }
            }
        } else if ((j === "none" ? Fb(a.nodeName) : j) === "inline") {
            p.display = j;
        }
    }
    function jc(a, b) {
        var c, d, e, f, g;
        // camelCase, specialEasing and expand cssHook pass
        for (c in a) {
            d = m.camelCase(c);
            e = b[d];
            f = a[c];
            if (m.isArray(f)) {
                e = f[1];
                f = a[c] = f[0];
            }
            if (c !== d) {
                a[d] = f;
                delete a[c];
            }
            g = m.cssHooks[d];
            if (g && "expand" in g) {
                f = g.expand(f);
                delete a[d];
                // not quite $.extend, this wont overwrite keys already present.
                // also - reusing 'index' from above because we have the correct "name"
                for (c in f) {
                    if (!(c in a)) {
                        a[c] = f[c];
                        b[c] = e;
                    }
                }
            } else {
                b[d] = e;
            }
        }
    }
    function kc(a, b, c) {
        var d, e, f = 0, g = dc.length, h = m.Deferred().always(function() {
            // don't match elem in the :animated selector
            delete i.elem;
        }), i = function() {
            if (e) {
                return false;
            }
            var b = $b || fc(), c = Math.max(0, j.startTime + j.duration - b), // archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)
            d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length;
            for (;g < i; g++) {
                j.tweens[g].run(f);
            }
            h.notifyWith(a, [ j, f, c ]);
            if (f < 1 && i) {
                return c;
            } else {
                h.resolveWith(a, [ j ]);
                return false;
            }
        }, j = h.promise({
            elem: a,
            props: m.extend({}, b),
            opts: m.extend(true, {
                specialEasing: {}
            }, c),
            originalProperties: b,
            originalOptions: c,
            startTime: $b || fc(),
            duration: c.duration,
            tweens: [],
            createTween: function(b, c) {
                var d = m.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                j.tweens.push(d);
                return d;
            },
            stop: function(b) {
                var c = 0, // if we are going to the end, we want to run all the tweens
                // otherwise we skip this part
                d = b ? j.tweens.length : 0;
                if (e) {
                    return this;
                }
                e = true;
                for (;c < d; c++) {
                    j.tweens[c].run(1);
                }
                // resolve when we played the last frame
                // otherwise, reject
                if (b) {
                    h.resolveWith(a, [ j, b ]);
                } else {
                    h.rejectWith(a, [ j, b ]);
                }
                return this;
            }
        }), k = j.props;
        jc(k, j.opts.specialEasing);
        for (;f < g; f++) {
            d = dc[f].call(j, a, k, j.opts);
            if (d) {
                return d;
            }
        }
        m.map(k, hc, j);
        if (m.isFunction(j.opts.start)) {
            j.opts.start.call(a, j);
        }
        m.fx.timer(m.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        }));
        // attach callbacks from options
        return j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always);
    }
    m.Animation = m.extend(kc, {
        tweener: function(a, b) {
            if (m.isFunction(a)) {
                b = a;
                a = [ "*" ];
            } else {
                a = a.split(" ");
            }
            var c, d = 0, e = a.length;
            for (;d < e; d++) {
                c = a[d];
                ec[c] = ec[c] || [];
                ec[c].unshift(b);
            }
        },
        prefilter: function(a, b) {
            if (b) {
                dc.unshift(a);
            } else {
                dc.push(a);
            }
        }
    });
    m.speed = function(a, b, c) {
        var d = a && typeof a === "object" ? m.extend({}, a) : {
            complete: c || !c && b || m.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !m.isFunction(b) && b
        };
        d.duration = m.fx.off ? 0 : typeof d.duration === "number" ? d.duration : d.duration in m.fx.speeds ? m.fx.speeds[d.duration] : m.fx.speeds._default;
        // normalize opt.queue - true/undefined/null -> "fx"
        if (d.queue == null || d.queue === true) {
            d.queue = "fx";
        }
        // Queueing
        d.old = d.complete;
        d.complete = function() {
            if (m.isFunction(d.old)) {
                d.old.call(this);
            }
            if (d.queue) {
                m.dequeue(this, d.queue);
            }
        };
        return d;
    };
    m.fn.extend({
        fadeTo: function(a, b, c, d) {
            // show any hidden elements after setting opacity to 0
            return this.filter(U).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d);
        },
        animate: function(a, b, c, d) {
            var e = m.isEmptyObject(a), f = m.speed(b, c, d), g = function() {
                // Operate on a copy of prop so per-property easing won't be lost
                var b = kc(this, m.extend({}, a), f);
                // Empty animations, or finishing resolves immediately
                if (e || m._data(this, "finish")) {
                    b.stop(true);
                }
            };
            g.finish = g;
            return e || f.queue === false ? this.each(g) : this.queue(f.queue, g);
        },
        stop: function(a, b, c) {
            var d = function(a) {
                var b = a.stop;
                delete a.stop;
                b(c);
            };
            if (typeof a !== "string") {
                c = b;
                b = a;
                a = undefined;
            }
            if (b && a !== false) {
                this.queue(a || "fx", []);
            }
            return this.each(function() {
                var b = true, e = a != null && a + "queueHooks", f = m.timers, g = m._data(this);
                if (e) {
                    if (g[e] && g[e].stop) {
                        d(g[e]);
                    }
                } else {
                    for (e in g) {
                        if (g[e] && g[e].stop && cc.test(e)) {
                            d(g[e]);
                        }
                    }
                }
                for (e = f.length; e--; ) {
                    if (f[e].elem === this && (a == null || f[e].queue === a)) {
                        f[e].anim.stop(c);
                        b = false;
                        f.splice(e, 1);
                    }
                }
                // start the next in the queue if the last step wasn't forced
                // timers currently will call their complete callbacks, which will dequeue
                // but only if they were gotoEnd
                if (b || !c) {
                    m.dequeue(this, a);
                }
            });
        },
        finish: function(a) {
            if (a !== false) {
                a = a || "fx";
            }
            return this.each(function() {
                var b, c = m._data(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = m.timers, g = d ? d.length : 0;
                // enable finishing flag on private data
                c.finish = true;
                // empty the queue first
                m.queue(this, a, []);
                if (e && e.stop) {
                    e.stop.call(this, true);
                }
                // look for any active animations, and finish them
                for (b = f.length; b--; ) {
                    if (f[b].elem === this && f[b].queue === a) {
                        f[b].anim.stop(true);
                        f.splice(b, 1);
                    }
                }
                // look for any animations in the old queue and finish them
                for (b = 0; b < g; b++) {
                    if (d[b] && d[b].finish) {
                        d[b].finish.call(this);
                    }
                }
                // turn off finishing flag
                delete c.finish;
            });
        }
    });
    m.each([ "toggle", "show", "hide" ], function(a, b) {
        var c = m.fn[b];
        m.fn[b] = function(a, d, e) {
            return a == null || typeof a === "boolean" ? c.apply(this, arguments) : this.animate(gc(b, true), a, d, e);
        };
    });
    // Generate shortcuts for custom animations
    m.each({
        slideDown: gc("show"),
        slideUp: gc("hide"),
        slideToggle: gc("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        m.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d);
        };
    });
    m.timers = [];
    m.fx.tick = function() {
        var a, b = m.timers, c = 0;
        $b = m.now();
        for (;c < b.length; c++) {
            a = b[c];
            // Checks the timer has not already been removed
            if (!a() && b[c] === a) {
                b.splice(c--, 1);
            }
        }
        if (!b.length) {
            m.fx.stop();
        }
        $b = undefined;
    };
    m.fx.timer = function(a) {
        m.timers.push(a);
        if (a()) {
            m.fx.start();
        } else {
            m.timers.pop();
        }
    };
    m.fx.interval = 13;
    m.fx.start = function() {
        if (!_b) {
            _b = setInterval(m.fx.tick, m.fx.interval);
        }
    };
    m.fx.stop = function() {
        clearInterval(_b);
        _b = null;
    };
    m.fx.speeds = {
        slow: 600,
        fast: 200,
        // Default speed
        _default: 400
    };
    // Based off of the plugin by Clint Helfers, with permission.
    // http://blindsignals.com/index.php/2009/07/jquery-delay/
    m.fn.delay = function(a, b) {
        a = m.fx ? m.fx.speeds[a] || a : a;
        b = b || "fx";
        return this.queue(b, function(b, c) {
            var d = setTimeout(b, a);
            c.stop = function() {
                clearTimeout(d);
            };
        });
    };
    (function() {
        // Minified: var a,b,c,d,e
        var a, b, c, d, e;
        // Setup
        b = y.createElement("div");
        b.setAttribute("className", "t");
        b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
        d = b.getElementsByTagName("a")[0];
        // First batch of tests.
        c = y.createElement("select");
        e = c.appendChild(y.createElement("option"));
        a = b.getElementsByTagName("input")[0];
        d.style.cssText = "top:1px";
        // Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
        k.getSetAttribute = b.className !== "t";
        // Get the style information from getAttribute
        // (IE uses .cssText instead)
        k.style = /top/.test(d.getAttribute("style"));
        // Make sure that URLs aren't manipulated
        // (IE normalizes it by default)
        k.hrefNormalized = d.getAttribute("href") === "/a";
        // Check the default checkbox/radio value ("" on WebKit; "on" elsewhere)
        k.checkOn = !!a.value;
        // Make sure that a selected-by-default option has a working selected property.
        // (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
        k.optSelected = e.selected;
        // Tests for enctype support on a form (#6743)
        k.enctype = !!y.createElement("form").enctype;
        // Make sure that the options inside disabled selects aren't marked as disabled
        // (WebKit marks them as disabled)
        c.disabled = true;
        k.optDisabled = !e.disabled;
        // Support: IE8 only
        // Check if we can trust getAttribute("value")
        a = y.createElement("input");
        a.setAttribute("value", "");
        k.input = a.getAttribute("value") === "";
        // Check if an input maintains its value after becoming a radio
        a.value = "t";
        a.setAttribute("type", "radio");
        k.radioValue = a.value === "t";
    })();
    var lc = /\r/g;
    m.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0];
            if (!arguments.length) {
                if (e) {
                    b = m.valHooks[e.type] || m.valHooks[e.nodeName.toLowerCase()];
                    if (b && "get" in b && (c = b.get(e, "value")) !== undefined) {
                        return c;
                    }
                    c = e.value;
                    // handle most common string cases
                    // handle cases where value is null/undef or number
                    return typeof c === "string" ? c.replace(lc, "") : c == null ? "" : c;
                }
                return;
            }
            d = m.isFunction(a);
            return this.each(function(c) {
                var e;
                if (this.nodeType !== 1) {
                    return;
                }
                if (d) {
                    e = a.call(this, c, m(this).val());
                } else {
                    e = a;
                }
                // Treat null/undefined as ""; convert numbers to string
                if (e == null) {
                    e = "";
                } else if (typeof e === "number") {
                    e += "";
                } else if (m.isArray(e)) {
                    e = m.map(e, function(a) {
                        return a == null ? "" : a + "";
                    });
                }
                b = m.valHooks[this.type] || m.valHooks[this.nodeName.toLowerCase()];
                // If set returns undefined, fall back to normal setting
                if (!b || !("set" in b) || b.set(this, e, "value") === undefined) {
                    this.value = e;
                }
            });
        }
    });
    m.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = m.find.attr(a, "value");
                    // Support: IE10-11+
                    // option.text throws exceptions (#14686, #14858)
                    return b != null ? b : m.trim(m.text(a));
                }
            },
            select: {
                get: function(a) {
                    var b, c, d = a.options, e = a.selectedIndex, f = a.type === "select-one" || e < 0, g = f ? null : [], h = f ? e + 1 : d.length, i = e < 0 ? h : f ? e : 0;
                    // Loop through all the selected options
                    for (;i < h; i++) {
                        c = d[i];
                        // oldIE doesn't update selected after form reset (#2551)
                        if ((c.selected || i === e) && (// Don't return options that are disabled or in a disabled optgroup
                        k.optDisabled ? !c.disabled : c.getAttribute("disabled") === null) && (!c.parentNode.disabled || !m.nodeName(c.parentNode, "optgroup"))) {
                            // Get the specific value for the option
                            b = m(c).val();
                            // We don't need an array for one selects
                            if (f) {
                                return b;
                            }
                            // Multi-Selects return an array
                            g.push(b);
                        }
                    }
                    return g;
                },
                set: function(a, b) {
                    var c, d, e = a.options, f = m.makeArray(b), g = e.length;
                    while (g--) {
                        d = e[g];
                        if (m.inArray(m.valHooks.option.get(d), f) >= 0) {
                            // Support: IE6
                            // When new option element is added to select box we need to
                            // force reflow of newly added node in order to workaround delay
                            // of initialization properties
                            try {
                                d.selected = c = true;
                            } catch (h) {
                                // Will be executed only in IE6
                                d.scrollHeight;
                            }
                        } else {
                            d.selected = false;
                        }
                    }
                    // Force browsers to behave consistently when non-matching value is set
                    if (!c) {
                        a.selectedIndex = -1;
                    }
                    return e;
                }
            }
        }
    });
    // Radios and checkboxes getter/setter
    m.each([ "radio", "checkbox" ], function() {
        m.valHooks[this] = {
            set: function(a, b) {
                if (m.isArray(b)) {
                    return a.checked = m.inArray(m(a).val(), b) >= 0;
                }
            }
        };
        if (!k.checkOn) {
            m.valHooks[this].get = function(a) {
                // Support: Webkit
                // "" is returned instead of "on" if a value isn't specified
                return a.getAttribute("value") === null ? "on" : a.value;
            };
        }
    });
    var mc, nc, oc = m.expr.attrHandle, pc = /^(?:checked|selected)$/i, qc = k.getSetAttribute, rc = k.input;
    m.fn.extend({
        attr: function(a, b) {
            return V(this, m.attr, a, b, arguments.length > 1);
        },
        removeAttr: function(a) {
            return this.each(function() {
                m.removeAttr(this, a);
            });
        }
    });
    m.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            // don't get/set attributes on text, comment and attribute nodes
            if (!a || f === 3 || f === 8 || f === 2) {
                return;
            }
            // Fallback to prop when attributes are not supported
            if (typeof a.getAttribute === K) {
                return m.prop(a, b, c);
            }
            // All attributes are lowercase
            // Grab necessary hook if one is defined
            if (f !== 1 || !m.isXMLDoc(a)) {
                b = b.toLowerCase();
                d = m.attrHooks[b] || (m.expr.match.bool.test(b) ? nc : mc);
            }
            if (c !== undefined) {
                if (c === null) {
                    m.removeAttr(a, b);
                } else if (d && "set" in d && (e = d.set(a, c, b)) !== undefined) {
                    return e;
                } else {
                    a.setAttribute(b, c + "");
                    return c;
                }
            } else if (d && "get" in d && (e = d.get(a, b)) !== null) {
                return e;
            } else {
                e = m.find.attr(a, b);
                // Non-existent attributes return null, we normalize to undefined
                return e == null ? undefined : e;
            }
        },
        removeAttr: function(a, b) {
            var c, d, e = 0, f = b && b.match(E);
            if (f && a.nodeType === 1) {
                while (c = f[e++]) {
                    d = m.propFix[c] || c;
                    // Boolean attributes get special treatment (#10870)
                    if (m.expr.match.bool.test(c)) {
                        // Set corresponding property to false
                        if (rc && qc || !pc.test(c)) {
                            a[d] = false;
                        } else {
                            a[m.camelCase("default-" + c)] = a[d] = false;
                        }
                    } else {
                        m.attr(a, c, "");
                    }
                    a.removeAttribute(qc ? c : d);
                }
            }
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!k.radioValue && b === "radio" && m.nodeName(a, "input")) {
                        // Setting the type on a radio button after the value resets the value in IE6-9
                        // Reset value to default in case type is set after value during creation
                        var c = a.value;
                        a.setAttribute("type", b);
                        if (c) {
                            a.value = c;
                        }
                        return b;
                    }
                }
            }
        }
    });
    // Hook for boolean attributes
    nc = {
        set: function(a, b, c) {
            if (b === false) {
                // Remove boolean attributes when set to false
                m.removeAttr(a, c);
            } else if (rc && qc || !pc.test(c)) {
                // IE<8 needs the *property* name
                a.setAttribute(!qc && m.propFix[c] || c, c);
            } else {
                a[m.camelCase("default-" + c)] = a[c] = true;
            }
            return c;
        }
    };
    // Retrieve booleans specially
    m.each(m.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = oc[b] || m.find.attr;
        oc[b] = rc && qc || !pc.test(b) ? function(a, b, d) {
            var e, f;
            if (!d) {
                // Avoid an infinite loop by temporarily removing this function from the getter
                f = oc[b];
                oc[b] = e;
                e = c(a, b, d) != null ? b.toLowerCase() : null;
                oc[b] = f;
            }
            return e;
        } : function(a, b, c) {
            if (!c) {
                return a[m.camelCase("default-" + b)] ? b.toLowerCase() : null;
            }
        };
    });
    // fix oldIE attroperties
    if (!rc || !qc) {
        m.attrHooks.value = {
            set: function(a, b, c) {
                if (m.nodeName(a, "input")) {
                    // Does not return so that setAttribute is also used
                    a.defaultValue = b;
                } else {
                    // Use nodeHook if defined (#1954); otherwise setAttribute is fine
                    return mc && mc.set(a, b, c);
                }
            }
        };
    }
    // IE6/7 do not support getting/setting some attributes with get/setAttribute
    if (!qc) {
        // Use this for any attribute in IE6/7
        // This fixes almost every IE6/7 issue
        mc = {
            set: function(a, b, c) {
                // Set the existing or create a new attribute node
                var d = a.getAttributeNode(c);
                if (!d) {
                    a.setAttributeNode(d = a.ownerDocument.createAttribute(c));
                }
                d.value = b += "";
                // Break association with cloned elements by also using setAttribute (#9646)
                if (c === "value" || b === a.getAttribute(c)) {
                    return b;
                }
            }
        };
        // Some attributes are constructed with empty-string values when not defined
        oc.id = oc.name = oc.coords = function(a, b, c) {
            var d;
            if (!c) {
                return (d = a.getAttributeNode(b)) && d.value !== "" ? d.value : null;
            }
        };
        // Fixing value retrieval on a button requires this module
        m.valHooks.button = {
            get: function(a, b) {
                var c = a.getAttributeNode(b);
                if (c && c.specified) {
                    return c.value;
                }
            },
            set: mc.set
        };
        // Set contenteditable to false on removals(#10429)
        // Setting to empty string throws an error as an invalid value
        m.attrHooks.contenteditable = {
            set: function(a, b, c) {
                mc.set(a, b === "" ? false : b, c);
            }
        };
        // Set width and height to auto instead of 0 on empty string( Bug #8150 )
        // This is for removals
        m.each([ "width", "height" ], function(a, b) {
            m.attrHooks[b] = {
                set: function(a, c) {
                    if (c === "") {
                        a.setAttribute(b, "auto");
                        return c;
                    }
                }
            };
        });
    }
    if (!k.style) {
        m.attrHooks.style = {
            get: function(a) {
                // Return undefined in the case of empty string
                // Note: IE uppercases css property names, but if we were to .toLowerCase()
                // .cssText, that would destroy case senstitivity in URL's, like in "background"
                return a.style.cssText || undefined;
            },
            set: function(a, b) {
                return a.style.cssText = b + "";
            }
        };
    }
    var sc = /^(?:input|select|textarea|button|object)$/i, tc = /^(?:a|area)$/i;
    m.fn.extend({
        prop: function(a, b) {
            return V(this, m.prop, a, b, arguments.length > 1);
        },
        removeProp: function(a) {
            a = m.propFix[a] || a;
            return this.each(function() {
                // try/catch handles cases where IE balks (such as removing a property on window)
                try {
                    this[a] = undefined;
                    delete this[a];
                } catch (b) {}
            });
        }
    });
    m.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(a, b, c) {
            var d, e, f, g = a.nodeType;
            // don't get/set properties on text, comment and attribute nodes
            if (!a || g === 3 || g === 8 || g === 2) {
                return;
            }
            f = g !== 1 || !m.isXMLDoc(a);
            if (f) {
                // Fix name and attach hooks
                b = m.propFix[b] || b;
                e = m.propHooks[b];
            }
            if (c !== undefined) {
                return e && "set" in e && (d = e.set(a, c, b)) !== undefined ? d : a[b] = c;
            } else {
                return e && "get" in e && (d = e.get(a, b)) !== null ? d : a[b];
            }
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    // elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
                    // http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
                    // Use proper attribute retrieval(#12072)
                    var b = m.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : sc.test(a.nodeName) || tc.test(a.nodeName) && a.href ? 0 : -1;
                }
            }
        }
    });
    // Some attributes require a special call on IE
    // http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
    if (!k.hrefNormalized) {
        // href/src property should get the full normalized URL (#10299/#12915)
        m.each([ "href", "src" ], function(a, b) {
            m.propHooks[b] = {
                get: function(a) {
                    return a.getAttribute(b, 4);
                }
            };
        });
    }
    // Support: Safari, IE9+
    // mis-reports the default selected property of an option
    // Accessing the parent's selectedIndex property fixes it
    if (!k.optSelected) {
        m.propHooks.selected = {
            get: function(a) {
                var b = a.parentNode;
                if (b) {
                    b.selectedIndex;
                    // Make sure that it also works with optgroups, see #5701
                    if (b.parentNode) {
                        b.parentNode.selectedIndex;
                    }
                }
                return null;
            }
        };
    }
    m.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        m.propFix[this.toLowerCase()] = this;
    });
    // IE6/7 call enctype encoding
    if (!k.enctype) {
        m.propFix.enctype = "encoding";
    }
    var uc = /[\t\r\n\f]/g;
    m.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = typeof a === "string" && a;
            if (m.isFunction(a)) {
                return this.each(function(b) {
                    m(this).addClass(a.call(this, b, this.className));
                });
            }
            if (j) {
                // The disjunction here is for better compressibility (see removeClass)
                b = (a || "").match(E) || [];
                for (;h < i; h++) {
                    c = this[h];
                    d = c.nodeType === 1 && (c.className ? (" " + c.className + " ").replace(uc, " ") : " ");
                    if (d) {
                        f = 0;
                        while (e = b[f++]) {
                            if (d.indexOf(" " + e + " ") < 0) {
                                d += e + " ";
                            }
                        }
                        // only assign if different to avoid unneeded rendering.
                        g = m.trim(d);
                        if (c.className !== g) {
                            c.className = g;
                        }
                    }
                }
            }
            return this;
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = arguments.length === 0 || typeof a === "string" && a;
            if (m.isFunction(a)) {
                return this.each(function(b) {
                    m(this).removeClass(a.call(this, b, this.className));
                });
            }
            if (j) {
                b = (a || "").match(E) || [];
                for (;h < i; h++) {
                    c = this[h];
                    // This expression is here for better compressibility (see addClass)
                    d = c.nodeType === 1 && (c.className ? (" " + c.className + " ").replace(uc, " ") : "");
                    if (d) {
                        f = 0;
                        while (e = b[f++]) {
                            // Remove *all* instances
                            while (d.indexOf(" " + e + " ") >= 0) {
                                d = d.replace(" " + e + " ", " ");
                            }
                        }
                        // only assign if different to avoid unneeded rendering.
                        g = a ? m.trim(d) : "";
                        if (c.className !== g) {
                            c.className = g;
                        }
                    }
                }
            }
            return this;
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            if (typeof b === "boolean" && c === "string") {
                return b ? this.addClass(a) : this.removeClass(a);
            }
            if (m.isFunction(a)) {
                return this.each(function(c) {
                    m(this).toggleClass(a.call(this, c, this.className, b), b);
                });
            }
            return this.each(function() {
                if (c === "string") {
                    // toggle individual class names
                    var b, d = 0, e = m(this), f = a.match(E) || [];
                    while (b = f[d++]) {
                        // check each className given, space separated list
                        if (e.hasClass(b)) {
                            e.removeClass(b);
                        } else {
                            e.addClass(b);
                        }
                    }
                } else if (c === K || c === "boolean") {
                    if (this.className) {
                        // store className if set
                        m._data(this, "__className__", this.className);
                    }
                    // If the element has a class name or if we're passed "false",
                    // then remove the whole classname (if there was one, the above saved it).
                    // Otherwise bring back whatever was previously saved (if anything),
                    // falling back to the empty string if nothing was stored.
                    this.className = this.className || a === false ? "" : m._data(this, "__className__") || "";
                }
            });
        },
        hasClass: function(a) {
            var b = " " + a + " ", c = 0, d = this.length;
            for (;c < d; c++) {
                if (this[c].nodeType === 1 && (" " + this[c].className + " ").replace(uc, " ").indexOf(b) >= 0) {
                    return true;
                }
            }
            return false;
        }
    });
    // Return jQuery for attributes-only inclusion
    m.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function(a, b) {
        // Handle event binding
        m.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
        };
    });
    m.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a);
        },
        bind: function(a, b, c) {
            return this.on(a, null, b, c);
        },
        unbind: function(a, b) {
            return this.off(a, null, b);
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d);
        },
        undelegate: function(a, b, c) {
            // ( namespace ) or ( selector, types [, fn] )
            return arguments.length === 1 ? this.off(a, "**") : this.off(b, a || "**", c);
        }
    });
    var vc = m.now();
    var wc = /\?/;
    var xc = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    m.parseJSON = function(b) {
        // Attempt to parse using the native JSON parser first
        if (a.JSON && a.JSON.parse) {
            // Support: Android 2.3
            // Workaround failure to string-cast null input
            return a.JSON.parse(b + "");
        }
        var c, d = null, e = m.trim(b + "");
        // Guard against invalid (and possibly dangerous) input by ensuring that nothing remains
        // after removing valid tokens
        return e && !m.trim(e.replace(xc, function(a, b, e, f) {
            // Force termination if we see a misplaced comma
            if (c && b) {
                d = 0;
            }
            // Perform no more replacements after returning to outermost depth
            if (d === 0) {
                return a;
            }
            // Commas must not follow "[", "{", or ","
            c = e || b;
            // Determine new depth
            // array/object open ("[" or "{"): depth += true - false (increment)
            // array/object close ("]" or "}"): depth += false - true (decrement)
            // other cases ("," or primitive): depth += true - true (numeric cast)
            d += !f - !e;
            // Remove this token
            return "";
        })) ? Function("return " + e)() : m.error("Invalid JSON: " + b);
    };
    // Cross-browser xml parsing
    m.parseXML = function(b) {
        var c, d;
        if (!b || typeof b !== "string") {
            return null;
        }
        try {
            if (a.DOMParser) {
                // Standard
                d = new DOMParser();
                c = d.parseFromString(b, "text/xml");
            } else {
                // IE
                c = new ActiveXObject("Microsoft.XMLDOM");
                c.async = "false";
                c.loadXML(b);
            }
        } catch (e) {
            c = undefined;
        }
        if (!c || !c.documentElement || c.getElementsByTagName("parsererror").length) {
            m.error("Invalid XML: " + b);
        }
        return c;
    };
    var // Document location
    yc, zc, Ac = /#.*$/, Bc = /([?&])_=[^&]*/, Cc = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, // IE leaves an \r character at EOL
    // #7653, #8125, #8152: local protocol detection
    Dc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Ec = /^(?:GET|HEAD)$/, Fc = /^\/\//, Gc = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, /* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
    Hc = {}, /* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
    Ic = {}, // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
    Jc = "*/".concat("*");
    // #8138, IE may throw an exception when accessing
    // a field from window.location if document.domain has been set
    try {
        zc = location.href;
    } catch (Kc) {
        // Use the href attribute of an A element
        // since IE will modify it given document.location
        zc = y.createElement("a");
        zc.href = "";
        zc = zc.href;
    }
    // Segment location into parts
    yc = Gc.exec(zc.toLowerCase()) || [];
    // Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
    function Lc(a) {
        // dataTypeExpression is optional and defaults to "*"
        return function(b, c) {
            if (typeof b !== "string") {
                c = b;
                b = "*";
            }
            var d, e = 0, f = b.toLowerCase().match(E) || [];
            if (m.isFunction(c)) {
                // For each dataType in the dataTypeExpression
                while (d = f[e++]) {
                    // Prepend if requested
                    if (d.charAt(0) === "+") {
                        d = d.slice(1) || "*";
                        (a[d] = a[d] || []).unshift(c);
                    } else {
                        (a[d] = a[d] || []).push(c);
                    }
                }
            }
        };
    }
    // Base inspection function for prefilters and transports
    function Mc(a, b, c, d) {
        var e = {}, f = a === Ic;
        function g(h) {
            var i;
            e[h] = true;
            m.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                if (typeof j === "string" && !f && !e[j]) {
                    b.dataTypes.unshift(j);
                    g(j);
                    return false;
                } else if (f) {
                    return !(i = j);
                }
            });
            return i;
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*");
    }
    // A special extend for ajax options
    // that takes "flat" options (not to be deep extended)
    // Fixes #9887
    function Nc(a, b) {
        var c, d, e = m.ajaxSettings.flatOptions || {};
        for (d in b) {
            if (b[d] !== undefined) {
                (e[d] ? a : c || (c = {}))[d] = b[d];
            }
        }
        if (c) {
            m.extend(true, a, c);
        }
        return a;
    }
    /* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
    function Oc(a, b, c) {
        var d, e, f, g, h = a.contents, i = a.dataTypes;
        // Remove auto dataType and get content-type in the process
        while (i[0] === "*") {
            i.shift();
            if (e === undefined) {
                e = a.mimeType || b.getResponseHeader("Content-Type");
            }
        }
        // Check if we're dealing with a known content-type
        if (e) {
            for (g in h) {
                if (h[g] && h[g].test(e)) {
                    i.unshift(g);
                    break;
                }
            }
        }
        // Check to see if we have a response for the expected dataType
        if (i[0] in c) {
            f = i[0];
        } else {
            // Try convertible dataTypes
            for (g in c) {
                if (!i[0] || a.converters[g + " " + i[0]]) {
                    f = g;
                    break;
                }
                if (!d) {
                    d = g;
                }
            }
            // Or just use first one
            f = f || d;
        }
        // If we found a dataType
        // We add the dataType to the list if needed
        // and return the corresponding response
        if (f) {
            if (f !== i[0]) {
                i.unshift(f);
            }
            return c[f];
        }
    }
    /* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
    function Pc(a, b, c, d) {
        var e, f, g, h, i, j = {}, // Work with a copy of dataTypes in case we need to modify it for conversion
        k = a.dataTypes.slice();
        // Create converters map with lowercased keys
        if (k[1]) {
            for (g in a.converters) {
                j[g.toLowerCase()] = a.converters[g];
            }
        }
        f = k.shift();
        // Convert to each sequential dataType
        while (f) {
            if (a.responseFields[f]) {
                c[a.responseFields[f]] = b;
            }
            // Apply the dataFilter if provided
            if (!i && d && a.dataFilter) {
                b = a.dataFilter(b, a.dataType);
            }
            i = f;
            f = k.shift();
            if (f) {
                // There's only work to do if current dataType is non-auto
                if (f === "*") {
                    f = i;
                } else if (i !== "*" && i !== f) {
                    // Seek a direct converter
                    g = j[i + " " + f] || j["* " + f];
                    // If none found, seek a pair
                    if (!g) {
                        for (e in j) {
                            // If conv2 outputs current
                            h = e.split(" ");
                            if (h[1] === f) {
                                // If prev can be converted to accepted input
                                g = j[i + " " + h[0]] || j["* " + h[0]];
                                if (g) {
                                    // Condense equivalence converters
                                    if (g === true) {
                                        g = j[e];
                                    } else if (j[e] !== true) {
                                        f = h[0];
                                        k.unshift(h[1]);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    // Apply converter (if not an equivalence)
                    if (g !== true) {
                        // Unless errors are allowed to bubble, catch and return them
                        if (g && a["throws"]) {
                            b = g(b);
                        } else {
                            try {
                                b = g(b);
                            } catch (l) {
                                return {
                                    state: "parsererror",
                                    error: g ? l : "No conversion from " + i + " to " + f
                                };
                            }
                        }
                    }
                }
            }
        }
        return {
            state: "success",
            data: b
        };
    }
    m.extend({
        // Counter for holding the number of active queries
        active: 0,
        // Last-Modified header cache for next request
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: zc,
            type: "GET",
            isLocal: Dc.test(yc[1]),
            global: true,
            processData: true,
            async: true,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            /*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/
            accepts: {
                "*": Jc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            // Data converters
            // Keys separate source (or catchall "*") and destination types with a single space
            converters: {
                // Convert anything to text
                "* text": String,
                // Text to html (true = no transformation)
                "text html": true,
                // Evaluate text as a json expression
                "text json": m.parseJSON,
                // Parse text as xml
                "text xml": m.parseXML
            },
            // For options that shouldn't be deep extended:
            // you can add your own custom options here if
            // and when you create one that shouldn't be
            // deep extended (see ajaxExtend)
            flatOptions: {
                url: true,
                context: true
            }
        },
        // Creates a full fledged settings object into target
        // with both ajaxSettings and settings fields.
        // If target is omitted, writes into ajaxSettings.
        ajaxSetup: function(a, b) {
            // Building a settings object
            // Extending ajaxSettings
            return b ? Nc(Nc(a, m.ajaxSettings), b) : Nc(m.ajaxSettings, a);
        },
        ajaxPrefilter: Lc(Hc),
        ajaxTransport: Lc(Ic),
        // Main method
        ajax: function(a, b) {
            // If url is an object, simulate pre-1.5 signature
            if (typeof a === "object") {
                b = a;
                a = undefined;
            }
            // Force options to be an object
            b = b || {};
            var // Cross-domain detection vars
            c, // Loop variable
            d, // URL without anti-cache param
            e, // Response headers as string
            f, // timeout handle
            g, // To know if global events are to be dispatched
            h, i, // Response headers
            j, // Create the final options object
            k = m.ajaxSetup({}, b), // Callbacks context
            l = k.context || k, // Context for global events is callbackContext if it is a DOM node or jQuery collection
            n = k.context && (l.nodeType || l.jquery) ? m(l) : m.event, // Deferreds
            o = m.Deferred(), p = m.Callbacks("once memory"), // Status-dependent callbacks
            q = k.statusCode || {}, // Headers (they are sent all at once)
            r = {}, s = {}, // The jqXHR state
            t = 0, // Default abort message
            u = "canceled", // Fake xhr
            v = {
                readyState: 0,
                // Builds headers hashtable if needed
                getResponseHeader: function(a) {
                    var b;
                    if (t === 2) {
                        if (!j) {
                            j = {};
                            while (b = Cc.exec(f)) {
                                j[b[1].toLowerCase()] = b[2];
                            }
                        }
                        b = j[a.toLowerCase()];
                    }
                    return b == null ? null : b;
                },
                // Raw string
                getAllResponseHeaders: function() {
                    return t === 2 ? f : null;
                },
                // Caches the header
                setRequestHeader: function(a, b) {
                    var c = a.toLowerCase();
                    if (!t) {
                        a = s[c] = s[c] || a;
                        r[a] = b;
                    }
                    return this;
                },
                // Overrides response content-type header
                overrideMimeType: function(a) {
                    if (!t) {
                        k.mimeType = a;
                    }
                    return this;
                },
                // Status-dependent callbacks
                statusCode: function(a) {
                    var b;
                    if (a) {
                        if (t < 2) {
                            for (b in a) {
                                // Lazy-add the new callback in a way that preserves old ones
                                q[b] = [ q[b], a[b] ];
                            }
                        } else {
                            // Execute the appropriate callbacks
                            v.always(a[v.status]);
                        }
                    }
                    return this;
                },
                // Cancel the request
                abort: function(a) {
                    var b = a || u;
                    if (i) {
                        i.abort(b);
                    }
                    x(0, b);
                    return this;
                }
            };
            // Attach deferreds
            o.promise(v).complete = p.add;
            v.success = v.done;
            v.error = v.fail;
            // Remove hash character (#7531: and string promotion)
            // Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
            // Handle falsy url in the settings object (#10093: consistency with old signature)
            // We also use the url parameter if available
            k.url = ((a || k.url || zc) + "").replace(Ac, "").replace(Fc, yc[1] + "//");
            // Alias method option to type as per ticket #12004
            k.type = b.method || b.type || k.method || k.type;
            // Extract dataTypes list
            k.dataTypes = m.trim(k.dataType || "*").toLowerCase().match(E) || [ "" ];
            // A cross-domain request is in order when we have a protocol:host:port mismatch
            if (k.crossDomain == null) {
                c = Gc.exec(k.url.toLowerCase());
                k.crossDomain = !!(c && (c[1] !== yc[1] || c[2] !== yc[2] || (c[3] || (c[1] === "http:" ? "80" : "443")) !== (yc[3] || (yc[1] === "http:" ? "80" : "443"))));
            }
            // Convert data if not already a string
            if (k.data && k.processData && typeof k.data !== "string") {
                k.data = m.param(k.data, k.traditional);
            }
            // Apply prefilters
            Mc(Hc, k, b, v);
            // If request was aborted inside a prefilter, stop there
            if (t === 2) {
                return v;
            }
            // We can fire global events as of now if asked to
            h = k.global;
            // Watch for a new set of requests
            if (h && m.active++ === 0) {
                m.event.trigger("ajaxStart");
            }
            // Uppercase the type
            k.type = k.type.toUpperCase();
            // Determine if request has content
            k.hasContent = !Ec.test(k.type);
            // Save the URL in case we're toying with the If-Modified-Since
            // and/or If-None-Match header later on
            e = k.url;
            // More options handling for requests with no content
            if (!k.hasContent) {
                // If data is available, append data to url
                if (k.data) {
                    e = k.url += (wc.test(e) ? "&" : "?") + k.data;
                    // #9682: remove data so that it's not used in an eventual retry
                    delete k.data;
                }
                // Add anti-cache in url if needed
                if (k.cache === false) {
                    k.url = Bc.test(e) ? // If there is already a '_' parameter, set its value
                    e.replace(Bc, "$1_=" + vc++) : // Otherwise add one to the end
                    e + (wc.test(e) ? "&" : "?") + "_=" + vc++;
                }
            }
            // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
            if (k.ifModified) {
                if (m.lastModified[e]) {
                    v.setRequestHeader("If-Modified-Since", m.lastModified[e]);
                }
                if (m.etag[e]) {
                    v.setRequestHeader("If-None-Match", m.etag[e]);
                }
            }
            // Set the correct header, if data is being sent
            if (k.data && k.hasContent && k.contentType !== false || b.contentType) {
                v.setRequestHeader("Content-Type", k.contentType);
            }
            // Set the Accepts header for the server, depending on the dataType
            v.setRequestHeader("Accept", k.dataTypes[0] && k.accepts[k.dataTypes[0]] ? k.accepts[k.dataTypes[0]] + (k.dataTypes[0] !== "*" ? ", " + Jc + "; q=0.01" : "") : k.accepts["*"]);
            // Check for headers option
            for (d in k.headers) {
                v.setRequestHeader(d, k.headers[d]);
            }
            // Allow custom headers/mimetypes and early abort
            if (k.beforeSend && (k.beforeSend.call(l, v, k) === false || t === 2)) {
                // Abort if not done already and return
                return v.abort();
            }
            // aborting is no longer a cancellation
            u = "abort";
            // Install callbacks on deferreds
            for (d in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                v[d](k[d]);
            }
            // Get transport
            i = Mc(Ic, k, b, v);
            // If no transport, we auto-abort
            if (!i) {
                x(-1, "No Transport");
            } else {
                v.readyState = 1;
                // Send global event
                if (h) {
                    n.trigger("ajaxSend", [ v, k ]);
                }
                // Timeout
                if (k.async && k.timeout > 0) {
                    g = setTimeout(function() {
                        v.abort("timeout");
                    }, k.timeout);
                }
                try {
                    t = 1;
                    i.send(r, x);
                } catch (w) {
                    // Propagate exception as error if not done
                    if (t < 2) {
                        x(-1, w);
                    } else {
                        throw w;
                    }
                }
            }
            // Callback for when everything is done
            function x(a, b, c, d) {
                var j, r, s, u, w, x = b;
                // Called once
                if (t === 2) {
                    return;
                }
                // State is "done" now
                t = 2;
                // Clear timeout if it exists
                if (g) {
                    clearTimeout(g);
                }
                // Dereference transport for early garbage collection
                // (no matter how long the jqXHR object will be used)
                i = undefined;
                // Cache response headers
                f = d || "";
                // Set readyState
                v.readyState = a > 0 ? 4 : 0;
                // Determine if successful
                j = a >= 200 && a < 300 || a === 304;
                // Get response data
                if (c) {
                    u = Oc(k, v, c);
                }
                // Convert no matter what (that way responseXXX fields are always set)
                u = Pc(k, u, v, j);
                // If successful, handle type chaining
                if (j) {
                    // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
                    if (k.ifModified) {
                        w = v.getResponseHeader("Last-Modified");
                        if (w) {
                            m.lastModified[e] = w;
                        }
                        w = v.getResponseHeader("etag");
                        if (w) {
                            m.etag[e] = w;
                        }
                    }
                    // if no content
                    if (a === 204 || k.type === "HEAD") {
                        x = "nocontent";
                    } else if (a === 304) {
                        x = "notmodified";
                    } else {
                        x = u.state;
                        r = u.data;
                        s = u.error;
                        j = !s;
                    }
                } else {
                    // We extract error from statusText
                    // then normalize statusText and status for non-aborts
                    s = x;
                    if (a || !x) {
                        x = "error";
                        if (a < 0) {
                            a = 0;
                        }
                    }
                }
                // Set data for the fake xhr object
                v.status = a;
                v.statusText = (b || x) + "";
                // Success/Error
                if (j) {
                    o.resolveWith(l, [ r, x, v ]);
                } else {
                    o.rejectWith(l, [ v, x, s ]);
                }
                // Status-dependent callbacks
                v.statusCode(q);
                q = undefined;
                if (h) {
                    n.trigger(j ? "ajaxSuccess" : "ajaxError", [ v, k, j ? r : s ]);
                }
                // Complete
                p.fireWith(l, [ v, x ]);
                if (h) {
                    n.trigger("ajaxComplete", [ v, k ]);
                    // Handle the global AJAX counter
                    if (!--m.active) {
                        m.event.trigger("ajaxStop");
                    }
                }
            }
            return v;
        },
        getJSON: function(a, b, c) {
            return m.get(a, b, c, "json");
        },
        getScript: function(a, b) {
            return m.get(a, undefined, b, "script");
        }
    });
    m.each([ "get", "post" ], function(a, b) {
        m[b] = function(a, c, d, e) {
            // shift arguments if data argument was omitted
            if (m.isFunction(c)) {
                e = e || d;
                d = c;
                c = undefined;
            }
            return m.ajax({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            });
        };
    });
    // Attach a bunch of functions for handling common AJAX events
    m.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(a, b) {
        m.fn[b] = function(a) {
            return this.on(b, a);
        };
    });
    m._evalUrl = function(a) {
        return m.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            async: false,
            global: false,
            "throws": true
        });
    };
    m.fn.extend({
        wrapAll: function(a) {
            if (m.isFunction(a)) {
                return this.each(function(b) {
                    m(this).wrapAll(a.call(this, b));
                });
            }
            if (this[0]) {
                // The elements to wrap the target around
                var b = m(a, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    b.insertBefore(this[0]);
                }
                b.map(function() {
                    var a = this;
                    while (a.firstChild && a.firstChild.nodeType === 1) {
                        a = a.firstChild;
                    }
                    return a;
                }).append(this);
            }
            return this;
        },
        wrapInner: function(a) {
            if (m.isFunction(a)) {
                return this.each(function(b) {
                    m(this).wrapInner(a.call(this, b));
                });
            }
            return this.each(function() {
                var b = m(this), c = b.contents();
                if (c.length) {
                    c.wrapAll(a);
                } else {
                    b.append(a);
                }
            });
        },
        wrap: function(a) {
            var b = m.isFunction(a);
            return this.each(function(c) {
                m(this).wrapAll(b ? a.call(this, c) : a);
            });
        },
        unwrap: function() {
            return this.parent().each(function() {
                if (!m.nodeName(this, "body")) {
                    m(this).replaceWith(this.childNodes);
                }
            }).end();
        }
    });
    m.expr.filters.hidden = function(a) {
        // Support: Opera <= 12.12
        // Opera reports offsetWidths and offsetHeights less than zero on some elements
        return a.offsetWidth <= 0 && a.offsetHeight <= 0 || !k.reliableHiddenOffsets() && (a.style && a.style.display || m.css(a, "display")) === "none";
    };
    m.expr.filters.visible = function(a) {
        return !m.expr.filters.hidden(a);
    };
    var Qc = /%20/g, Rc = /\[\]$/, Sc = /\r?\n/g, Tc = /^(?:submit|button|image|reset|file)$/i, Uc = /^(?:input|select|textarea|keygen)/i;
    function Vc(a, b, c, d) {
        var e;
        if (m.isArray(b)) {
            // Serialize array item.
            m.each(b, function(b, e) {
                if (c || Rc.test(a)) {
                    // Treat each array item as a scalar.
                    d(a, e);
                } else {
                    // Item is non-scalar (array or object), encode its numeric index.
                    Vc(a + "[" + (typeof e === "object" ? b : "") + "]", e, c, d);
                }
            });
        } else if (!c && m.type(b) === "object") {
            // Serialize object item.
            for (e in b) {
                Vc(a + "[" + e + "]", b[e], c, d);
            }
        } else {
            // Serialize scalar item.
            d(a, b);
        }
    }
    // Serialize an array of form elements or a set of
    // key/values into a query string
    m.param = function(a, b) {
        var c, d = [], e = function(a, b) {
            // If value is a function, invoke it and return its value
            b = m.isFunction(b) ? b() : b == null ? "" : b;
            d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b);
        };
        // Set traditional to true for jQuery <= 1.3.2 behavior.
        if (b === undefined) {
            b = m.ajaxSettings && m.ajaxSettings.traditional;
        }
        // If an array was passed in, assume that it is an array of form elements.
        if (m.isArray(a) || a.jquery && !m.isPlainObject(a)) {
            // Serialize the form elements
            m.each(a, function() {
                e(this.name, this.value);
            });
        } else {
            // If traditional, encode the "old" way (the way 1.3.2 or older
            // did it), otherwise encode params recursively.
            for (c in a) {
                Vc(c, a[c], b, e);
            }
        }
        // Return the resulting serialization
        return d.join("&").replace(Qc, "+");
    };
    m.fn.extend({
        serialize: function() {
            return m.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                // Can add propHook for "elements" to filter or add form elements
                var a = m.prop(this, "elements");
                return a ? m.makeArray(a) : this;
            }).filter(function() {
                var a = this.type;
                // Use .is(":disabled") so that fieldset[disabled] works
                return this.name && !m(this).is(":disabled") && Uc.test(this.nodeName) && !Tc.test(a) && (this.checked || !W.test(a));
            }).map(function(a, b) {
                var c = m(this).val();
                return c == null ? null : m.isArray(c) ? m.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(Sc, "\r\n")
                    };
                }) : {
                    name: b.name,
                    value: c.replace(Sc, "\r\n")
                };
            }).get();
        }
    });
    // Create the request object
    // (This is still attached to ajaxSettings for backward compatibility)
    m.ajaxSettings.xhr = a.ActiveXObject !== undefined ? // Support: IE6+
    function() {
        // XHR cannot access local files, always use ActiveX for that case
        // Support: IE7-8
        // oldIE XHR does not support non-RFC2616 methods (#13240)
        // See http://msdn.microsoft.com/en-us/library/ie/ms536648(v=vs.85).aspx
        // and http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9
        // Although this check for six methods instead of eight
        // since IE also does not support "trace" and "connect"
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && Zc() || $c();
    } : // For all other browsers, use the standard XMLHttpRequest object
    Zc;
    var Wc = 0, Xc = {}, Yc = m.ajaxSettings.xhr();
    // Support: IE<10
    // Open requests must be manually aborted on unload (#5280)
    if (a.ActiveXObject) {
        m(a).on("unload", function() {
            for (var a in Xc) {
                Xc[a](undefined, true);
            }
        });
    }
    // Determine support properties
    k.cors = !!Yc && "withCredentials" in Yc;
    Yc = k.ajax = !!Yc;
    // Create transport if the browser can provide an xhr
    if (Yc) {
        m.ajaxTransport(function(a) {
            // Cross domain only allowed if supported through XMLHttpRequest
            if (!a.crossDomain || k.cors) {
                var b;
                return {
                    send: function(c, d) {
                        var e, f = a.xhr(), g = ++Wc;
                        // Open the socket
                        f.open(a.type, a.url, a.async, a.username, a.password);
                        // Apply custom fields if provided
                        if (a.xhrFields) {
                            for (e in a.xhrFields) {
                                f[e] = a.xhrFields[e];
                            }
                        }
                        // Override mime type if needed
                        if (a.mimeType && f.overrideMimeType) {
                            f.overrideMimeType(a.mimeType);
                        }
                        // X-Requested-With header
                        // For cross-domain requests, seeing as conditions for a preflight are
                        // akin to a jigsaw puzzle, we simply never set it to be sure.
                        // (it can always be set on a per-request basis or even using ajaxSetup)
                        // For same-domain requests, won't change header if already provided.
                        if (!a.crossDomain && !c["X-Requested-With"]) {
                            c["X-Requested-With"] = "XMLHttpRequest";
                        }
                        // Set headers
                        for (e in c) {
                            // Support: IE<9
                            // IE's ActiveXObject throws a 'Type Mismatch' exception when setting
                            // request header to a null-value.
                            //
                            // To keep consistent with other XHR implementations, cast the value
                            // to string and ignore `undefined`.
                            if (c[e] !== undefined) {
                                f.setRequestHeader(e, c[e] + "");
                            }
                        }
                        // Do send the request
                        // This may raise an exception which is actually
                        // handled in jQuery.ajax (so no try/catch here)
                        f.send(a.hasContent && a.data || null);
                        // Listener
                        b = function(c, e) {
                            var h, i, j;
                            // Was never called and is aborted or complete
                            if (b && (e || f.readyState === 4)) {
                                // Clean up
                                delete Xc[g];
                                b = undefined;
                                f.onreadystatechange = m.noop;
                                // Abort manually if needed
                                if (e) {
                                    if (f.readyState !== 4) {
                                        f.abort();
                                    }
                                } else {
                                    j = {};
                                    h = f.status;
                                    // Support: IE<10
                                    // Accessing binary-data responseText throws an exception
                                    // (#11426)
                                    if (typeof f.responseText === "string") {
                                        j.text = f.responseText;
                                    }
                                    // Firefox throws an exception when accessing
                                    // statusText for faulty cross-domain requests
                                    try {
                                        i = f.statusText;
                                    } catch (k) {
                                        // We normalize with Webkit giving an empty statusText
                                        i = "";
                                    }
                                    // Filter status for non standard behaviors
                                    // If the request is local and we have data: assume a success
                                    // (success with no data won't get notified, that's the best we
                                    // can do given current implementations)
                                    if (!h && a.isLocal && !a.crossDomain) {
                                        h = j.text ? 200 : 404;
                                    } else if (h === 1223) {
                                        h = 204;
                                    }
                                }
                            }
                            // Call complete if needed
                            if (j) {
                                d(h, i, j, f.getAllResponseHeaders());
                            }
                        };
                        if (!a.async) {
                            // if we're in sync mode we fire the callback
                            b();
                        } else if (f.readyState === 4) {
                            // (IE6 & IE7) if it's in cache and has been
                            // retrieved directly we need to fire the callback
                            setTimeout(b);
                        } else {
                            // Add to the list of active xhr callbacks
                            f.onreadystatechange = Xc[g] = b;
                        }
                    },
                    abort: function() {
                        if (b) {
                            b(undefined, true);
                        }
                    }
                };
            }
        });
    }
    // Functions to create xhrs
    function Zc() {
        try {
            return new a.XMLHttpRequest();
        } catch (b) {}
    }
    function $c() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP");
        } catch (b) {}
    }
    // Install script dataType
    m.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(a) {
                m.globalEval(a);
                return a;
            }
        }
    });
    // Handle cache's special case and global
    m.ajaxPrefilter("script", function(a) {
        if (a.cache === undefined) {
            a.cache = false;
        }
        if (a.crossDomain) {
            a.type = "GET";
            a.global = false;
        }
    });
    // Bind script tag hack transport
    m.ajaxTransport("script", function(a) {
        // This transport only deals with cross domain requests
        if (a.crossDomain) {
            var b, c = y.head || m("head")[0] || y.documentElement;
            return {
                send: function(d, e) {
                    b = y.createElement("script");
                    b.async = true;
                    if (a.scriptCharset) {
                        b.charset = a.scriptCharset;
                    }
                    b.src = a.url;
                    // Attach handlers for all browsers
                    b.onload = b.onreadystatechange = function(a, c) {
                        if (c || !b.readyState || /loaded|complete/.test(b.readyState)) {
                            // Handle memory leak in IE
                            b.onload = b.onreadystatechange = null;
                            // Remove the script
                            if (b.parentNode) {
                                b.parentNode.removeChild(b);
                            }
                            // Dereference the script
                            b = null;
                            // Callback if not abort
                            if (!c) {
                                e(200, "success");
                            }
                        }
                    };
                    // Circumvent IE6 bugs with base elements (#2709 and #4378) by prepending
                    // Use native DOM manipulation to avoid our domManip AJAX trickery
                    c.insertBefore(b, c.firstChild);
                },
                abort: function() {
                    if (b) {
                        b.onload(undefined, true);
                    }
                }
            };
        }
    });
    var _c = [], ad = /(=)\?(?=&|$)|\?\?/;
    // Default jsonp settings
    m.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = _c.pop() || m.expando + "_" + vc++;
            this[a] = true;
            return a;
        }
    });
    // Detect, normalize options and install callbacks for jsonp requests
    m.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== false && (ad.test(b.url) ? "url" : typeof b.data === "string" && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && ad.test(b.data) && "data");
        // Handle iff the expected data type is "jsonp" or we have a parameter to set
        if (h || b.dataTypes[0] === "jsonp") {
            // Get callback name, remembering preexisting value associated with it
            e = b.jsonpCallback = m.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback;
            // Insert callback into url or form data
            if (h) {
                b[h] = b[h].replace(ad, "$1" + e);
            } else if (b.jsonp !== false) {
                b.url += (wc.test(b.url) ? "&" : "?") + b.jsonp + "=" + e;
            }
            // Use data converter to retrieve json after script execution
            b.converters["script json"] = function() {
                if (!g) {
                    m.error(e + " was not called");
                }
                return g[0];
            };
            // force json dataType
            b.dataTypes[0] = "json";
            // Install callback
            f = a[e];
            a[e] = function() {
                g = arguments;
            };
            // Clean-up function (fires after converters)
            d.always(function() {
                // Restore preexisting value
                a[e] = f;
                // Save back as free
                if (b[e]) {
                    // make sure that re-using the options doesn't screw things around
                    b.jsonpCallback = c.jsonpCallback;
                    // save the callback name for future use
                    _c.push(e);
                }
                // Call if it was a function and we have a response
                if (g && m.isFunction(f)) {
                    f(g[0]);
                }
                g = f = undefined;
            });
            // Delegate to script
            return "script";
        }
    });
    // data: string of html
    // context (optional): If specified, the fragment will be created in this context, defaults to document
    // keepScripts (optional): If true, will include scripts passed in the html string
    m.parseHTML = function(a, b, c) {
        if (!a || typeof a !== "string") {
            return null;
        }
        if (typeof b === "boolean") {
            c = b;
            b = false;
        }
        b = b || y;
        var d = u.exec(a), e = !c && [];
        // Single tag
        if (d) {
            return [ b.createElement(d[1]) ];
        }
        d = m.buildFragment([ a ], b, e);
        if (e && e.length) {
            m(e).remove();
        }
        return m.merge([], d.childNodes);
    };
    // Keep a copy of the old load method
    var bd = m.fn.load;
    /**
 * Load a url into a page
 */
    m.fn.load = function(a, b, c) {
        if (typeof a !== "string" && bd) {
            return bd.apply(this, arguments);
        }
        var d, e, f, g = this, h = a.indexOf(" ");
        if (h >= 0) {
            d = m.trim(a.slice(h, a.length));
            a = a.slice(0, h);
        }
        // If it's a function
        if (m.isFunction(b)) {
            // We assume that it's the callback
            c = b;
            b = undefined;
        } else if (b && typeof b === "object") {
            f = "POST";
        }
        // If we have elements to modify, make the request
        if (g.length > 0) {
            m.ajax({
                url: a,
                // if "type" variable is undefined, then "GET" method will be used
                type: f,
                dataType: "html",
                data: b
            }).done(function(a) {
                // Save response for use in complete callback
                e = arguments;
                g.html(d ? // If a selector was specified, locate the right elements in a dummy div
                // Exclude scripts to avoid IE 'Permission Denied' errors
                m("<div>").append(m.parseHTML(a)).find(d) : // Otherwise use the full result
                a);
            }).complete(c && function(a, b) {
                g.each(c, e || [ a.responseText, b, a ]);
            });
        }
        return this;
    };
    m.expr.filters.animated = function(a) {
        return m.grep(m.timers, function(b) {
            return a === b.elem;
        }).length;
    };
    var cd = a.document.documentElement;
    /**
 * Gets a window from an element
 */
    function dd(a) {
        return m.isWindow(a) ? a : a.nodeType === 9 ? a.defaultView || a.parentWindow : false;
    }
    m.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = m.css(a, "position"), l = m(a), n = {};
            // set position first, in-case top/left are set even on static elem
            if (k === "static") {
                a.style.position = "relative";
            }
            h = l.offset();
            f = m.css(a, "top");
            i = m.css(a, "left");
            j = (k === "absolute" || k === "fixed") && m.inArray("auto", [ f, i ]) > -1;
            // need to be able to calculate position if either top or left is auto and position is either absolute or fixed
            if (j) {
                d = l.position();
                g = d.top;
                e = d.left;
            } else {
                g = parseFloat(f) || 0;
                e = parseFloat(i) || 0;
            }
            if (m.isFunction(b)) {
                b = b.call(a, c, h);
            }
            if (b.top != null) {
                n.top = b.top - h.top + g;
            }
            if (b.left != null) {
                n.left = b.left - h.left + e;
            }
            if ("using" in b) {
                b.using.call(a, n);
            } else {
                l.css(n);
            }
        }
    };
    m.fn.extend({
        offset: function(a) {
            if (arguments.length) {
                return a === undefined ? this : this.each(function(b) {
                    m.offset.setOffset(this, a, b);
                });
            }
            var b, c, d = {
                top: 0,
                left: 0
            }, e = this[0], f = e && e.ownerDocument;
            if (!f) {
                return;
            }
            b = f.documentElement;
            // Make sure it's not a disconnected DOM node
            if (!m.contains(b, e)) {
                return d;
            }
            // If we don't have gBCR, just use 0,0 rather than error
            // BlackBerry 5, iOS 3 (original iPhone)
            if (typeof e.getBoundingClientRect !== K) {
                d = e.getBoundingClientRect();
            }
            c = dd(f);
            return {
                top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
                left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
            };
        },
        position: function() {
            if (!this[0]) {
                return;
            }
            var a, b, c = {
                top: 0,
                left: 0
            }, d = this[0];
            // fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
            if (m.css(d, "position") === "fixed") {
                // we assume that getBoundingClientRect is available when computed position is fixed
                b = d.getBoundingClientRect();
            } else {
                // Get *real* offsetParent
                a = this.offsetParent();
                // Get correct offsets
                b = this.offset();
                if (!m.nodeName(a[0], "html")) {
                    c = a.offset();
                }
                // Add offsetParent borders
                c.top += m.css(a[0], "borderTopWidth", true);
                c.left += m.css(a[0], "borderLeftWidth", true);
            }
            // Subtract parent offsets and element margins
            // note: when an element has margin: auto the offsetLeft and marginLeft
            // are the same in Safari causing offset.left to incorrectly be 0
            return {
                top: b.top - c.top - m.css(d, "marginTop", true),
                left: b.left - c.left - m.css(d, "marginLeft", true)
            };
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent || cd;
                while (a && (!m.nodeName(a, "html") && m.css(a, "position") === "static")) {
                    a = a.offsetParent;
                }
                return a || cd;
            });
        }
    });
    // Create scrollLeft and scrollTop methods
    m.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = /Y/.test(b);
        m.fn[a] = function(d) {
            return V(this, function(a, d, e) {
                var f = dd(a);
                if (e === undefined) {
                    return f ? b in f ? f[b] : f.document.documentElement[d] : a[d];
                }
                if (f) {
                    f.scrollTo(!c ? e : m(f).scrollLeft(), c ? e : m(f).scrollTop());
                } else {
                    a[d] = e;
                }
            }, a, d, arguments.length, null);
        };
    });
    // Add the top/left cssHooks using jQuery.fn.position
    // Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
    // getComputedStyle returns percent when specified for top/left/bottom/right
    // rather than make the css module depend on the offset module, we just check for it here
    m.each([ "top", "left" ], function(a, b) {
        m.cssHooks[b] = Lb(k.pixelPosition, function(a, c) {
            if (c) {
                c = Jb(a, b);
                // if curCSS returns percentage, fallback to offset
                return Hb.test(c) ? m(a).position()[b] + "px" : c;
            }
        });
    });
    // Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
    m.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        m.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            // margin is only for outerHeight, outerWidth
            m.fn[d] = function(d, e) {
                var f = arguments.length && (c || typeof d !== "boolean"), g = c || (d === true || e === true ? "margin" : "border");
                return V(this, function(b, c, d) {
                    var e;
                    if (m.isWindow(b)) {
                        // As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
                        // isn't a whole lot we can do. See pull request at this URL for discussion:
                        // https://github.com/jquery/jquery/pull/764
                        return b.document.documentElement["client" + a];
                    }
                    // Get document width or height
                    if (b.nodeType === 9) {
                        e = b.documentElement;
                        // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
                        // unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
                        return Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a]);
                    }
                    // Get width or height on the element, requesting but not forcing parseFloat
                    // Set width or height on the element
                    return d === undefined ? m.css(b, c, g) : m.style(b, c, d, g);
                }, b, f ? d : undefined, f, null);
            };
        });
    });
    // The number of elements contained in the matched element set
    m.fn.size = function() {
        return this.length;
    };
    m.fn.andSelf = m.fn.addBack;
    // Register as a named AMD module, since jQuery can be concatenated with other
    // files that may use define, but not via a proper concatenation script that
    // understands anonymous AMD modules. A named AMD is safest and most robust
    // way to register. Lowercase jquery is used because AMD module names are
    // derived from file names, and jQuery is normally delivered in a lowercase
    // file name. Do this after creating the global so that if an AMD module wants
    // to call noConflict to hide this version of jQuery, it will work.
    // Note that for maximum portability, libraries that are not jQuery should
    // declare themselves as anonymous modules, and avoid setting a global if an
    // AMD loader is present. jQuery is a special case. For more information, see
    // https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon
    if (typeof define === "function" && define.amd) {
        define("jquery", [], function() {
            return m;
        });
    }
    var // Map over jQuery in case of overwrite
    ed = a.jQuery, // Map over the $ in case of overwrite
    fd = a.$;
    m.noConflict = function(b) {
        if (a.$ === m) {
            a.$ = fd;
        }
        if (b && a.jQuery === m) {
            a.jQuery = ed;
        }
        return m;
    };
    // Expose jQuery and $ identifiers, even in
    // AMD (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
    // and CommonJS for browser emulators (#13566)
    if (typeof b === K) {
        a.jQuery = a.$ = m;
    }
    return m;
});