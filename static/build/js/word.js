/*!
 * ------/ Source: js/directives/create.js
 */
(function(a, b, c) {
    "use strict";
    c.directive("create", [ "utility", function(c) {
        return {
            restrict: "A",
            scope: true,
            link: function(d, e) {
                var f = b(e).find("form");
                d.suggestions = [];
                function g(e) {
                    var f = {
                        word: b("#word").val(),
                        file: e,
                        filter: b("#filter").val()
                    };
                    b.post("/create", f, function(b) {
                        loader.hide();
                        if (b.success) {
                            d.$parent.emitMessage("word-created");
                            setTimeout(function() {
                                window.location = "/";
                            }, 200);
                            return;
                        }
                        if (a.isArray(b.data)) {
                            d.suggestions = b.data;
                            c.safeApply(d);
                            return;
                        }
                        alert(b.data);
                    });
                }
                function h() {
                    loader.show();
                    var a = b("#asset")[0].files;
                    if (!a.length) {
                        return alert("Please upload an image.");
                    }
                    var c = new FileReader();
                    c.onload = function(a) {
                        g(a.target.result);
                    };
                    c.readAsDataURL(a[0]);
                }
                f.on("submit", function(a) {
                    a.preventDefault();
                    h();
                });
                d.shim = function(a, c) {
                    a.preventDefault();
                    b("#word").val(c);
                    h();
                };
            }
        };
    } ]);
})(angular, jQuery, Word);

//! END
/*!
 * ------/ Source: js/directives/feed.js
 */
(function(a, b, c) {
    "use strict";
    c.directive("feed", [ "$timeout", "utility", function(a, c) {
        return {
            restrict: "A",
            scope: true,
            link: function(d, e, f) {
                d.posts = [];
                d.loaded = false;
                d.user_id = b(e).attr("user-id");
                d.count = 0;
                d.$parent.onMessage = function(a) {
                    if (window.location.pathname !== "/") {
                        return;
                    }
                    a = JSON.parse(a);
                    d.count += parseInt(a.new);
                    c.safeApply(d);
                    window.document.title = "(" + d.count + ") Feed | Word";
                    if (typeof Audio !== undefined) {
                        var b = new Audio("/img/music_marimba_chord.wav");
                        b.play();
                    }
                };
                d.refresh = function() {
                    loader.show();
                    b.get("/feed", function(a) {
                        if (a.success) {
                            d.posts = a.data;
                        }
                        d.loaded = true;
                        c.safeApply(d);
                        loader.hide();
                    });
                };
                d.deletePost = function(a) {
                    var e = confirm("Are you sure?");
                    if (!e) {
                        return;
                    }
                    loader.show();
                    b.get("/delete/" + a, function(a) {
                        if (a.success) {
                            d.refresh();
                        }
                        c.safeApply(d);
                        loader.hide();
                    });
                };
                d.play = function(a, b) {
                    a.preventDefault();
                    if (typeof Audio === undefined) {
                        return alert("This browser does not support audio.");
                    }
                    if (d.mute) {
                        return;
                    }
                    var c = new Audio(b);
                    c.play();
                };
                a(d.refresh, 10);
            }
        };
    } ]);
})(angular, jQuery, Word);

//! END
/*!
 * ------/ Source: js/directives/login.js
 */
(function(a, b, c) {
    "use strict";
    c.directive("login", [ function() {
        return {
            restrict: "A",
            scope: true,
            link: function(a, c) {
                var d = b(c).find("form");
                function e() {
                    loader.show();
                    var a = {
                        email: d.find(".email").val(),
                        password: d.find(".password").val()
                    };
                    b.post("/login", a, function(a) {
                        loader.hide();
                        if (a.success) {
                            window.location = "/";
                        } else {
                            alert(a.data);
                        }
                    });
                }
                d.on("submit", function(a) {
                    a.preventDefault();
                    e();
                });
            }
        };
    } ]);
})(angular, jQuery, Word);

//! END
/*!
 * ------/ Source: js/directives/register.js
 */
(function(a, b, c) {
    "use strict";
    c.directive("register", [ function() {
        return {
            restrict: "A",
            scope: true,
            link: function(a, c) {
                var d = b(c).find("form");
                function e() {
                    loader.show();
                    var a = {
                        email: d.find(".email").val(),
                        password: d.find(".password").val(),
                        confirm: d.find(".password-confirmation").val()
                    };
                    b.post("/register", a, function(a) {
                        loader.hide();
                        if (a.success) {
                            window.location = "/";
                        } else {
                            alert(a.data);
                        }
                    });
                }
                d.on("submit", function(a) {
                    a.preventDefault();
                    e();
                });
            }
        };
    } ]);
})(angular, jQuery, Word);

//! END
/*!
 * ------/ Source: js/controllers/WordController.js
 */
(function(a, b) {
    "use strict";
    b.controller("WordController", [ "$scope", "$http", function(a, b) {
        if (typeof WebSocket !== "undefined") {
            var c;
            var d = function() {
                c = new WebSocket($('meta[name="socket"]').attr("content"));
                c.onmessage = function(b) {
                    if (typeof a.onMessage === "function") {
                        a.onMessage(b.data);
                    }
                };
                c.onclose = function() {
                    setTimeout(d, 200);
                };
            };
            d();
        }
        a.emitMessage = function(a) {
            if (c === undefined) {
                return;
            }
            c.send(JSON.stringify(a));
        };
    } ]);
})(angular, Word);

//! END
/*!
 * ------/ Source: js/services/utility.js
 */
(function(a, b) {
    "use strict";
    b.service("utility", function() {
        this.safeApply = function(a) {
            var b = a.$root.$$phase;
            if (b !== "$apply" && b !== "$digest") {
                a.$apply();
            }
        };
    });
})(angular, Word);