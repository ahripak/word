<?php

return array(
	'base'       => 'http://www.dictionaryapi.com/api/v1/references/collegiate/xml',
	'media_base' => 'http://media.merriam-webster.com/soundc11',
	'dictionary' => 'b492b10a-0ac0-4ab4-91ee-d9754af2531f',
	'thesaurus'  => '4dabbce0-d56e-4c81-84e5-7750271abac1',
);
