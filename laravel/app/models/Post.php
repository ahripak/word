<?php

class Post extends Eloquent {

	protected $filable = array(
		'user_id',
		'is_deleted',
		'word',
		'pronounciation',
		'sound',
		'asset',
		'created_at',
		'updated_at',
	);

	public function user()
	{
		return $this->belongsTo('user');
	}

	public function softDelete()
	{
		$this->is_deleted = true;
		return $this->save();
	}
}
