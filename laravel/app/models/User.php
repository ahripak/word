<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

	use UserTrait;

	protected $appends = array(
		'gravatar',
	);

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array(
		'password',
	);


	public function getGravatarAttribute()
	{
		if (! is_null($this->email)) {
			return 'http://www.gravatar.com/avatar/' . md5(strtolower($this->email));
		}
		return null;
	}

}
