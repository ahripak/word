<?php

use Illuminate\Console\Command;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class WordManagerCommand extends Command
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'word:listen';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Opens a Ratchet connection and handles listening for messages.';

	/**
	 * The socket server.
	 * @var [type]
	 */
	protected $server;

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->server = IoServer::factory(
			new HttpServer(
				new WsServer(
					new ConnectionManager($this)
				)
			),
			8080
		);

		$this->server->run();
	}

}
