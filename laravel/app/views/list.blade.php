<div feed class="feed" user-id="{{ $user_id }}">
	<div class="alert alert-success" ng-show="count">
		[[ count ]] new posts, <a href="/">refresh the feed.</a>
	</div>
	<div ng-show="posts && posts.length">
		<div class="col-sm-4" ng-repeat="post in posts">
			<div class="post-container">
				<img ng-src="[[ post.asset ]]" />
				<div class="post-actions">
					<a href="#" ng-click="play($event, post.sound)" class="fa fa-play fa-2x play"></a>
				</div>
				<a href="#" ng-if="user_id == post.user_id" ng-click="deletePost(post.id)" class="fa fa-remove fa-2x remove"></a>
				<img ng-src="[[ post.user.gravatar ]]" class="gravatar" />
			</div>
		</div>
	</div>
	<div ng-show="posts.length === 0 && loaded">
		No posts here.
	</div>
</div>