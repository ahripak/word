<div class="col-sm-8 center-no-float" register>
	<form class="form-signin" role="form">
		<h3 class="form-signin-heading">Register</h3>
		<input type="email" class="form-control email" placeholder="Email" required autofocus>
		<input type="password" class="form-control password" placeholder="Password" required>
		<input type="password" class="form-control password-confirmation" placeholder="Password Confirmation" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Make My Account</button>
	</form>
</div>