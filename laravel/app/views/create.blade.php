<div class="col-sm-8 center-no-float" create>
	<form class="form-create" role="form">
		<div class="form-group">
			<label for="word">One word to describe the picture:</label>
			<input type="textfield" class="form-control" id="word" placeholder="i.e. {{ $word }}">
		</div>
		<div ng-show="suggestions.length" class="form-group">
			<label for="word">Did you mean one of the following?</label>
			<div class="btn-group suggestions">
				<button class="btn btn-default" ng-repeat="suggestion in suggestions" ng-click="shim($event, suggestion)">[[ suggestion ]]</button>
			</div>
		</div>
		<div class="form-group">
			<label for="asset">Upload a photo:</label>
			<input type="file" class="form-control" id="asset">
		</div>
		<div class="form-group">
			<label for="filter">Choose a filter:</label>
			<select id="filter" class="form-control">
				<option selected value="TiltShift">TiltShift</option>
				<option value="Gotham">Gotham</option>
				<option value="Nashville">Nashville</option>
				<option value="Lomo">Lomo</option>
				<option value="Kelvin">Kelvin</option>
				<option value="none">None</option>
			</select>
		</div>
		<button type="submit" class="btn btn-primary btn-block">Save</button>
	</form>
</div>