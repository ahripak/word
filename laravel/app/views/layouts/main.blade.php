<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="socket" content="ws://localhost:8080">
		<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, target-densitydpi=device-dpi" />
		<title>{{ $title }}</title>
		{{ HTML::script('js/lib/angular.js')}}
		{{ HTML::script('js/lib/jquery.js')}}

		{{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}
		{{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css') }}
		{{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') }}

		{{ HTML::style('css/screen.css') }}
		{{ HTML::script('js/app.js') }}
		{{ HTML::script('js/word.js') }}
	</head>
	<body>
		<div ng-controller="WordController">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">Word</a>
					</div>
					<div class="loading"></div>
					@if ($authed)
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="/">Feed</a>
								</li>
								<li>
									<a href="/create">Create</a>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="/logout">Logout</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					@else
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="/login">Log In</a>
								</li>
								<li>
									<a href="/register">Register</a>
								</li>
							</ul>
						</div>
					@endif
				</div>
			</div>
			<div class="container">
				{{ $content }}
			</div>
		</div>
	</body>
</html>