<div class="col-sm-8 center-no-float" login>
	<form class="form-signin" role="form">
		<h3 class="form-signin-heading">Log In</h3>
		<input type="email" class="form-control email" placeholder="Email" required autofocus>
		<input type="password" class="form-control password" placeholder="Password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Punch It</button>
	</form>
</div>