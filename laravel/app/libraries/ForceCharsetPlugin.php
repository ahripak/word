<?php

use GuzzleHttp\Event\EmitterInterface;
use GuzzleHttp\Event\SubscriberInterface;
use GuzzleHttp\Event\BeforeEvent;
use GuzzleHttp\Event\CompleteEvent;

class ForceCharsetPlugin implements SubscriberInterface
{
	private $forcedCharset = 'utf8';

	public function getEvents()
	{
		return array(
			'complete' => array(
				'onComplete',
			),
		);
	}

	public function setForcedCharset($charset)
	{
		$this->forcedCharset = $charset;

		return $this;
	}

	public function getForcedCharset()
	{
		return $this->forcedCharset;
	}

	public function onComplete(CompleteEvent $event)
	{
		$response = $event->getResponse();
		$header = $response->getHeader('content-type');

		$modified = $header . '; charset=utf-8';

		$response->setHeader('content-type', $modified);
	}
}
