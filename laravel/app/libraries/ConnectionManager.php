<?php

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Illuminate\Console\Command;

class ConnectionManager implements MessageComponentInterface
{
	/**
	 * In memory storage for the connected clients.
	 * @var SplObjectStorage
	 */
	protected $clients;

	/**
	 * Holds the parent class for logging purposes.
	 * @var Illuminate\Console\Command
	 */
	protected $command;

	public function __construct(Command $command)
	{
		$this->clients   = new SplObjectStorage;
		$this->command   = $command;
	}

	public function onOpen(ConnectionInterface $connection)
	{
		$this->clients->attach($connection);
		$this->command->info('New Connection ID: ' . $connection->resourceId);
	}

	public function onClose(ConnectionInterface $connection)
	{
		$this->clients->detach($connection);
		$this->command->info('Connection Dropped ID: ' . $connection->resourceId);
	}

	public function onError(ConnectionInterface $connection, Exception $e)
	{
		$this->command->error($e->getMessage());
		$connection->close();
	}

	public function onMessage(ConnectionInterface $from, $message)
	{
		$this->command->info('Payload received: ' . json_encode($message));

		foreach ($this->clients as $client) {
			$client->send(json_encode(array('new' => 1)));
		}
	}

}
