<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'PostsController@listView');
Route::get('feed', 'PostsController@feed');
Route::get('login', 'UsersController@login');
Route::post('login', 'UsersController@login');
Route::get('register', 'UsersController@register');
Route::post('register', 'UsersController@register');
Route::get('logout', 'UsersController@logout');
Route::get('create', 'PostsController@createView');
Route::post('create', 'PostsController@create');
Route::get('delete/{id?}', 'PostsController@delete');