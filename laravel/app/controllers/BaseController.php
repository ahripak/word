<?php

class BaseController extends Controller {

	public function __construct()
	{
		$this->layout = 'layouts/main';
	}

	public function renderView($title = null, $content = '')
	{
		$views = array(
			'content' => $content
		);

		View::share('title', $title . ' | Word');
		View::share('authed', Auth::check());

		$return = View::make($this->layout)
			->with($views);

		return Response::make($return, 200);
	}

	public function renderJson($success = true, $data = null)
	{
		return Response::json(array(
			'success' => (bool) $success,
			'data'    => $data,
		));
	}

}
