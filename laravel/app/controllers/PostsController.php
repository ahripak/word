<?php

class PostsController extends BaseController {

	public function createView()
	{
		if (! Auth::check()) {
			return Redirect::to('login');
		}

		$words = array(
			'Querulous',
			'Munificent',
			'Nefarious',
		);
		View::share('word', $words[array_rand($words)]);
		return $this->renderView('Create', View::make('create'));
	}

	private function _fetch_word($word = null)
	{
		$client = new GuzzleHttp\Client();
		$plugin = new ForceCharsetPlugin();

		$plugin->setForcedCharset('utf8');
		$client->getEmitter()->attach($plugin);

		$response = $client->get(Config::get('mw.base') . '/' . $word, array(
			'query' => array(
				'key' => Config::get('mw.dictionary'),
			),
		));

		if ($response->getStatusCode() !== 200) {
			$data = $response->xml();

			if (isset($data->suggestion)) {
				return array(
					'suggestions' => (array) $data->suggestion,
				);
			}

			$sound = $data->entry->sound->wav;

			return array(
				'pronunciation' => $data->entry->pr[0],
				'sound'         => (string) Config::get('mw.media_base') . '/' . substr($sound, 0, 1) . '/' . $sound,
			);
		}

		return false;
	}

	public function create()
	{
		if (! Request::ajax()) {
			return App::abort(404);
		}

		if (! Auth::check()) {
			return Redirect::to('login');
		}

		$file = Input::get('file');

		if (strpos($file, 'data:image') !== 0) {
			return $this->renderJson(false, 'The upload must be an image.');
		}

		if (! $file = base64_decode(explode('base64,', $file)[1])) {
			return $this->renderJson(false, 'The upload failed.');
		}

		if (! Input::has('word')) {
			return $this->renderJson(false, 'A word must be provided.');
		}

		$handler = finfo_open();
		$mime    = finfo_buffer($handler, $file, FILEINFO_MIME_TYPE);
		finfo_close($handler);

		$name    = md5($file);
		$ext     = explode('/', $mime)[1];
		$path    = public_path() . '/img/uploads/' . $name . '.' . $ext;
		$uri     = '/img/uploads/' . $name . '.' . $ext;
		$handler = fopen($path, 'wb');
		fwrite($handler, $file);
		fclose($handler);

		$image_info = getimagesize($path);

		if ($image_info[0] !== $image_info[1]) {
			return $this->renderJson(false, 'The upload must be square.');
		}

		if ((int) $image_info[0] > 800 || (int) $image_info[1] > 800) {
			return $this->renderJson(false, 'The upload must be 800px square or less.');
		}

		$word      = trim(Input::get('word'), ' ');
		$word_data = $this->_fetch_word($word);

		if (isset($word_data['suggestions'])) {
			return $this->renderJson(false, $word_data['suggestions']);
		}

		$success = $this->_addWordToImage($word, $path, $name);

		$post                 = new Post;
		$post->word           = (string) strtolower($word);
		$post->pronunciation  = utf8_encode($word_data['pronunciation']);
		$post->sound          = (string) $word_data['sound'];
		$post->asset          = (string) str_replace($name, $success['name'], $uri);
		$post->user_id        = (int) Auth::id();
		$saved                = $post->save();

		return $this->renderJson((bool) $saved);
	}

	private function _addWordToImage($word, $path, $name)
	{
		$image = new Imagick($path);
		$text  = new ImagickDraw();
		$temp  = tempnam('/tmp', 'WORD');

		$image->resizeImage(400, 400, Imagick::FILTER_POINT, 1);
		$image->writeImage($temp);
		$image->destroy();

		if (Input::get('filter') !== 'none') {
			$insta = new Instagraph;
			$insta->setInput($temp);
			$insta->setOutput($temp);
			$insta->process(Input::get('filter'));
		}

		$text->setFillColor('#FFFFFF');
		$text->setGravity(Imagick::GRAVITY_SOUTH);
		$text->setFont(public_path() . '/fonts/gen.ttf');

		$length    = strlen($word);
		$font_size = 60;
		$base      = 5;

		if ($length >= 16) {
			$font_size = 30;
			$base = 25;
		}

		$text->setFontSize($font_size);

		$back = new ImagickDraw();
		$back->setFillColor(new ImagickPixel('rgba(0, 0, 0, .8)'));
		$back->rectangle(0, 320, 400, 400);

		$final = new Imagick($temp);
		$final->drawImage($back);

		$final->annotateImage($text, 0, $base, 0, ucfirst(strtolower($word)));
		$text->destroy();

		$new_name = md5($final->getImageBlob());
		$new_path = str_replace($name, $new_name, $path);

		$final->writeImage($new_path);
		$back->destroy();
		$final->destroy();

		return array(
			'path' => $new_path,
			'name' => $new_name,
		);
	}

	public function delete($id = null)
	{
		if (! Auth::check()) {
			return Redirect::to('login');
		}

		if (! Request::ajax() || is_null($id)) {
			return App::abort(404);
		}

		$post = Post::find((int) $id);

		if (is_null($post) || $post->user_id !== Auth::id()) {
			return App::abort(404);
		}

		return $this->renderJson($post->softDelete());
	}

	public function feed()
	{
		if (! Request::ajax()) {
			return App::abort(404);
		}

		if (! Auth::check()) {
			return Redirect::to('login');
		}

		$posts = Post::with('user');

		$posts->where('is_deleted', '=', 0);
		$posts->orderBy('created_at', 'DESC');

		if (Input::has('mine') && (int) Input::get('mine')) {
			$posts->where('user_id', '=', Auth::id());
		}

		$posts = $posts->get();

		return $this->renderJson(true, $posts->toArray());
	}

	public function listView()
	{
		if (! Auth::check()) {
			return Redirect::to('login');
		}
		View::share('user_id', Auth::id());
		return $this->renderView('Feed', View::make('list'));
	}
}
