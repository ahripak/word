<?php

class UsersController extends BaseController {

	public function login()
	{
		if (Auth::check()) {
			return Redirect::to('');
		}

		if (! Request::ajax()) {
			return $this->renderView('Log In', View::make('login'));
		}

		$message = 'Invalid credentials.';

		$auth = Auth::attempt(
			array(
				'email' => Input::get('email'),
				'password' => Input::get('password'),
			)
		);

		if ($auth) {
			$message = null;
		}

		return $this->renderJson($auth, $message);
	}

	public function register()
	{
		if (Auth::check()) {
			return Redirect::to('');
		}

		if (! Request::ajax()) {
			return $this->renderView('Register', View::make('register'));
		}

		$email = trim(Input::get('email'), ' ');

		if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return $this->renderJson(false, 'Email is invalid.');
		}

		$user = User::where('email', '=', $email)->first();

		if (! is_null($user)) {
			return $this->renderJson(false, 'Email is already registered.');
		}

		if (Input::get('password') !== Input::get('confirm')) {
			return $this->renderJson(false, 'Passwords don\'t match.');
		}

		$user = new User;
		$user->email = trim(Input::get('email'), ' ');
		$user->password = Hash::make(Input::get('password'));
		$user->save();

		Auth::login($user);

		return $this->renderJson(true);
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('login');
	}

}
