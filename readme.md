# Word
### Requirements
* PHP
* MySQL
* Composer
* Bower
* Node & npm
* Grunt CLI
* Imagick

### Installation
* Clone the repository
* Init the submodules (`git submodule update --init`)
* Install the composer packages (`cd laravel && composer install`)
* Run the artisan listener (`cd laravel && ./artisan word:listen`)
* Create a new MySQL database and import `misc/schema.sql`
* Ensure the `laravel/app/config/database.php` is correct
* Run `cd static && grunt build:dev` to recompile assets

### Features
* Create Account
* Login/Logout
* Create post
    * Image upload with filter selection
    * Overlay word of your choosing
    * Query against the Merriam-Webster API for annunciation sound
    * Query against the Merriam-Webster API for suggestions for misspellings
* View feed of all posts
* Delete posts you own
* Play annunciation sound
* _Facebook Style_ notifications (on browsers that support WebSockets) for new posts

### License
The MIT License (MIT)

Copyright (c) 2014 Alexander Hripak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.